#define _CRT_SECURE_NO_WARNINGS 1


#include <stdio.h>
//int main()
//{
//	int num = 0;
//	scanf("%d", &num);
//
//	int i = 0;
//	int j = 0;
//
//	//上半部分
//	for (i = 0; i < num; i++)
//	{
//		for (j = 0; j < num - 1-i; j++)
//		{
//			printf(" ");
//		}
//
//		for (j = 0; j < 2 * i + 1; j++)
//		{
//			printf("*");
//		}
//		printf("\n");
//
//	}
//	//下半部分
//	for (i = 0; i < num - 1; i++)
//	{
//		for (j = 0; j <= i; j++)
//		{
//			printf(" ");
//		}
//
//		for (j = 0; j < 2 * (num - 1 - i) - 1; j++)
//		{
//			printf("*");
//		}
//		printf("\n");
//	}
//	return 0;
//}


//求水仙花数
#include <math.h>
int main()
{
	int tmp = 0;
	int i = 0;
	for (i = 0; i < 99999; i++)
	{
		//计算是几位数
		int count = 1;
		tmp = i;
		while (tmp / 10)
		{
			count++;
			tmp = tmp / 10;
		}

		//计算每位数的次方
		int num = 0;
		tmp = i;
		while (tmp)
		{
			num = num + pow(tmp % 10, count);
			tmp = tmp / 10;
		}

		//判断是否相等
		if (i == num)
		{
			printf("%d是水仙花数\n", i);
		}

	}
	return 0;
}

