#define _CRT_SECURE_NO_WARNINGS 1


#include "Binary.h"

// 通过前序遍历的数组"ABD##E#H##CF##G##"构建二叉树
BTNode* BinaryTreeCreate(BTDataType* a, int* pi)
{
	if (a[*pi] == '#')
	{
		(*pi)++;
		return NULL;
	}
	BTNode* root = (BTNode*)malloc(sizeof(BTNode));
	if (root == NULL)
	{
		perror("malloc fail");
		return NULL;
	}
	
	root->_Data = a[(*pi)++];
	root->_Left = BinaryTreeCreate(a, pi);
	root->_Right = BinaryTreeCreate(a, pi);


	return root;
}
// 二叉树节点个数
int BTSize(BTNode* root)
{
	if (root == NULL)
		return 0;
	return BTSize(root->_Left) + BTSize(root->_Right) + 1;
}
// 二叉树叶子节点个数
int BTLeafSize(BTNode* root)
{
	if (root == NULL)
		return 0;
	int leSize = BTLeafSize(root->_Left);
	int riSize = BTLeafSize(root->_Right);

	return leSize > riSize ? leSize + 1 : riSize + 1;

}
// 二叉树第k层节点个数
int BTLevelKSize(BTNode* root, int k)
{	
	assert(k > 0);
	if (root == NULL)
		return 0;
	if (k == 1)
		return 1;
	return BTLevelKSize(root->_Left, k - 1) + BTLevelKSize(root->_Right, k - 1);
}
// 二叉树查找值为x的节点
BTNode* BinaryTreeFind(BTNode* root, BTDataType x)
{
	if (root == NULL)
		return NULL;
	if (root->_Data == x)
	{
		return root;
	}
	BTNode* lret = BinaryTreeFind(root->_Left, x);
	if (lret != NULL)
		return lret;
	BTNode* rret = BinaryTreeFind(root->_Right, x);
	if (rret != NULL)
		return rret;
	return NULL;
}

// 二叉树前序遍历
void BTPrevOrder(BTNode* root)
{
	if (root == NULL)
	{
		printf("NULL ");
		return;
	}
	printf("%c ", root->_Data);
	BTPrevOrder(root->_Left);
	BTPrevOrder(root->_Right);

}
// 二叉树中序遍历
void BTInOrder(BTNode* root)
{
	if (root == NULL)
	{
		printf("NULL ");
		return;
	}
	BTPrevOrder(root->_Left);
	printf("%d ", root->_Data);
	BTPrevOrder(root->_Right);
}
// 二叉树后序遍历
void BTPostOrder(BTNode* root)
{
	if (root == NULL)
	{
		printf("NULL ");
		return;
	}
	BTPrevOrder(root->_Left);
	BTPrevOrder(root->_Right);
	printf("%d ", root->_Data);

}
// 二叉树销毁
void BinaryTreeDestory(BTNode* root)
{
	if (root == NULL)
		return;
	BinaryTreeDestory(root->_Left);
	BinaryTreeDestory(root->_Right);
	free(root);

}

// 层序遍历
void BTLevelOrder(BTNode* root)
{
	Queue q;
	QueueInit(&q);
	if (root)
		QueuePush(&q, root);
	while (!QueueEmpty(&q))
	{
		BTNode* front = QueueFront(&q);
		QueuePop(&q);
		printf("%c ", front->_Data);

		if(front->_Left)
			QueuePush(&q, front->_Left);
		if (front->_Right)
			QueuePush(&q, front->_Right);
	}
	printf("\n");


	QueueDestory(&q);
}
// 判断二叉树是否是完全二叉树
int BTComplete(BTNode* root)
{
	Queue q;
	QueueInit(&q);
	if (root)
		QueuePush(&q, root);
	while (!QueueEmpty(&q))
	{
		BTNode* front = QueueFront(&q);
		QueuePop(&q);
		if (front == NULL)
		{
			break;
		}
		
			QueuePush(&q, front->_Left);
			QueuePush(&q, front->_Right);
	}
	while (!QueueEmpty(&q))
	{
		BTNode* front = QueueFront(&q);
		QueuePop(&q);
		if (front != NULL)
		{
			QueueDestory(&q);
			return false;
		}
	}

	QueueDestory(&q);
	return true;
}