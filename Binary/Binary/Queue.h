#pragma once

#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <assert.h>

typedef char BTDataType;
typedef struct BinaryTreeNode
{
	BTDataType _Data;
	struct BinaryTreeNode* _Left;
	struct BinaryTreeNode* _Right;
}BTNode;

typedef BTNode* QDatatype;
typedef struct QListNode
{
	struct QListNode* _next;
	QDatatype _data;
}QNode;
// 队列的结构
typedef struct Queue
{
	QNode* _front;
	QNode* _rear;
}Queue;
//初始化队列
void QueueInit(Queue* q);
//队尾入队列
void QueuePush(Queue* q, QDatatype data);
//队头出队列
void QueuePop(Queue* q);
// 获取队列头部元素
QDatatype QueueFront(Queue* q);
// 获取队列队尾元素
QDatatype QueueBack(Queue* q);
// 获取队列中有效元素个数
int QueueSize(Queue* q);
// 检测队列是否为空，如果为空返回非零结果，如果非空返回0 
int QueueEmpty(Queue* q);
//销毁队列
void QueueDestory(Queue* q);