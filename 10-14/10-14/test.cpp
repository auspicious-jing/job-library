#define _CRT_SECURE_NO_WARNINGS 1


#include <iostream>

using namespace std;


//int main()
//{
//	int year1, month1, day1;
//	int year2, month2, day2;
//	scanf("%4d%2d%2d\n%4d%2d%2d", &year1, &month1, &day1, &year2, &month2, &day2);
//	if (year1 <= year2 && month1 <= month2 && day1 <= day2)
//	{
//		cout << "1" << endl;
//	}
//
//
//
//		return 0;
//}
//
//
////不用判断条件，实现1+2+3....+n
//class Solution {
//public:
//	int Sum_Solution(int n) {
//
//		n && (n += Sum_Solution(n - 1));
//		return n;
//	}
//};


//class A
//{
//public:
//    A(int a)
//        :_a1(a)
//        , _a2(_a1)
//    {}
//
//    void Print() {
//        cout << _a1 << " " << _a2 << endl;
//    }
//private:
//    int _a2;
//    int _a1;
//};
//int main() {
//    A aa(1);
//    aa.Print();
//}


//class Date
//{
//public:
//    // 1. 单参构造函数，没有使用explicit修饰，具有类型转换作用
//    // explicit修饰构造函数，禁止类型转换---explicit去掉之后，代码可以通过编译
//   /* explicit Date(int year)
//        :_year(year)
//    {}*/
//    
//    // 2. 虽然有多个参数，但是创建对象时后两个参数可以不传递，没有使用explicit修饰，具
//    //有类型转换作用
//     //explicit修饰构造函数，禁止类型转换
//        explicit Date(int year, int month = 1, int day = 1)
//            : _year(year)
//            , _month(month)
//            , _day(day)
//            {}
//    
//
//    Date& operator=(const Date& d)
//    {
//        if (this != &d)
//        {
//            _year = d._year;
//            _month = d._month;
//            _day = d._day;
//        }
//        return *this;
//    }
//private:
//    int _year;
//    int _month;
//    int _day;
//};
//
//void Test()
//{
//    Date d1(2022);
//    // 用一个整形变量给日期类型对象赋值
//    // 实际编译器背后会用2023构造一个无名对象，最后用无名对象给d1对象进行赋值
//    d1 = 2023;
//    Date d2 = 2023;
//    // 将1屏蔽掉，2放开时则编译失败，因为explicit修饰构造函数，禁止了单参构造函数类型转
//    //换的作用
//}
//int main()
//{
//    Test();
//    return 0;
//}