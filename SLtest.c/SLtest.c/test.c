#define _CRT_SECURE_NO_WARNINGS 1


#include "SeqList.h"

void SeqList1()
{
	SL s;
	SLInit(&s);

	printf("尾插\n");
	SLPushBack(&s, 1);
	SLPushBack(&s, 2);
	SLPushBack(&s, 3);
	SLPushBack(&s, 4);
	SLPushBack(&s, 5);
	SLPushBack(&s, 6);
	SLPrint(&s);

	printf("头插\n");
	SLPushFornt(&s, 10);
	SLPushFornt(&s, 20);
	SLPrint(&s);

	printf("尾删\n");
	SLPopBack(&s);
	SLPopBack(&s);
	SLPopBack(&s);
	SLPrint(&s);

	printf("头删\n");
	SLPopFornt(&s);
	SLPopFornt(&s);
	SLPrint(&s);

	printf("查找3\n");
	int ret = SeqListFind(&s, 3);
	printf("找到了，下标是%d\n", ret);

	printf("修改pos位置\n");
	SeqListInsert(&s, ret, 15);
	SLPrint(&s);
	
	printf("删除pos位置\n");
	SeqListErase(&s, ret);
	SLPrint(&s);

	printf("销毁\n");
	SLDestory(&s);
}

int main()
{
	SeqList1();

	return 0;
}