#pragma once

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

typedef int SLDateType;

typedef struct SeqList
{
	SLDateType* arr;
	size_t size;
	size_t capacity;
}SL;

//��ʼ��
void SLInit(SL* psl);
//����
void SLDestory(SL* psl);
//��ӡ˳���
void SLPrint(SL* psl);
//β��
void SLPushBack(SL* psl,SLDateType x);
//ͷ��
void SLPushFornt(SL* psl, SLDateType x);
//βɾ
void SLPopBack(SL* psl);
//ͷɾ
void SLPopFornt(SL* psl);
// ˳�������
int SeqListFind(SL* psl, SLDateType x);
// ˳�����posλ�ò���x
void SeqListInsert(SL* psl, size_t pos, SLDateType x);
// ˳���ɾ��posλ�õ�ֵ
void SeqListErase(SL* psl, size_t pos);