#define _CRT_SECURE_NO_WARNINGS 1
#include "SeqList.h"

//初始化
void SLInit(SL* psl)
{

	psl->arr = NULL;
	psl->size = psl->capacity = 0;
}
//销毁
void SLDestory(SL* psl)
{
	assert(psl);
	
	if (psl->arr)
	{
		free(psl->arr);
		psl->arr = NULL;
		psl->capacity = psl->size = 0;
	}
}

//打印顺序表
void SLPrint(SL* psl)
{
	assert(psl);
	for (size_t i = 0; i < psl->size; i++)
	{
		printf("%d ", psl->arr[i]);
	}
	printf("\n");
}
//检查容量
void CheckCapacity(SL* psl)
{
	
	if (psl->size == psl->capacity)
	{
		int newcapacity = psl->capacity == 0 ? 4 : psl->capacity * 2;
		SLDateType* tmp = (SLDateType*)realloc(psl->arr, newcapacity * sizeof(SLDateType));
		if (tmp == NULL)
		{
			perror("realloc fail");
			return;
		}
		psl->arr = tmp;
		psl->capacity = newcapacity;

	}
}
//尾插
void SLPushBack(SL* psl, SLDateType x)
{
	assert(psl);

	CheckCapacity(psl);

	psl->arr[psl->size] = x;
	psl->size++;
}
//头插
void SLPushFornt(SL* psl, SLDateType x)
{
	assert(psl);

	CheckCapacity(psl);

	int end = psl->size - 1;
	while (end >= 0)
	{
		psl->arr[end + 1] = psl->arr[end];
		end--;
	}
	psl->arr[0] = x;
	psl->size++;
}
//尾删
void SLPopBack(SL* psl)
{
	assert(psl);

	if (psl->size > 0)
	{
		psl->size--;
	}
}	
//头删
void SLPopFornt(SL* psl)
{
	assert(psl);

	if (psl->size > 0)
	{
		
		for (size_t i = 0; i < psl->size-1; i++)
		{
			psl->arr[i] = psl->arr[i + 1];
		}
		psl->size--;
	}

}

// 顺序表查找
//找到了返回下标，没找到返回-1
int SeqListFind(SL* psl, SLDateType x)
{
	assert(psl);

	for (size_t i = 0; i < psl->size; i++)
	{

		if (psl->arr[i] == x)
		{
			return i;
		}
	}
	return -1;
}

// 顺序表在pos位置插入x
void SeqListInsert(SL* psl, size_t pos, SLDateType x)
{
	assert(psl);
	CheckCapacity(psl);

	if (pos > psl->size)
		return;
	size_t begin = psl->size;
	while (begin > pos)
	{
		psl->arr[begin] = psl->arr[begin - 1];
		--begin;
	}
	
	psl->arr[pos] = x;
	psl->size++;
}

// 顺序表删除pos位置的值
void SeqListErase(SL* psl, size_t pos)
{
	assert(psl);
	if (pos >= psl->size)
		return;
	else
	{
		for (size_t i = pos; i < psl->size - 1; i++)
		{
			psl->arr[i] = psl->arr[i + 1];
		}
		psl->size--;
	}
	
}
