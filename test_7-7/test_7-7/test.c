#define _CRT_SECURE_NO_WARNINGS 1

#include <stdio.h>
#include <assert.h>


////模拟实现strcmp
//int my_strcmp(const char* str1, const char* str2)
//{
//	assert(str1 && str2);
//	
//	while (*str1 !='\0'&& * str1 == *str2)
//	{
//		str1++;
//		str2++;
//
//	}
//	return *str1 - *str2;
//}
//
//int main()
//{
//	char arr1[] = "abc";
//	char arr2[] = "abcdef";
//
//	int ret = my_strcmp(arr1, arr2);
//
//	if (ret > 0)
//	{
//		printf(">\n");
//	}
//	else if (ret == 0)
//	{
//		printf("==\n");
//	}
//	else
//	{
//		printf("<\n");
//	}
//	return 0;
//}



//模拟实现strstr
//const char* my_strstr(const char* str1, const char* str2)
//{
//	assert(str1 && str2);
//
//	const char* p = str1;
//	while ( *p)
//	{
//		const char* s1 = p;
//		const char* s2 = str2;
//
//		while (*s1++ == *s2++)
//		{
//			/*s1++;
//			s2++;*/
//
//			if (*s2 == '\0')
//				return p;
//					
//		}
//		p++;
//	}
//	return NULL;
//}
//int main()
//{
//	char arr1[] = "abbcdef";
//	char arr2[] = "\0";
//
//	const char* ret = my_strstr(arr1, arr2);
//
//	if (ret != NULL)
//	{
//		printf("%s\n", ret);
//	}
//	else
//	{
//		printf("子串不存在\n");
//	}
//	return 0;
//}

//#include<stdio.h>
int main()
{
    union
    {
        short k;
        char i[2];
    }*s, a;
    s = &a;
    s->i[0] = 0x39;
    s->i[1] = 0x38;
    printf(" % x\n", a.k);
    return 0;
}