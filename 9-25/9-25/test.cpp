#define _CRT_SECURE_NO_WARNINGS 1


#include <iostream>
#include "Func.h"
using namespace std;

//int main()
//{
//	int ret = 0;
//
//	ret = Func();
//
//	cout << ret << endl;
//
//	return 0;
//}


//int TestAuto()
//{
//	return 10;
//}
//int main()
//{
//	int a = 10;
//	auto b = a;
//	auto c = 'a';
//	auto d = TestAuto();
//	cout << typeid(b).name() << endl;
//	cout << typeid(c).name() << endl;
//	cout << typeid(d).name() << endl;
//	auto e; 无法通过编译，使用auto定义变量时必须对其进行初始化
//	return 0;
//}
//void TestAuto()
//{
//    auto a = 1, b = 2;
//    auto c = 3, d = 4.0;  // 该行代码会编译失败，因为c和d的初始化表达式类型不同
//}
//
//int main()
//{
//
//    TestAuto();
//
//    int x = 10;
//    auto a = &x;
//    auto* b = &x;
//    auto& c = x;
//    cout << typeid(a).name() << endl;
//    cout << typeid(b).name() << endl;
//    cout << typeid(c).name() << endl;
//    *a = 20;
//    cout << *a << endl;
//    *b = 30;
//    cout << *b << endl;
//    c = 40;
//    cout << c << endl;
//
//    return 0;
//}


//void TestFor()
//{
//    int array[] = { 1, 2, 3, 4, 5 };
//    //每个数乘等于2
//    for (auto& e : array)
//        e *= 2;
//    for (auto e : array)
//        cout << e << " ";
//}
//
//
//int main()
//{
//    TestFor();
//
//
//    return 0;
//}


void f(int)
{
	cout << "f(int)" << endl;
}
void f(int*)
{
	cout << "f(int*)" << endl;
}
int main()
{
	f(0);
	f(NULL);
	f((int*)NULL);
	return 0;
}
