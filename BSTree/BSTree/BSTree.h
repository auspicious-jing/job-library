#pragma once

namespace byte
{
	template<class K>
	struct BSTreeNode
	{
		BSTreeNode(const K& key)
			:_key(key)
			, _left(nullptr)
			, _right(nullptr)
		{}

		BSTreeNode<K>* _left;
		BSTreeNode<K>* _right;
		K _key;
		
	};
	template<class K>
	class BSTree
	{
	public:
		typedef BSTreeNode<K> Node;
		BSTree()
			:_root(nullptr)
		{}
	
		
		BSTree(const BSTree<K>& t)
		{
			_root = copy(t._root);
		}
		BSTree<K>& operator=(BSTree<K> t)
		{
			swap(_root, t._root);
			return *this;
		}
		~BSTree()
		{
			Destory(_root);
			_root = nullptr;
		}
		bool Insert(const K& key)
		{
			if (_root == nullptr)
			{
				_root = new Node(key);
				return true;
			}
			Node* cur = _root;
			Node* parent = nullptr;
			while (cur)
			{
				if (cur->_key > key)
				{
					parent = cur;
					cur = cur->_left;

				}
				else if (cur->_key < key)
				{
					parent = cur;
					cur = cur->_right;
				}
				else
				{
					return false;
				}
			}
			cur = new Node(key);
			if (parent->_key > key)
				parent->_left = cur;
			else
				parent->_right = cur;
			return true;
		}
		bool Erase(const K& key)
		{
			Node* cur = _root;
			Node* parent = nullptr;
			while (cur)
			{
				if (cur->_key > key)
				{
					parent = cur;
					cur = cur->_left;
				}
				else if (cur->_key < key)
				{
					parent = cur;
					cur = cur->_right;
				}
				else
				{
					//1.左为空
					if (cur->_left == nullptr)
					{
						if (cur == _root)
						{
							_root = cur->_right;
						}
						else
						{
							if (parent->_left == cur)
							{
								parent->_left = cur->_right;
							}
							else
							{
								parent->_right = cur->_right;
							}
						}
						delete cur;
					}
					//2.右为空
					else if (cur->_right == nullptr)
					{
						if (cur == _root)
						{
							_root = cur->_left;
						}
						else
						{
							if (parent->_right == cur)
							{
								parent->_right = cur->_left;
							}
							else
							{
								parent->_left = cur->_left;
							}
						}
						delete cur;
					}
					//3.都不为空
					else
					{
						Node* parent = cur;
						Node* minRight = cur->_right;
						while (minRight->_left)
						{
							parent = minRight;
							minRight = minRight->_left;
						}
						cur->_key = minRight->_key;
						if (minRight == parent->_left)
							parent->_left = minRight->_right;
						else
							parent->_right = minRight->_right;
						delete minRight;
					}
					
					return true;
				}
			}
			return false;
		}
		void InOrder()
		{
			_InOrder(_root);
			cout << endl;
		}
		bool Find(const K& key)
		{
			Node* cur = _root;
			while (cur)
			{
				if (cur->_key > key)
				{
					cur = cur->_left;
				}
				else if (cur->_key < key)
				{
					cur = cur->_right;
				}
				else
				{
					return true;
				}
			}
			return false;
		}
		bool FindR(const K& key)
		{
			return _FindR(_root, key);
		}
		bool InsertR(const K& key)
		{
			return _InsertR(_root, key);
		}
		bool EraseR(const K& key)
		{
			return _EraseR(_root, key);
		}
	private:
		Node* copy(Node* root)
		{
			if (root == nullptr)
				return nullptr;
			//前序
			Node* newNode = new Node(root->_key);
			newNode->_left =  copy(root->_left);
			newNode->_right =  copy(root->_right);

			return newNode;
		}
		void Destory(Node* root)
		{
			//二叉树后续遍历删除
			if (root == nullptr)
				return;
			Destory(root->_left);
			Destory(root->_right);

			delete root;

		}
		bool _EraseR(Node*& root,const K& key)
		{
			if (root == nullptr)
				return false;

			if (root->_key > key)
				return _EraseR(root->_left, key);
			else if (root->_key < key)
				return _EraseR(root->_right, key);
			else
			{
				Node* del = root;
				//1.左为空
				if (root->_left == nullptr)
				{
					root = root->_right;
				}
				//2.右为空
				else if (root->_right == nullptr)
				{
					root = root->_left;
				}
				//3.都不为空
				else
				{
					Node* minRight = root->_right;
					while (minRight->_left)
					{
						minRight = minRight->_left;
					}
					//root->_key = minRight->_key;
					//return _EraseR(root->_right, minRight->_key);
					//转换到子树去删除
					swap(root->_key, minRight->_key);
					return _EraseR(root->_right, key);
				}
				delete del;
				return true;
			}
		}
		bool _InsertR(Node*& root,const K& key)
		{
			if (root == nullptr)
			{
				root = new Node(key);
				return true;
			}
			if (root->_key > key)
				return _InsertR(root->_left, key);
			else if (root->_key < key)
				return _InsertR(root->_right, key);
			else
				return false;

		}
		bool _FindR(Node* root, const K& key)
		{
			if (root == nullptr)
				return false;
			if (root->_key > key)
				return _FindR(root->_left, key);
			else if (root->_key < key)
				return _FindR(root->_right, key);
			else
				return true;
		}
		void _InOrder(Node* root)
		{
			if (root == nullptr)
				return;
			_InOrder(root->_left);
			cout << root->_key << ' ';
			_InOrder(root->_right);
		}

	private:
		Node* _root = nullptr;
	};


	void test1()
	{
		int arr[] = { 8, 3, 1, 10, 6, 4, 7, 14, 13 };
		BSTree<int> bst;
		for (auto e : arr)
		{
			bst.Insert(e);
		}
		bst.InOrder();
		bst.Insert(5);
		bst.InOrder();

		bst.Erase(3);
		bst.InOrder();

		bst.Erase(8);
		bst.InOrder();
		bst.Erase(14);
		bst.InOrder();
		
	}
	void test2()
	{
		int arr[] = { 8, 3, 1, 10, 6, 4, 7, 14, 13 };
		BSTree<int> bst;
		for (auto e : arr)
		{
			bst.InsertR(e);
		}
		bst.InOrder();
		bst.Insert(2);
		bst.InOrder();

		bst.EraseR(3);
		bst.InOrder();

		bst.EraseR(8);
		bst.InOrder();
		
	}
	void test3()
	{
		int arr[] = { 8, 3, 1, 10, 6, 4, 7, 14, 13 };
		BSTree<int> bst;
		for (auto e : arr)
		{
			bst.InsertR(e);
		}
		bst.InOrder();
		BSTree<int> bst1(bst);
		bst1.InOrder();

		BSTree<int> bst2;
		bst2 = bst;
		bst2.InOrder();

	}
}
namespace KV
{
	template<class K, class V>
	struct BSTreeNode
	{
		BSTreeNode<K, V>* _left;
		BSTreeNode<K, V>* _right;
		K _key;
		V _value;

		BSTreeNode(const K& key, const V& value)
			:_key(key)
			, _value(value)
			, _left(nullptr)
			, _right(nullptr)
		{}
	};
	template<class K, class V>
	class BSTree
	{
		typedef BSTreeNode<K, V> Node;
	public:
		bool Insert(const K& key, const V& value)
		{
			if (_root == nullptr)
			{
				_root = new Node(key, value);
				return true;
			}

			Node* parent = nullptr;
			Node* cur = _root;
			while (cur)
			{
				if (cur->_key < key)
				{
					parent = cur;
					cur = cur->_right;
				}
				else if (cur->_key > key)
				{
					parent = cur;
					cur = cur->_left;
				}
				else
				{
					return false;
				}
			}
			cur = new Node(key, value);
			if (parent->_key < key)
			{
				parent->_right = cur;
			}
			else
			{
				parent->_left = cur;
			}
			return true;
		}
		Node* Find(const K& key)
		{
			Node* cur = _root;
			while (cur)
			{
				if (cur->_key < key)
				{
					cur = cur->_right;
				}
				else if (cur->_key > key)
				{
					cur = cur->_left;
				}
				else
				{
					return cur;
				}
			}
			return nullptr;
		}
		void Inorder()
		{
			_Inorder(_root);
		}

		void _Inorder(Node* root)
		{
			if (root == nullptr)
				return;

			_Inorder(root->_left);
			cout << root->_key << ":" << root->_value << endl;
			_Inorder(root->_right);
		}
	private:
		Node* _root = nullptr;
	};

	void TestBSTree2()
	{

		// Key/Value的搜索模型,通过Key查找或修改Value
		KV::BSTree<string, string> dict;
		dict.Insert("sort", "排序");
		dict.Insert("string", "字符串");
		dict.Insert("left", "左边");
		dict.Insert("right", "右边");

		string str;
		while (cin >> str)
		{
			KV::BSTreeNode<string, string>* ret = dict.Find(str);
			if (ret)
			{
				cout << ret->_value << endl;
			}
			else
			{
				cout << "无此单词" << endl;
			}
		}
	}
	void TestBSTree3()
	{
		// 统计水果出现的次数
		string arr[] = { "苹果", "西瓜", "香蕉", "草莓","苹果", "西瓜", "苹果", 
			"苹果", "西瓜", "苹果", "香蕉", "苹果", "香蕉" };

		KV::BSTree<string, int> countTree;
		for (auto e : arr)
		{
			auto* ret = countTree.Find(e);
			if (ret == nullptr)
			{
				countTree.Insert(e, 1);
			}
			else
			{
				ret->_value++;
			}
		}

		countTree.Inorder();
	}
}
