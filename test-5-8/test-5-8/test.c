#define _CRT_SECURE_NO_WARNINGS 1


#include <stdio.h>

//int main()
//{
//	int a, b, c;
//	a = 5;
//	c = ++a;//c=6 a=6
//	b = ++c, c++, ++a, a++;//b=7   c=7,a=7 //c=8 a=8
//	b += a++ + c;//b=7+8+8 =23,a=9
//	printf("a = %d b = %d c = %d\n:", a, b, c);
//	return 0;
//}

//int main()
//{
//	int m = 0;
//	int n = 0;
//	scanf("%d %d", &m, &n);
//
//	int count = 0;
//	int i = 0;
//	int c = 1;
//	for (i = 1; i <= 32; i++)
//	{
//		if ((m & c) != (n & c))
//		{
//			count++;
//		}
//		c= 1 << i;
//	}
//	printf("%d\n", count);
//	return 0;
//}

//int main()
//{
//	int m = 0;
//	int n = 0;
//	scanf("%d %d", &m, &n);
//
//	int count = 0;
//	int c = m ^ n;
//	while (c)
//	{
//		c= c & (c-1);
//		count++;
//	}
//	printf("%d\n", count);
//	return 0;
//}
//
//void Printbit(int num)
//{
//	for (int i = 31; i >= 1; i -= 2)
//	{
//		printf("%d ", (num >> i) & 1);
//	}
//	printf("\n");
//
//	for (int i = 30; i >= 0; i -= 2)
//	{
//		printf("%d ", (num >> i) & 1);
//	}
//	printf("\n");
//}
//int main()
//{
//	int n = 0;
//	scanf("%d", &n);
//
//	Printbit(n);
//	//int i = 0;
//	////二进制位
//	//for (i = 31; i >= 0; i--)
//	//{
//	//	printf("%d", (n >> i) & 1);
//	//}
//	//printf("\n");
//	////偶数位
//	//for (i = 31; i >= 1; i -= 2)
//	//{
//	//	printf("%d ", (n >> i) & 1);
//	//}
//	//printf("\n");
//	////奇数位
//	//for (i = 30; i >= 0; i -= 2)
//	//{
//	//	printf("%d ", (n >> i) & 1);
//	//}
//	//printf("\n");
//	return 0;
//}
//

//统计二进制中1的个数
//
//int main()
//{
//	int n = 0;
//	scanf("%d", &n);
//	int count = 0;
//	int i = 0;
//	for (i = 0; i < 32; i++)
//	{
//		if (((n>>i) & 1) == 1)
//		{
//			count++;
//		}
//	}
//	printf("%d\n", count);
//	return 0;
//}


//不允许创建临时变量，交换两个整数的内容
//
//int main()
//{
//	int a = 10;
//	int b = 20;
//	int c = 0;
//	//交换前
//	printf("a = %d b = %d\n", a, b);
//
//	c = a;
//	a = b;
//	b = c;
//	//交换后
//	printf("a = %d b = %d\n", a, b);
//	return 0;
////}
//
//int main()
//{
//	int a = 10;
//	int b = 20;
//	//交换前
//	printf("交换前a = %d b = %d\n", a, b);
//	//交换
//	a = a ^ b;
//	b = a ^ b;
//	a = a ^ b;
//
//	//交换后
//	printf("交换后a = %d b = %d\n", a, b);
//
//	return 0;
//}