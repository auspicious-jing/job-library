#define _CRT_SECURE_NO_WARNINGS 1

#include "Date.h"

//构造函数
Date::Date(int year, int month, int day)
{
	_year = year;
	_month = month;
	_day = day;
}

 //拷贝构造函数
 //  d2(d1)
Date::Date(const Date& d)
{
	_year = d._year;
	_month = d._month;
	_day = d._day;
}
 //获取某年某月的天数
int Date:: GetMonthDay(int year, int month)
{
	int Monthday[13] = { 0,31,28,31,30,31,30,31,31,30,31,30,31 };
	if (month == 2 && ((year % 4 == 0 && year % 100 != 0) || (year % 400 == 0)))
	{
		return 29;
	}
	else
	{
		return Monthday[month];
	}
}

////显示日期
void Date:: Print()
{
	cout << _year << "/" << _month << "/" << _day << endl;
}

// 赋值运算符重载
	// d2 = d3 -> d2.operator=(&d2, d3)
//Date& Date:: operator= (const Date& d)
//{
//
//}

	// 日期+=天数
Date& Date:: operator+=(int day)
{
	_day += day;
	while ((_day - GetMonthDay(_year, _month)) > 0)
	{
		_day -= GetMonthDay(_year, _month);
		_month++;
		if (_month > 12)
		{
			_year++;
			_month = 1;
		}
	}
	return *this;
}

//// 日期+天数
Date Date:: operator+(int day)
{
	Date ret(*this);
	ret += day;
	return ret;
}
//
//// 日期-天数
//Date operator-(int day);
//
//// 日期-=天数
//Date& operator-=(int day);
//
//// 前置++
//Date& operator++();
//
//// 后置++
//Date operator++(int);
//
//// 后置--
//Date operator--(int);
//
//// 前置--
//Date& operator--();
// 

// >运算符重载
bool Date:: operator>(const Date& d)
{
	if (_year > d._year)
	{
		return true;
	}
	else if (_year == d._year && _month > d._month)
	{
		return true;
	}
	else if (_year == d._year && _month == d._month && _day > d._day)
	{
		return true;
	}
	return false;
}
// ==运算符重载
bool Date:: operator==(const Date& d)
{
	if (_year == d._year && _month == d._month && _day == d._day)
	{
		return true;
	}
	return false;
}
// >=运算符重载
bool Date:: operator >= (const Date& d)
{
	return *this > d || *this == d;
}

// <运算符重载
bool Date:: operator < (const Date& d)
{
	return !(*this >= d);
}

// <=运算符重载
bool Date:: operator <= (const Date& d)
{
	return !(*this > d);
}

// !=运算符重载
bool Date:: operator != (const Date& d)
{
	return !(*this == d);
}