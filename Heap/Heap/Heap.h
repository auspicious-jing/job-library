#pragma once

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include <assert.h>
#include <time.h>

typedef int HpDataType;

typedef struct Heap
{
	HpDataType* _a;
	int _size;
	int _capacity;
}Heap;

//初始化
void HeapInit(Heap* hp);
void HeapPrint(Heap* hp);
// 堆的构建
void HeapCreate(Heap* hp, HpDataType* a, int n);
//插入
void HeapPush(Heap* hp, HpDataType x);
//删除堆顶数据
void HeapPop(Heap* hp);
//判断是否为空
bool HeapEmpty(Heap* hp);
//获取堆顶的数据
HpDataType HeapTop(Heap* hp);
//堆的数据个数
int HeapSize(Heap* hp);
//堆的销毁
void HeapDestry(Heap* hp);
//找n个数里最大或最小的前K个
void PrintTopK(int* a, int n, int k);
// 对数组进行堆排序
void HeapSort(int* a, int n);
