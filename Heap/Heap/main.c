#define _CRT_SECURE_NO_WARNINGS 1

#include "Heap.h"



void test1()
{
	//int a[] = { 16,72,31,23,94,53 };
	int a[] = { 94,23,31,72,16,53 };
	int sz = sizeof(a) / sizeof(int);
	Heap hp;
	HeapCreate(&hp, a, sz);
	HeapPrint(&hp);

	HeapPush(&hp, 10);
	HeapPrint(&hp);
	HeapPush(&hp, 100);
	HeapPrint(&hp);

	HeapPop(&hp);
	HeapPrint(&hp);

	HeapDestry(&hp);
}
void test2()
{
	int a[] = { 94,23,31,72,16,53,10,80,20,45};
	int sz = sizeof(a) / sizeof(int);
	HeapSort(a, sz);

	for (int i = 0; i < sz; ++i)
	{
		printf("%d ", a[i]);
	}
	printf("\n");
}
void WriteData(const char* filename, int n)
{
	FILE* fin = fopen(filename, "w");
	if (fin == NULL)
	{
		perror("fopen fail");
		return;
	}
	srand(time(NULL));

	while (n--)
	{
		fprintf(fin, "%d ", rand());
	}


	fclose(fin);
}
void TestTopK()
{
	const char* filename = "test1.txt";
	int n = 10000;
	int a[10] = { 0 };
	int k = 10;
	//WriteData(filename, n);
	PrintTopK(a, n, k);
	for (int i = 0; i < k; ++i)
	{
		printf("%d ", a[i]);
	}
	printf("\n");
}
int main()
{
	//test2();
	TestTopK();

	return 0;
}