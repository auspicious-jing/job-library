#define _CRT_SECURE_NO_WARNINGS 1
#include "Heap.h"

//初始化
void HeapInit(Heap* hp)
{
	assert(hp);
	hp->_a = NULL;
	hp->_capacity = hp->_size = 0;
}
 

void HeapPrint(Heap* hp)
{
	assert(hp);
	for(int i = 0;i < hp->_size;++i)
	{
		printf("%d ", hp->_a[i]);
	}
	printf("\n");
}
// 堆的构建
void HeapCreate(Heap* hp, HpDataType* a, int n)
{
	assert(hp);
	HeapInit(hp);

	for(int i = 0;i < n;++i)
	{
		HeapPush(hp, a[i]);
	}

}
void swap(HpDataType* q1, HpDataType* q2)
{
	HpDataType tmp = *q1;
	*q1 = *q2;
	*q2 = tmp;
}
//向上调整
void Adjustup(HpDataType* a,int child)
{
	int parent = (child - 1) / 2;

	while (child > 0)
	{
		if (a[child] < a[parent])
		{
			swap(&a[child], &a[parent]);
			child = parent;
			parent = (child - 1) / 2;
		}
		else
		{
			break;
		}
	}

}
//向下调整
void Adjustdown(HpDataType* a,int n, int parent)
{
	int child = parent * 2 + 1;
	while (child < n)
	{
		if ( child + 1< n && a[child] > a[child + 1])
		{
			++child;
		}
		if (a[child] < a[parent])
		{
			swap(&a[child], &a[parent]);
			parent = child;
			child = parent * 2 + 1;
		}
		else
		{
			break;
		}
	}
}
//插入
void HeapPush(Heap* hp, HpDataType x)
{
	assert(hp);
	if (hp->_capacity == hp->_size)
	{
		HpDataType newcapacity = hp->_capacity == 0 ? 4 : hp->_capacity * 2;
		HpDataType* tmp = (HpDataType*)realloc(hp->_a, sizeof(HpDataType)* newcapacity);
		if (tmp == NULL)
		{
			perror("malloc fail");
			exit(-1);
		}
		
		hp->_a = tmp;
		hp->_capacity = newcapacity;
	}
	hp->_a[hp->_size] = x;
	hp->_size++;
	Adjustup(hp->_a,hp->_size - 1);
}
//删除堆顶数据
void HeapPop(Heap* hp)
{
	assert(hp);
	if (!HeapEmpty(hp))
	{
		swap(&hp->_a[0], &hp->_a[hp->_size - 1]);
		hp->_size--;
		Adjustdown(hp->_a,hp->_size,0);
	}
}
//判断是否为空
bool HeapEmpty(Heap* hp)
{
	assert(hp);
	return hp->_size == 0;
}
//获取堆顶的数据
HpDataType HeapTop(Heap* hp)
{
	assert(hp);
	return hp->_a[0];
}
//堆的数据个数
int HeapSize(Heap* hp)
{
	assert(hp);
	return hp->_size;
}
//堆的销毁
void HeapDestry(Heap* hp)
{
	assert(hp);
	free(hp->_a);
	hp->_capacity = hp->_size = 0;
}
// 对数组进行堆排序
void HeapSort(int* a, int n)
{
	//升序建大堆，降序建小堆
	for (int i = (n-2)/2; i >= 0; --i)
	{
		Adjustdown(a, n, i);
	}
	//先选出最大的或最小的，交换到数组尾部，向下调整后再选出次大或次小的再次交换到尾部位置的前一个位置
	for (int i = 1; i < n; ++i)
	{
		swap(&a[0], &a[n - i]);
		Adjustdown(a, n - i, 0);
	}
}


//找n个数里最大或最小的前K个
void PrintTopK(int* a, int n, int k)
{
	const char* filename = "test1.txt";
	FILE* fout = fopen(filename, "r");
	if (fout == NULL)
	{
		perror("fopen fail");
		return;
	}
	/*a = (int*)malloc(sizeof(int) * k);
	if (a == NULL)
	{
		perror("malloc fail");
		return;
	}*/
	
	for(int i = 0;i<k;++i)
	{
		fscanf(fout, "%d", &a[i]);
		
	}
	for (int i = (k - 2) / 2; i >= 0; --i)
	{
		Adjustdown(a, k, i);
	}
	int ret = 0;
	while (fscanf(fout, "%d", &ret) != EOF)
	{
		if (ret > a[0])
		{
			a[0] = ret;
			Adjustdown(a, k, 0);
		}
	}

	//free(a);
	fclose(fout);
}