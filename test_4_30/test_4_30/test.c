#define _CRT_SECURE_NO_WARNINGS 1

#include<stdio.h>

//编写一个函数 reverse_string(char * string)（递归实现）

//int MyStrlen(char* string)
//{
//	int count = 0;
//	while (*string != '\0')
//	{
//		count++;
//		string++;
//	}
//	return count;
//}
////void reverse_string(char* string)
////{
////	int sz = MyStrlen(string);
////	int left = 0;
////	int right = sz - 1;
////	if (*string !='\0')
////	{
////		char tmp = string[left];
////		string[left] = string[right];
////		string[right] = '\0';
////		reverse_string(string + 1);
////		string[right] = tmp;
////	}
////}
//void reverse_string(char* string)
//{
//	char* left = string;
//	char* right = string+(MyStrlen(string) -1);
//	if (*left != '\0')
//	{
//		char tmp = *left;
//		*left = *right;
//		*right = '\0';
//		reverse_string(string + 1);
//		*right = tmp;
//	}
//}
//int main()
//{
//	char arr[] = "abcdefg";
//	printf("%s\n", arr);//逆序前
//	reverse_string(arr);
//	printf("%s\n", arr);//逆序后
//	return 0;
//}




//非递归实现求第n个斐波那契数
//
//int Fib(int n)
//{
//	if (n <= 2)
//		return 1;
//
//	int c = 0;
//	int a = 1;
//	int b = 1;
//	while (n>2)
//	{
//		c = a + b;
//		a = b;
//		b = c;
//		n--;
//	}
//	return c;
//}
//int main()
//{
//	int n = 0;
//	scanf("%d", &n);
//
//	int ret = Fib(n);
//
//	printf("%d\n", ret);
//
//	return 0;
//}


//递归实现求第n个斐波那契数
//
//int Fib(int n)
//{
//	if (n <= 2)
//		return 1;
//	else
//	{
//		return Fib(n - 1) + Fib(n - 2);
//	}
//}
//int main()
//{
//	int n = 0;
//	scanf("%d", &n);
//	int ret = Fib(n);
//	printf("%d\n", ret);
//	return 0;
//}

////将数组A中的内容和数组B中的内容进行交换。（数组一样大）
//
//void Swap(int arr1[], int arr2[],int sz)
//{
//	int i = 0;
//	for (i = 0; i < sz; i++)
//	{
//		int tmp = arr1[i];
//		arr1[i] = arr2[i];
//		arr2[i] = tmp;
//	}
//}
//int main()
//{
//	int arr1[] = { 1,3,5,7,9 };
//	int arr2[] = { 2,4,6,8,0 };
//	int sz = sizeof(arr1) / sizeof(arr1[0]);
//	Swap(arr1,arr2,sz);
//
//	int i = 0;
//	for (i = 0; i < sz; i++)
//	{
//		printf("%d ", arr1[i]);
//	}
//	printf("\n");
//	for (i = 0; i < sz; i++)
//	{
//		printf("%d ", arr2[i]);
//	}
//
//	return 0;
//}

//
//创建一个整形数组，完成对数组的操作
//
//实现函数init() 初始化数组为全0
//实现print()  打印数组的每个元素
//实现reverse()  函数完成数组元素的逆置。
void init(int arr[],int sz)
{
	int i = 0;
	for (i = 0; i < sz; i++)
	{
		arr[i] = 0;
	}
}
void print(int arr[],int sz)
{
	int i = 0;
	for (i = 0; i < sz; i++)
	{
		printf("%d ",arr[i]);
	}
	printf("\n");
}
void reverse(int arr[],int sz)
{
	int left = 0;
	int right = sz - 1;
	while (left < right)
	{
		int tmp = arr[left];
		arr[left] = arr[right];
		arr[right] = tmp;
		left++;
		right--;
	}
}
int main()
{
	int arr[10] = {1,2,3,4,5,6,7,8,9,0 };
	int sz = sizeof(arr) / sizeof(arr[0]);
	print(arr, sz);

	//数组逆序
	reverse(arr, sz);
	print(arr, sz);

	//初始化数组
	init(arr,sz);
	print(arr,sz);
	return 0;
}