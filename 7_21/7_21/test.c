#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>
#include <assert.h>
#include <string.h>


////在一个字符串中查找子串
//char* Mystrstr(const char* str1, const char* str2)
//{
//	assert(str1 && str2);
//
//	const char* s1 = str1;
//	const char* s2 = str2;
//	const char* p = str1;
//
//	while (*p)
//	{
//		s1 = p;
//		s2 = str2;
//
//		while (*s1 != '\0' && *s2 != '\0' && *s1 == *s2)
//		{
//			s1++;
//			s2++;
//		}
//		if (*s2 == '\0')
//		{
//			return (char*)p;
//		}
//		p++;
//	}
//	return NULL;
//}
//
//int main()
//{
//	char arr1[] = "abbbcdef";
//	char arr2[] = "bbc";
//
//	char* ret = Mystrstr(arr1, arr2);
//
//	if (ret != NULL)
//	{
//		printf("%s\n", ret);
//	}
//	else
//	{
//		printf("找不到子字符串\n");
//	}
//	return 0;
//}


//
void* Mymemcpy(void* dest, const void* src, size_t num)
{
	assert(dest && src);
	
	void* ret = dest;
	while( num-- )
	{
		*(char*)dest = *(char*)src;

		++(char*)dest;
		++(char*)src;

	}
	return ret;
}

int main()
{
	

	int arr1[10] = {1,2,3,4,5};
	int arr2[] = { 1,2,3,4,5 };
	int sz = sizeof(arr2) / sizeof(arr2[0]);
	Mymemcpy(arr1+5,arr2, sizeof(arr2));
	for (int i = 0; i < 10; i++)
	{
		printf("%d ", arr1[i]);
	}
	
	return 0;
}