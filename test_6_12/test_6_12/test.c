#define _CRT_SECURE_NO_WARNINGS 1

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
//练习使用库函数，qsort排序各种类型的数据

struct Stu
{
	char name[20];
	int age;
};
//结构体的判断
int compare_name(const void* e1,const void* e2)
{
	/*return (*(int*)e1 - *(int*)e2);*/
	//return strcmp(((struct Stu*)e1)->name, ((struct Stu*)e2)->name);
	return ((struct Stu*)e1)->age - ((struct Stu*)e2)->age;
}
//整形判断
int compare(const void* e1, const void* e2)
{
	return (*(int*)e1 - *(int*)e2);
}
//交换函数
void Swap(char* e1, char* e2, int width)
{
	int i = 0;
	for (i = 0; i < width; i++)
	{
		char tmp = 0;
		tmp = *e1;
		*e1 = *e2;
		*e2 = tmp;
		e1++;
		e2++;
	}
}

void my_qsort(void* base, int num, int width, int (*compare)(const void* e1, const void* e2))
{

	int i = 0;
	int j = 0;
	int flag = 1;
	for (i = 0; i < num - 1; i++)
	{
		for (j = 0; j < num - 1 - i; j++)
		{
			if (compare((char*)base + j * width, (char*)base + (j + 1) * width) > 0)
			{
				Swap((char*)base + j * width, (char*)base + (j + 1) * width, width);
				flag = 0;
			}
		}
		if (flag == 1)
		{
			break;
		}
	}

}
void test2()
{
	struct Stu s[] = { {"zhangsan",20},{"lisi",30},{"wangwu",18}};
	int sz = sizeof(s) / sizeof(s[0]);
	my_qsort(s, sz, sizeof(s[0]), compare_name);

	int i = 0;
	for (i = 0; i < sz; i++)
	{
		printf("%s %d\n", (s[i]).name, (s[i]).age);
	}
}
//浮点数判断
int cmp_dou(const void* e1, const void* e2)
{
	int k = 0;
	if ((*(float*)e1 - *(float*)e2) > 0)
	{
		k = 1;
	}
	else if ((*(float*)e1 - *(float*)e2) < 0)
	{
		k = -1;
	}
	else
	{
		k = 0;
	}

	return k;
}
void test3()
{
	float b[3] = { -3.12f,2.34f,1.12f };
	my_qsort(b, 3, sizeof(b[0]), cmp_dou);
	int i = 0;
	for (i = 0; i < 3; i++)
	{
		printf("%.2f ", b[i]);
	}
}

// void *base, size_t num, size_t width, int (__cdecl *compare )(const void *elem1, const void *elem2 )

void test1()
{
	int arr[] = { 9,8,7,6,5,4,3,2,1,0 };

	int sz = sizeof(arr) / sizeof(arr[0]);

	my_qsort(arr, sz, sizeof(arr[0]), compare);
	int i = 0;
	for (i = 0; i < sz; i++)
	{
		printf("%d ", arr[i]);

	}
}
int main()
{
	//test1();//整形
	//test2();//结构体
	test3();//浮点型
	return 0;
}

//