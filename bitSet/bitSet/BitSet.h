#pragma once


template<size_t N>
class BitSet
{
public:
	BitSet()
	{	//因为除8后可能会丢掉后面的小数，所以+1，保证开足够的空间
		_bs.resize(N / 8 + 1, 0);
	}
	//将映射位置的比特位改为1
	void set(size_t x)
	{
		size_t range = x / 8;
		size_t place = x % 8;
		_bs[range] |= (1 << place);
		
	}
	//将映射位置的比特位改为0
	void reset(size_t x)
	{
		size_t range = x / 8;
		size_t place = x % 8;
		_bs[range] &= (~(1 << place));
	}
	//检查映射位置的比特位是否为真
	bool test(size_t x)
	{
		size_t range = x / 8;
		size_t place = x % 8;
		return _bs[range] & (1 << place);
	}
private:
	vector<char> _bs;
};

void test1()
{
	BitSet<1000> bst;
	bst.set(123);
	bst.set(999);
	bst.set(888);
	bst.set(123);

	cout << bst.test(123) << endl;
	cout << bst.test(999) << endl;	
	cout << bst.test(111) << endl;


	bst.reset(123);
	bst.reset(888);

	cout << bst.test(123) << endl;
	cout << bst.test(888) << endl;
	cout << bst.test(999) << endl;

}