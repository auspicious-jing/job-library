#define _CRT_SECURE_NO_WARNINGS 1

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void _mergesort(int* a,int begin,int end,int* tmp)
{
    if (begin >= end)
        return;

    int mid = (begin + end) / 2;
    _mergesort(a, begin, mid, tmp);
    _mergesort(a, mid + 1, end, tmp);

    //归并
    int begin1 = begin, end1 = mid;
    int begin2 = mid + 1, end2 = end;
    int i = begin;
    while (begin1 <= end1 && begin2 <= end2)
    {
        if (a[begin1] <= a[begin2])
        {
            tmp[i++] = a[begin1++];
        }
        else
        {
            tmp[i++] = a[begin2++];
        }
    }
    while (begin1 <= end1)
    {
        tmp[i++] = a[begin1++];
    }
    while (begin2 <= end2)
    {
        tmp[i++] = a[begin2++];
    }

    //拷贝回源数据
    memcpy(a + begin, tmp + begin, (end - begin + 1) * sizeof(int));

}

void MergeSort(int* a,int sz)
{
    int* tmp = (int*)malloc(sz * sizeof(int));
    if (tmp == NULL)
    {
        perror("malloc fail");
    }

    int begin = 0, end = sz - 1;
    _mergesort(a,begin,end,tmp);

    free(tmp);
    tmp = NULL;

}

int main()
{
    int arr[] = { 10,6,7,1,3,9,4,2,5,8};
    int sz = sizeof(arr) / sizeof(arr[0]);

    MergeSort(arr,sz);

    for (int i = 0; i < sz; i++)
    {
        printf("%d ", arr[i]);
    }
    printf("\n");
    return 0;
}










//void _mergesort(int* a, int begin, int end, int* tmp)
//{
//    if (begin >= end)
//        return;
//
//    int mid = (begin + end) / 2;
//    _mergesort(a, begin, mid, tmp);
//    _mergesort(a, mid + 1, end, tmp);
//
//    //归并
//    int begin1 = begin, end1 = mid;
//    int begin2 = mid + 1, end2 = end;
//    int i = begin;
//    while (begin1 <= end1 && begin2 <= end2)
//    {
//        if (a[begin1] <= a[begin2])
//        {
//            tmp[i++] = a[begin1++];
//        }
//        else
//        {
//            tmp[i++] = a[begin2++];
//        }
//    }
//    while (begin1 <= end1)
//    {
//        tmp[i++] = a[begin1++];
//    }
//    while (begin2 <= end2)
//    {
//        tmp[i++] = a[begin2++];
//    }
//
//    //拷贝回原数组
//    memcpy(a + begin, tmp + begin, (end - begin + 1) * sizeof(int));
//
//}
//void MergeSort(int* a, int sz)
//{
//
//    int* tmp = (int*)malloc(sz * sizeof(int));
//    if (tmp == NULL)
//    {
//        perror("malloc fail");
//        return;
//    }
//    int begin = 0;
//    int end = sz - 1;
//    _mergesort(a, begin, end, tmp);
//
//    free(tmp);
//    tmp = NULL;
//
//}