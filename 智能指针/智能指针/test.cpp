#define _CRT_SECURE_NO_WARNINGS 1

#include <iostream>

using namespace std;


//int div()
//{
//	int a, b;
//	cin >> a >> b;
//	if (b == 0)
//		throw invalid_argument("除0错误");
//	return a / b;
//}
//void Func()
//{
//	// 1、如果p1这里new 抛异常会如何？
//	// 2、如果p2这里new 抛异常会如何？
//	// 3、如果div调用这里又会抛异常会如何？
//	int* p1 = new int;
//	int* p2 = new int;
//	cout << div() << endl;
//	delete p1;
//	delete p2;
//}
//int main()
//{
//	try
//	{
//		Func();
//	}
//	catch (exception& e)
//	{
//		cout << e.what() << endl;
//	}
//	return 0;
//}
//template<class T>
//class SmartPtr
//{
//public:
//	//RAII
//	SmartPtr(T* ptr = nullptr)
//		:_ptr(ptr)
//	{}
//	~SmartPtr()
//	{
//		cout << _ptr << endl;
//		if(_ptr)
//			delete _ptr;
//	}
//	//像指针一样
//	T& operator*()
//	{
//		return *_ptr;
//	}
//	T* operator->()
//	{
//		return _ptr;
//	}
//	T& operator[](const int n)
//	{
//		return *(_ptr + n);
//	}
//private:
//	T* _ptr;
//};
//int div()
//{
//	int a, b;
//	cin >> a >> b;
//	if (b == 0)
//		throw invalid_argument("除0错误");
//	return a / b;
//}
//void Func()
//{
//	SmartPtr<int> sp1(new int);
//	SmartPtr<int> sp2(new int);
//
//	*sp2 = 10;
//	sp2[0]--;
//
//	cout << *sp2 << endl;
//
//	cout << div() << endl;
//	
//}
//int main()
//{
//	try
//	{
//		Func();
//	}
//	catch (exception& e)
//	{
//		cout << e.what() << endl;
//	}
//	return 0;
//}
#include "SmartPtr.h"


//int main()
//{
//	/*byte::auto_ptr<int> sp1(new int);
//	byte::auto_ptr<int> sp2(sp1);*/
//
//	/*unique_ptr<int> up1(new int);
//	unique_ptr<int> up2(up1);*/
//
//	byte::shared_ptr<int> sp1(new int(1));
//	byte::shared_ptr<int> sp2(sp1);
//	cout << (*sp2) << endl;
//
//	byte::shared_ptr<int> sp3;
//	sp3 = sp1;
//
//	byte::shared_ptr<int> sp4(new int(1));
//
//
//
//	return 0;
//}


int main()
{
	byte::test_shared_ptr5();
	


	return 0;
}