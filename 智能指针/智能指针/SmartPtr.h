#pragma once

#include <thread>
#include <mutex>
#include <memory>

namespace byte
{
	template<class T>
	class auto_ptr
	{
	public:
		//RAII
		auto_ptr(T* ptr = nullptr)
			:_ptr(ptr)
		{}
		~auto_ptr()
		{
			cout << _ptr << endl;
			if (_ptr)
				delete _ptr;
		}
		auto_ptr(auto_ptr<T>& sp)
			:_ptr(sp._ptr)
		{
			sp._ptr = nullptr;
		}
		//像指针一样
		T& operator*()
		{
			return *_ptr;
		}
		T* operator->()
		{
			return _ptr;
		}
		T& operator[](const int n)
		{
			return *(_ptr + n);
		}
	private:
		T* _ptr;
	};

	template<class T>
	class unique_ptr
	{
	public:
		//RAII
		unique_ptr(T* ptr = nullptr)
			:_ptr(ptr)
		{}
		~unique_ptr()
		{
			cout << _ptr << endl;
			if (_ptr)
				delete _ptr;
		}
		unique_ptr(const unique_ptr<T>& up) = delete;
		
		//像指针一样
		T& operator*()
		{
			return *_ptr;
		}
		T* operator->()
		{
			return _ptr;
		}
		T& operator[](const int n)
		{
			return *(_ptr + n);
		}
	private:
		T* _ptr;
	};

	template<class T>
	class defualt_delete
	{
	public:
		void operator()(T* ptr)
		{
			delete ptr;
			cout << "delete :" << ptr << endl;
		}
	};
	template<class T,class D = defualt_delete<T>>
	class shared_ptr
	{
	public:
		//RAII
		shared_ptr(T* ptr = nullptr)
			:_ptr(ptr)
			,_pcount(new int(1))
			,_pmutex(new mutex)
		{}
		~shared_ptr()
		{
			release();
		}
		shared_ptr(const shared_ptr<T>& sp)
			:_ptr(sp._ptr)
			,_pcount(sp._pcount)
			, _pmutex(sp._pmutex)
		{
			_pmutex->lock();
			++(*_pcount);
			_pmutex->unlock();

		}
		//sp1 = sp2
		shared_ptr<T>& operator=(const shared_ptr<T>& sp)
		{
			if (sp._ptr != _ptr)
			{
				release();
				_ptr = sp._ptr;
				_pcount = sp._pcount;
				_pmutex = sp._pmutex;

				_pmutex->lock();
				++(*_pcount);
				_pmutex->unlock();
			}
			return *this;
		}
		void release()
		{
			bool flag = false;
			_pmutex->lock();
			if (--(*_pcount) == 0)
			{
				//delete _ptr;
				_del(_ptr);//可以用我们传的方式释放空间
				delete _pcount;
				flag = true;
			}
			_pmutex->unlock();
			if (flag)
			{
				delete _pmutex;
			}
		}
		int Pcount()const
		{
			return *_pcount;
		}
		T* getPtr()const
		{
			return _ptr;
		}
		//像指针一样
		T& operator*()
		{
			return *_ptr;
		}
		T* operator->()
		{
			return _ptr;
		}
		T& operator[](const int n)
		{
			return *(_ptr + n);
		}
	private:
		T* _ptr;
		int* _pcount;
		mutex* _pmutex;
		D _del;
	};
	/*void test_shared_ptr1()
	{
		int n = 10000;
		shared_ptr<int> sp1(new int(1));

		thread t1([&]() 
			{
				for (int i = 0; i < n; ++i)
				{
					shared_ptr<int> sp2(sp1);
				}
			});

		thread t2([&]() 
			{
				for (int i = 0; i < n; ++i)
				{
					shared_ptr<int> sp3(sp1);
				}
			});

		
		t1.join();
		t2.join();

		cout << sp1.Pcount() << endl;
		cout << sp1.getPtr() << endl;

	}*/
	struct Date
	{
		int _year = 0;
		int _month = 0;
		int _day = 0;
	};
	void test_shared_ptr2()
	{
		int n = 500000;
		shared_ptr<Date> sp1(new Date);
		mutex mtx;

		thread t1([&]()
			{
				for (int i = 0; i < n; ++i)
				{
					shared_ptr<Date> sp2(sp1);
					mtx.lock();
					sp2->_day++;
					sp2->_month++;
					sp2->_year++;
					mtx.unlock();

				}
			});

		thread t2([&]()
			{
				for (int i = 0; i < n; ++i)
				{
					shared_ptr<Date> sp3(sp1);
					mtx.lock();
					sp3->_day++;
					sp3->_month++;
					sp3->_year++;
					mtx.unlock();
				}
			});

		t1.join();
		t2.join();
		cout << sp1.Pcount() << endl;
		cout << sp1->_day++ << endl;
		cout << sp1->_month++ << endl;
		cout << sp1->_year++ << endl;

	}
	template<class T>
	class weak_ptr
	{
	public:
		weak_ptr(T* ptr = nullptr)
			:_ptr(ptr)
		{}
		weak_ptr(const shared_ptr<T>& sp)
			:_ptr(sp.getPtr())
		{}
		/*weak_ptr<T>& operator=(const shared_ptr<T>& sp)
		{

		}*/
	/*	T& operator*()
		{
			return *_ptr;
		}
		T* operator->()
		{
			return _ptr;
		}
		T& operator[](const int n)
		{
			return *(_ptr + n);
		}*/
	private:
		T* _ptr;
	};


	struct ListNode
	{
		int _data = 0;
		weak_ptr<ListNode> _prev;
		weak_ptr<ListNode> _next;
		~ListNode() { cout << "~ListNode()" << endl; }

	};
	void test_shared_ptr3()
	{
		shared_ptr<ListNode> sp1(new ListNode);
		shared_ptr<ListNode> sp2(new ListNode);

		sp2->_next = sp1;
		sp1->_prev = sp2;

		cout << sp1.Pcount() << endl;
		cout << sp2.Pcount() << endl;

	}
	//仿函数
	template<class T>
	struct Delete
	{
		void operator()(const T* ptr)
		{
			delete[] ptr;
			cout << "delete[] :" << ptr << endl;
		}
	};
	void test_shared_ptr4()
	{
		std::shared_ptr<int> sp1(new int[10],Delete<int>());
		std::shared_ptr<string> sp2(new string[10], Delete<string>());

		std::shared_ptr<string> sp3(new string[10], [](string* ptr) {delete[] ptr; cout << "lambda delete[] :" << ptr << endl; });
		std::shared_ptr<FILE> sp4(fopen("test.cpp","r"), [](FILE* ptr) {fclose(ptr); cout << "fclose() :" << ptr << endl; });

	}
	void test_shared_ptr5()
	{
		shared_ptr<int> sp1(new int);
		shared_ptr<string> sp2(new string);
		shared_ptr<int, Delete<int>> sp3(new int[10]);
		shared_ptr<string, Delete<string>> sp4(new string[10]);

	}

}