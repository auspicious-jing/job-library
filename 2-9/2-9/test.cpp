#define _CRT_SECURE_NO_WARNINGS 1

#include <map>
#include <string>


#include <iostream>

using namespace std;



int main()
{
	string arr[] = {"ƻ��"};
	map<string, int> countmap;
	for (auto& e : arr)
	{
		auto it = countmap.find(e);
		if (it == countmap.end())
		{
			countmap.insert(make_pair(e, 1));
		}
		else
		{
			it->second++;
		}
	}
	for ( auto& e: arr)
	{
		countmap[e]++;
	}

	for (const auto& KV : countmap)
	{
		cout << KV.first << ':' << KV.second << endl;
	}

	return 0;
}