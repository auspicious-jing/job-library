#define _CRT_SECURE_NO_WARNINGS 1




#include <stdio.h>


////下面代码的结果是什么？
//int sum(int a)
//{
//    int c = 0;
//    static int b = 3;
//    c += 1;//c=1,
//    b += 2;//b=5,7
//    return (a + b + c);//2+5+1=8,10,12,14,16
//}
//int main()
//{
//    int i;
//    int a = 2;
//    for (i = 0; i < 5; i++)
//    {
//        printf("%d,", sum(a));
//    }
//}

//int main()
//{
//    printf("printf(\"Hello world!\\n\");");
//    printf("puts(cout >> \"Hello world!\");");
//    return 0;
//}
//
//int main()
//{
//    int a1 = 0;
//    int a2 = 0;
//    int a3 = 0;
//    int a4 = 0;
//
//    //输入
//    scanf("%d %d %d %d",&a1,&a2,&a3,&a4);
//    //计算
//    int k = 0;
//    if (a1 < a2)
//    {
//        k = a1;
//        a1 = a2;
//        a2 = k;
//    }
//    if (a3 < a4)
//    {
//        k = a3;
//        a3 = a4;
//        a4 = k;
//    }
//    if (a1 < a3)
//    {
//        k = a1;
//        a1 = a3;
//        a3 = k;
//    }
//    printf("%d\n", a1);
//    return 0;
//}
//
//int main()
//{
//    int arr[4] = { 0 };
//    //输入
//    scanf("%d %d %d %d", &arr[0], &arr[1], &arr[2], &arr[3]);
//    //计算
//    int i = 0;
//    for (i = 1; i < 4; i++)
//    {
//        int j = 0;
//        if (arr[0] < arr[i])
//        {
//            j = arr[0];
//            arr[0] = arr[i];
//            arr[i] = j;
//
//        }
////    }
////    printf("%d\n", arr[0]);
////    return 0;
//}
//
//int main()
//{
//    double r = 0.0;
//    double v = 0.0;
//    double π = 3.1415926;
//
//    //输入
//    scanf("%lf", &r);
//    //计算
//    v = 4.0 / 3.0 * π * (r * r * r);
//    //输出
//    printf("%.3f\n",v);
//    return 0;
//}

int main()
{
    int k = 0.0;
    int c = 0.0;
    float b = 0.0;
    //输入
    scanf("%d %d", &k, &c);
    //计算
    b = k/((c/100.0)*(c/100.0));
    //输出
    printf("%.2f\n", b);
    return 0;
}