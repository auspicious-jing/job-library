#define _CRT_SECURE_NO_WARNINGS 1


#include <iostream>
using namespace std;

//class Person 
//{
//public:
//	//虚函数重写    三同(函数名，参数，返回类型)
//	virtual void BuyTicket() { cout << "Person->买票-全价" << endl; }
//};
//class Student : public Person 
//{
//public:
//	virtual void BuyTicket() { cout << "Student->买票-半价" << endl; }
//};
////多态的条件：
////1.虚函数重写
////2.父类指针或引用调用虚函数
//void func(Person& p)
//{
//	p.BuyTicket();
//}
//
//int main()
//{
//	//普通调用：跟调用的类型有关
//	//多态调用：跟指向的对象有关
//	Person pn;
//	Student st;
//
//	func(pn);
//	func(st);
//
//	return 0;
//}


////这里建议父类的析构函数都加上virtual，如果有申请的内存，不加可能会导致内存泄漏
//class Person 
//{
//public:
//	virtual ~Person() { cout << "~Person()" << endl; }
//};
//class Student : public Person 
//{
//public:
//	virtual ~Student() { cout << "~Student()" << endl; }
//};
//// 只有派生类Student的析构函数重写了Person的析构函数，下面的delete对象调用析构函
////数，才能构成多态，才能保证p1和p2指向的对象正确的调用析构函数。
//int main()
//{
//	Person* p1 = new Person;
//	Person* p2 = new Student;
//	delete p1;
//	delete p2;
//
//	return 0;
//}
//如何实现一个不被继承的类
// 1.构造私有 C++98
// 2.类定义时加final C++11
//class Car final
//{
//public:
//	virtual void Drive()  {}
//};
//class Benz :public Car
//{
//public:
//	virtual void Drive() { cout << "Benz-舒适" << endl; }
//};
// class Car
//{
//public:
//	virtual void Drive() final {}
//};
//class Benz :public Car
//{
//public:
//	virtual void Drive() { cout << "Benz-舒适" << endl; }
//};
//// 
class Car {
public:
	 void Drive() {}
};
class Benz :public Car {
public:
	virtual void Drive() override { cout << "Benz-舒适" << endl; }
};

int main()
{
	Benz b;

	return 0;
}



