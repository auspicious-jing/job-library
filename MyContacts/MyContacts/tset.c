#define _CRT_SECURE_NO_WARNINGS 1

#include "Contacts.h"

void menu()
{
	//1.增加 2.删除     3.查找 4.修改
	//5.显示 6.清除 7.排序 0.退出
	printf("*****************************************\n");
	printf("********  1.Add      2.Del       ********\n");
	printf("********  3.Search   4.Modify    ********\n");
	printf("********  5.Show     6.Cleared   ********\n");
	printf("********  7.Sort     0.Exit      ********\n");
	printf("*****************************************\n");

}
int main()
{
	int input = 0;

	Contact con;
	//初始化通讯录
	InitContact(&con);
	do
	{
		menu();
		printf("请选择:>");
		scanf("%d", &input);
		switch (input)
		{
		case 1:
			AddContact(&con);
			break;
		case 2:
			DelContact(&con);
			break; 
		case 3:
			SearchContact(&con);
			break; 
		case 4:
			break; 
		case 5:
			ShowContact(&con);
			break; 
		case 6:
			break; 
		case 7:
			break; 
		case 0:
			printf("退出通讯录\n");
			break;
		default :
			printf("选择错误！请重新选择\n");
			break;
		}

	} while (input);

	return 0;
}