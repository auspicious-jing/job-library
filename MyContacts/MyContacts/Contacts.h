#pragma once

#include <stdio.h>
#include <string.h>
#include <assert.h>



#define MAX 1000
#define PEO_NAME 20
#define PEO_SEX 10
#define PEO_TELE 12
#define PEO_ADDRESS 20



//人的信息
typedef struct PeoInfor
{
	char name[PEO_NAME];
	int age;
	char sex[PEO_SEX];
	char tele[PEO_TELE];
	char address[PEO_ADDRESS];
}PeoInfor;


//通讯录
typedef struct Contact
{
	//存放人的信息
	PeoInfor date[MAX];
	int count;
}Contact;

//初始化通讯录
void InitContact(Contact* pc);

//增加联系人
void AddContact(Contact* pc);

//删除联系人
void DelContact(Contact* pc);

//查找联系人
void SearchContact(Contact* pc);

//显示联系人
void ShowContact(const Contact* pc);
