#define _CRT_SECURE_NO_WARNINGS 1

#include <stdio.h>

//实现函数判断year是不是润年。
//
//int leap_year(int year)
//{
//	if (year % 4 == 0 && year % 10 != 0 || year % 400 == 0)
//	{
//		return 1;
//	}
//	return 0;
//}
//int main()
//{
//
//	int year = 0;
//	//输入
//	scanf("%d", &year);
//	
//
//	if (leap_year(year))//是闰年为真，就打印是闰年
//	{
//		printf("%d 是闰年\n", year);
//	}
//	else //(leap_year(year))//否则为假，打印不是闰年
//	{
//		printf("%d 不是闰年\n", year);
//	}
//	return 0;
//}


//实现一个函数，判断一个数是不是素数。
//利用上面实现的函数打印100到200之间的素数。
//#include <math.h>
//int prime_num(int num)
//{
//	int j = 0;
//	for (j = 2; j <= sqrt(num); j++)
//	{
//		if (num % j == 0)
//		{
//			return 0;
//		}
//
//	}
//	return 1;
//}
//
//int main()
//{
//	int i = 0;
//	for (i = 100; i <= 200; i++)
//	{
//		if (prime_num(i))
//		{
//			printf("%d ", i);
//		}
//
//
//	}
//	return 0;
//}

//
//
//int Fun(int n)//2  3  4 5
//{
//    if (n == 5)
//        return 2;
//    else
//        return 2 * Fun(n + 1);//2*2*2*2
//}




//递归方式实现打印一个整数的每一位

//void print(unsigned int num)
//{
//	if (num > 9)
//	{
//		print(num / 10);
//	}
//	printf("%d ", num % 10);
//}
//
//int main()
//{
//	unsigned int num = 0;
//	scanf("%u", &num);
//
//	print(num);
//
//	return 0;
//}



//递归实现求n的阶乘（不考虑溢出的问题）

//int fac(int n)
//{
//	if (n <= 1)
//	{
//		return 1;
//	}
//	return n * fac(n - 1);
//}
//
//int main()
//{
//	int n = 0;
//	scanf("%d", &n);
//	//int ret = fac(n);
//	
//	printf("%d\n", fac(n));
//	return 0;
//}


////非递归实现求n的阶乘（不考虑溢出的问题）
//int fac(int n)
//{
//	int ret = 1;
//	int i = 0;
//	for (i = 1; i <= n; i++)
//	{
//		ret *= i;
//	}
//	return ret;
//}
//int main()
//{
//	int n = 0;
//	scanf("%d", &n);
//
//	printf("%d\n", fac(n));
//	return 0;
//}


//递归实现strlen

//int str(char *parr)
//{
//	if (*parr != '\0')
//	{
//		return 1 + str(parr + 1);
//	}
//	else
//		return 0;
//}
//int main()
//{
//	char arr[] = "abcde";
//	int ret = str(arr);
//
//	printf("%d\n", ret);
//	return 0;
//}

//非递归实现strlen

//int str(char* parr)
//{
//	int count = 0;
//	while (*parr != '\0')
//	{
//		count++;
//		parr++;
//	}
//	return count;
//}
//int main()
//{
//	char arr[] = "abcd";
//
//	int ret = str(arr);
//	printf("%d\n", ret);
//	return 0;
//}


//写一个递归函数DigitSum(n)，输入一个非负整数，返回组成它的数字之和

//int DigitSum(unsigned int num)
//{
//
//	if (num <= 9)
//	{
//		return num;
//	}
//	else
//	{
//		return num % 10 + DigitSum(num / 10);
//	}
//}
//int main()
//{
//	unsigned int num = 0;
//	scanf("%u", &num);
//
//
//	int ret = DigitSum(num);
//
//	printf("%d\n", ret);
//	return 0;
//}


//编写一个函数实现n的k次方，使用递归实现。

int Power(int n, int k)
{
	if (k <= 1)
	{
		return n;
	}
	else
		return n * Power(n,k-1);
}

int main()
{
	int n = 0;
	int k = 0;
	scanf("%d %d", &n,&k);//n是某个数，k是n的次方，如2的6次方
	int ret = Power(n, k);

	printf("%d\n", ret);
	return 0;
}