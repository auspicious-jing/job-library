#pragma once

#include <iostream>
#include <assert.h>
using namespace std;

namespace bit
{
	class string
	{
		friend ostream& operator<<(ostream& _cout, const bit::string& s);
		friend istream& operator>>(istream& _cin, bit::string& s);
	public:
		typedef char* iterator;
	public:
		//构造函数
		string(const char* str = "")
		{
			_size = strlen(str);
			_capacity = _size;
			_str = new char[_capacity + 1];
			strcpy(_str, str);

		}
		////拷贝构造
		//string(const string& s)
		//{
		//	_size = s._size;
		//	_capacity = s._capacity;
		//	_str = new char[_capacity + 1];
		//	strcpy(_str, s._str);

		//}
		//拷贝构造,现代写法s(s1)
		string(const string& s)
			:_str(nullptr)
			,_size(0)
			,_capacity(0)
		{
			string tmp(s._str);
			swap(tmp);
		}
		//移动构造
		string(string&& s)
			:_str(nullptr)
			, _size(0)
			, _capacity(0)
		{
			
			swap(s);
		}
		////赋值重载
		//string& operator=(const string& s)
		//{
		//	if (this != &s)
		//	{
		//		_capacity = s._capacity;
		//		_size = s._size;
		//		char* tmp = new char[_capacity + 1];
		//		strcpy(tmp, s._str);
		//		delete[] _str;
		//		_str = tmp;
		//	}
		//	return *this;
		//}
		////赋值重载,现代写法
		//string& operator=( string s)
		//{
		//	
		//	return s;
		//}
		 //赋值重载,现代写法
		string& operator=(const string& s)
		{
			if (this != &s)
			{
				string tmp(s);
				swap(tmp);
			}
			return *this;
		}
		//移动赋值
		string& operator=(string&& s)
		{			
			swap(s);
			
			return *this;
		}
		//析构函数
		~string()
		{
			delete[] _str;
			_str = nullptr;
			_size = _capacity = 0;

		}

		//iterator 迭代器
		iterator begin()
		{
			return _str;
		}
		iterator end()
		{
			return _str + _size;
		}
		
		//modify
		//尾插字符
		void push_back(char c)
		{
			//扩容
			if (_size == _capacity)
			{
				size_t new_capacity = _capacity == 0 ? 4 : _capacity * 2;
				reserve(new_capacity);
			}
			_str[_size] = c;
			++_size;
			_str[_size] = '\0';

		}
		string& operator+=(char c)
		{
			push_back(c);
			return *this;
		}
		//追加字符串
		void append(const char* str)
		{
			size_t len = strlen(str);


			if (len + _size > _capacity)
				reserve(len + _size);
			strcat(_str, str);
			_size += len;
		}
		string& operator+=(const char* str)
		{
			append(str);
			return *this;
		}
		//清理数据
		void clear()
		{
			_size = 0;
			_str[0] = '\0';
		}
		//交换
		void swap(string& s)
		{
			std::swap(_str, s._str);
			std::swap(_size, s._size);
			std::swap(_capacity, s._capacity);
		}
		//返回C格式的字符串
		const char* c_str()const
		{
			return _str;
		}

		//capacity
		//字符串长度
		size_t size()const
		{
			return _size;
		}
		//容量
		size_t capacity()const
		{
			return _capacity;
		}
		//判断是否为空
		bool empty()const
		{
			if (_size == 0)
				return false;
			else
				return true;
		}
		//将有效字符的个数该成n个，多出的空间用字符c填充
		void resize(size_t n, char c = '\0')
		{
			if (n < _size)
			{
				_size = n;
				_str[_size] = '\0';
			}
			else if (n > _size)
			{
				if (n > _capacity)
				{
					//扩容
					reserve(n);
				}
				for (size_t i = _size; i < n; ++i)
				{
					_str[i] = c;
				}
				_size = n;
				_str[_size] = '\0';
			}
		}
		//为字符串预留n个空间
		void reserve(size_t n)
		{
			if (n > _capacity)
			{
				char* ret = new char[n + 1];
				strcpy(ret, _str);
				delete[]_str;
				_str = ret;
				_capacity = n;
			}
		}

		//access
		char& operator[](size_t index)
		{
			assert(index < _size);
			return _str[index];
		}
		const char& operator[](size_t index)const
		{
			assert(index < _size);

			return _str[index];
		}

		////relational operators
		bool operator<(const string& s)
		{
			assert(s._str);
			int ret = strcmp(_str, s._str);
			if (ret < 0)
				return true;
			else
				return false;
		}
		bool operator<=(const string& s)
		{
			assert(s._str);
			return (_str == s._str)|| (_str < s._str);
		}
		bool operator>(const string& s)
		{
			assert(s._str);
			int ret = strcmp(_str, s._str);
			if (ret > 0)
				return true;
			else
				return false;
		}
		bool operator>=(const string& s)
		{
			return (_str == s._str) || (_str > s._str);
		}
		bool operator==(const string& s)
		{
			assert(s._str);
				int ret = strcmp(_str, s._str);
				if (ret == 0)
					return true;
				else
					return false;
		}
		bool operator!=(const string& s)
		{
			return !(_str == s._str);

		}

		// 返回c在string中第一次出现的位置
		size_t find(char c, size_t pos = 0)const
		{
			assert(pos < _size);

			for (size_t i = pos; i < _size; ++i)
			{
				if (_str[i] == c)
					return i;
			}
			return npos;
		}
		// 返回子串s在string中第一次出现的位置
		size_t find(const char* s, size_t pos = 0)const
		{
			assert(pos < _size);
			const char* tmp = strstr(_str + pos, s);
			if (tmp == nullptr)
				return npos;
			else
				return tmp - _str;
		}
		// 在pos位置上插入字符c/字符串str，并返回该字符的位置
		string& insert(size_t pos, char c)
		{
			assert(pos <= _size);
			if (_size == _capacity)
			{
				size_t new_capacity = _capacity == 0 ? 4 : _capacity * 2;
				reserve(new_capacity);
			}
			size_t end = _size + 1;
			while (pos < end)
			{
				_str[end] = _str[end - 1];
				--end;
			}
			_str[pos] = c;
			++_size;
			return *this;
		}
		string& insert(size_t pos, const char* str)
		{
			assert(pos <= _size);
			size_t len = strlen(str);
			if (_size + len > _capacity)
			{

				reserve(_size + len);
			}
			size_t end = _size + len;
			while (pos < end - len + 1)
			{
				_str[end] = _str[end - len];
				--end;
			}
			strncpy(_str + pos, str, len);
			_size += len;
			return *this;
		}

		// 删除pos位置上的元素，并返回该元素的下一个位置
		string& erase(size_t pos, size_t len = npos)
		{
			assert(pos < _size);
			if (len == npos || len >= _size - pos)
			{
				_str[pos] = '\0';
				_size = pos;
			}
			else
			{
				strcpy(_str + pos, _str + pos + len);
				_size -= len;
			}
			return *this;
		}
	private:
		char* _str;
		size_t _size;
		size_t _capacity;
		static const size_t npos = -1;
	};
	//流插入
	ostream& operator<<(ostream& _cout, const bit::string& s)
	{
		for (size_t i = 0; i < s.size(); ++i)
		{
			_cout << s[i];
		}
		return _cout;
	}
	//流提取
	istream& operator>>(istream& _cin, bit::string& s)
	{
		s.clear();

		char buff[128] = { '\0' };
		size_t i = 0;
		char ch = _cin.get();
		while (ch != ' ' && ch != '\n')
		{
			if (i == 127)
			{
				s += buff;
				i = 0;
			}
			buff[i++] = ch;
			ch = _cin.get();
		}
		if (i > 0)
		{
			buff[i] = '\0';
			s += buff;
		}
		return _cin;
	}

	void test1()
	{
		string s("hello world");
		cout << s.c_str() << endl;

		for (size_t i = 0; i < s.size(); ++i)
		{
			s[i]--;
		}
		cout << s.c_str() << endl;
		//迭代器
		string::iterator it1 = s.begin();
		while (it1 != s.end())
		{
			(*it1)++;
			++it1;
		}
		cout << s.c_str() << endl;
		for (auto ch : s)
		{
			cout << ch << ' ';
		}
		cout << endl;
	}
	void test2()
	{
		string s1("hello world");
		string s("hello ");

		s1.push_back('m');
		s1 += 'e';
		cout << s1.c_str() << endl;

		s.append("world");
		s += "hello";
		cout << s.c_str() << endl;
	}
	void test3()
	{
		string s("helloworld");

		s.insert(5, ' ');
		cout << s.c_str() << endl;
		s.insert(0, "");
		cout << s.c_str() << endl;
		s.insert(11, 'x');
		cout << s.c_str() << endl;

		string s1;
		s1.insert(0, "hello");
		cout << s1.c_str() << endl;
	}
	void test4()
	{
		string s("hello hello world");
		s.erase(6, 6);
		cout << s.c_str() << endl;

		s.erase(5);
		cout << s.c_str() << endl;

	}
	void test5()
	{
		string s("hello world");
		size_t pos = s.find('w');
		s.insert(pos, "hello ");
		cout << s.c_str() << endl;

		pos = s.find("hello");
		s.erase(pos, 6);
		cout << s.c_str() << endl;
	}
	void test6()
	{
		string s("hello world");
		cout << s.c_str() << endl;

		s.resize(5);
		cout << s.size() << endl;
		cout << s.capacity() << endl;
		cout << s.c_str() << endl;

		s.resize(20,'m');
		cout << s.size() << endl;
		cout << s.capacity() << endl;
		cout << s.c_str() << endl;

		s.resize(5);
		s.resize(15, 'x');
		cout << s.size() << endl;
		cout << s.capacity() << endl;
		cout << s.c_str() << endl;
	}
	void test7()
	{
		string s("hello world");
		cout << s << endl;
		
		cin >> s;
		cout << s << endl;

		string s2;
		cin >> s2;
		cout << s2 << endl;
	}
	void test8()
	{
		string s("hello world");
		string s1(s);
		cout << s << endl;
		cout << s1 << endl;

	}
	void test9()
	{
		string s("hello world");
		string s1;
		s1.swap(s);
		cout << s << endl;
		cout << s1 << endl;
	}
	void test10()
	{
		string s("hello");
		string s1("asdwad ");
		cout << (s == s1) << endl;
		cout << (s != s1) << endl;
		cout << (s > s1) << endl;
		cout << (s >= s1) << endl;
		cout << (s < s1) << endl;
		cout << (s <= s1) << endl;

	}
	void test11()
	{
		string s("hello");
		string s2(s);
		string s3(move(s));

	}
}
