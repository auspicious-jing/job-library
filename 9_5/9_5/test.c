#define _CRT_SECURE_NO_WARNINGS 1
#include "SLlist.h"


void test1()
{
	SListNode* plist = NULL;
	printf("单链表头插\n");
	SListPushFront(&plist, 1);
	SListPushFront(&plist, 2);
	SListPushFront(&plist, 3);
	SListPushFront(&plist, 4);
	SListPrint(plist);

	printf("头删\n");
	SListPopFront(&plist);
	SListPrint(plist);
	SListPopFront(&plist);
	SListPrint(plist);
	SListPopFront(&plist);
	SListPrint(plist);
	SListPopFront(&plist);
	SListPrint(plist);
	//此时链表为空,没有节点可以删除，直接返回
	SListPopFront(&plist);
	SListPrint(plist);

	printf("单链表销毁\n");
	SListDestroy(&plist);
}
void test2()
{
	SListNode* plist = NULL;
	printf("单链表尾插\n");
	SListPushBack(&plist, 1);
	SListPushBack(&plist, 2);
	SListPushBack(&plist, 3);
	SListPushBack(&plist, 4);
	SListPrint(plist);
	printf("单链表尾删\n");
	SListPopBack(&plist);
	SListPrint(plist);
	SListPopBack(&plist);
	SListPrint(plist);
	SListPopBack(&plist);
	SListPrint(plist);
	SListPopBack(&plist);
	SListPrint(plist);
	//此时链表为空,没有节点可以删除，直接返回
	SListPopBack(&plist);
	SListPrint(plist);
	printf("单链表销毁\n");
	SListDestroy(&plist);

}
void test3()
{
	SListNode* plist = NULL;
	printf("单链表头插\n");
	SListPushFront(&plist, 1);
	SListPushFront(&plist, 2);
	SListPushFront(&plist, 3);
	SListPushFront(&plist, 4);
	SListPrint(plist);

	printf("查找3的位置\n");
	SListNode* pos = SListFind(plist, 3);
	if (pos != NULL)
	{
		pos->data *= 10;
		printf("找到了，值*=10\n");
	}
	else
	{
		printf("没找到\n");
	}
	SListPrint(plist);
	printf("pos位置后面插入30\n");
	SListInsertAfter(pos, 30);
	SListPrint(plist);
	printf("删除pos位置后的值\n");
	SListEraseAfter(pos);
	SListPrint(plist);
	printf("单链表销毁\n");
	SListDestroy(&plist);
}
int main()
{
	
	test3();

	return 0;
}