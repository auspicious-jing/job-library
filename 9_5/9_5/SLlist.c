#define _CRT_SECURE_NO_WARNINGS 1


#include "SLlist.h"

// 动态申请一个节点
SListNode* BuySListNode(SLTDateType x)
{
	SListNode* newnode = (SListNode*)malloc(sizeof(SListNode));

	if (newnode == NULL)
	{
		perror("malloc fail\n");
		exit(-1);
	}

	newnode->data = x;
	newnode->next = NULL;
	
	return newnode;
}
// 单链表打印
void SListPrint(SListNode* plist)
{
	SListNode* cur = plist;

	while (cur != NULL)
	{
		printf("%d->",cur->data);
		cur = cur->next;
	}

	printf("NULL\n");
}

// 单链表尾插
void SListPushBack(SListNode** pplist, SLTDateType x)
{
	assert(pplist);

	SListNode* newnode = BuySListNode(x);

	//1.没有节点
	//2.多个节点
	if (*pplist == NULL)
	{
		*pplist = newnode;
	}
	else
	{
		SListNode* cur = *pplist;
		while (cur->next != NULL)
		{
			cur = cur->next;
		}
		cur->next = newnode;
	}
}

// 单链表的头插
void SListPushFront(SListNode** pplist, SLTDateType x)
{
	assert(pplist);

	SListNode* newnode = BuySListNode(x);

	newnode->next = *pplist;
	*pplist = newnode;
}
// 单链表的尾删
void SListPopBack(SListNode** pplist)
{
	assert(pplist);
	if (*pplist == NULL)
	{
		return;
	}
	if ((*pplist)->next == NULL)
	{
		free(*pplist);
		*pplist = NULL;
	}
	else
	{
		SListNode* del = *pplist;
		while (del->next->next != NULL)
		{
			del = del->next;
		}
		free(del->next);
		del->next = NULL;
	}
}
// 单链表头删
void SListPopFront(SListNode** pplist)
{
	assert(pplist);
	if (*pplist == NULL)
	{
		return;
	}
	
	SListNode* del = *pplist;
	*pplist = del->next;
	free(del);
	del = NULL;
}
// 单链表查找
SListNode* SListFind(SListNode* plist, SLTDateType x) 
{
	SListNode* cur = plist;

	while (cur)
	{
		if (cur->data == x)
		{
			return cur;
		}
		cur = cur->next;
	}
	return NULL;
}
// 单链表在pos位置之后插入x
// 分析思考为什么不在pos位置之前插入？  
//因为单链表找pos前面的位置比较麻烦，而且如果pos是第一个节点，那么还需要修改plist，
//所以不建议在pos之前插入，如果非得前插，那么定义两个指针即可。
void SListInsertAfter(SListNode* pos, SLTDateType x)
{
	assert(pos);
	SListNode* newnode = BuySListNode(x);
	newnode->next = pos->next;
	pos->next = newnode;
}
// 单链表删除pos位置之后的值
// 分析思考为什么不删除pos位置？
//因为删除pos位置后，pos之前的位置找的话比较麻烦
void SListEraseAfter(SListNode* pos)
{
	assert(pos);

	if (pos->next == NULL)
	{
		return;
	}
	SListNode* del = pos->next;
	pos->next = del->next;
	free(del);
}
// 单链表的销毁
void SListDestroy(SListNode** pplist)
{
	assert(pplist);
	
	SListNode* prve = *pplist;
	while (prve != NULL)
	{
		SListNode* cur = prve->next;
		free(prve);
		prve = cur;

	}
	pplist = NULL;
}
