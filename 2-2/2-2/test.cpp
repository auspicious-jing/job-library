#define _CRT_SECURE_NO_WARNINGS 1


#include <iostream>

using namespace std;


//class A
//{
//public:
//	int _a;
//};
//class B :public A
//{
//public:
//	int _b;
//};
//class C :public A
//{
//public:
//	int _c;
//};
//class D :public B,public C
//{
//public:
//	int _d;
//};
class A
{
public:
	int _a;
};
class B :virtual public A
{
public:
	int _b;
};
class C :virtual public A
{
public:
	int _c;
};
class D :public B, public C
{
public:
	int _d;
};
int main()
{
	D d;
	d._b = 1;
	d._c = 2;
	d._d = 3;
	d.B::_a = 4;
	d.C::_a = 5;
	d._a = 6;
	return 0;
}