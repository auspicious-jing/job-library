#define _CRT_SECURE_NO_WARNINGS 1

#include <stdio.h>

//有一个数字矩阵，矩阵的每行从左到右是递增的，矩阵从上到下是递增的，请编写程序在这样的矩阵中查找某个数字是否存在。
int Y_matrix(int arr[5][5], int k, int row, int col)
{
	if (k >= arr[0][0] && k <= arr[4][4])
	{
		int i = 0;
		for (i = 0; i < col; i++)
		{
			if (k == arr[i][4])
			{
				return 1;
			}
			else if (k < arr[i][4])
			{
				int j = 0;
				for (j = 0; j < row; j++)
				{
					if (k == arr[i][j])
						return 1;
				}
			}
		}
	}
		return 0;
}

int main()
{
	int arr[5][5] = { 1, 2, 3, 4, 5,
					  6, 7, 8, 9, 10,
					  11,12,13,14,15,
					  16,17,18,19,20,
					  21,22,23,24,25 };
	int k = 0;
	scanf("%d", &k);
	int ret = Y_matrix(arr,k,5,5);
	if (ret != 0)
		printf("存在\n");
	else
		printf("不存在\n");
	return 0;
}