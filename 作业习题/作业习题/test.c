#define _CRT_SECURE_NO_WARNINGS 1

//#include <stdio.h>
//
//int main()
//{
//    int n = 0;
//    int i = 0;
//    int j = 0;
//    //输入
//    while (scanf("%d", &n) != EOF)
//    {
//        for (i = 0; i < n; i++)
//        {
//
//            for (j = 0; j < n; j++)
//            {
//                if (i == 0 || i == n - 1 || j == 0 || j == n - 1)
//                {
//                    printf("* ");
//                }
//                else
//                    printf("  ");
//            }
//            printf("\n");
//        }
//    }
//
//
//    return 0;
//}


//int main()
//{
//    输入
//    long int n, m;
//    scanf("%d %d", &n, &m);
//
//    计算最大公约数
//    long int a, b, c;
//    a = n;
//    b = m;
//    while (c = a % b)
//    {
//        a = b;
//        b = c;
//    }
//    最小公倍数
//    c = n * m / b;
//    printf("%ld\n", c + b);
//    return 0;
//}
#include <stdio.h>

int main()
{
    int n = 0;
    int i = 0;
    int j = 0;

    while (scanf("%d", &n)!=EOF)
    {
        for (i = 0; i < n + 1; i++)
        {

            for (j = n * 2 - i; j > 0 + i; j--)
            {
                printf(" ");
            }
            for (j = 0; j <= i; j++)
            {
                printf("*");
            }
            printf("\n");
        }
        for (i = 0; i < n; i++)
        {
            for (j = 0; j < 2 + i * 2; j++)
            {
                printf(" ");
            }
            for (j = 0; j < n - i; j++)
            {
                printf("*");
            }
            printf("\n");
        }
    }

    return 0;
}

#include <stdio.h>

int main()
{
    int n = 0;
    //输入int i = 0;
    int max = 0;
    int mix = 100;
    int ret = 0;
    while (scanf("%d", &n) != EOF)
    {

        if (max < n)
        {
            max = n;
        }
        if (mix > n)
        {
            mix = n;
        }

        ret += n;
        i++;
        if (i == 7)
        {
            printf("%.2f\n", (ret - max - mix) / 5.0);
            i = 0;
            max = 0;
            mix = 100;
            ret = 0;
        }
    }

    return 0;
}