#define _CRT_SECURE_NO_WARNINGS 1

//#include <iostream>
//
//using namespace std;
//
//int main()
//{
//    string s;
//    string str;
//    char c = 0;
//    c = getchar();
//    while (c != '\n')
//    {
//        str += c;
//        c = getchar();
//    }
//    c = getchar();
//    while (c != '\n')
//    {
//        s += c;
//        c = getchar();
//    }
//    for (size_t i = 0; i < str.size()-1; ++i)
//    {
//        for (size_t j = 0; j < s.size()-1; ++j)
//        {
//            if (str[i] == s[j])
//            {
//                str.erase(i, 1);
//            }
//            
//        }
//    }
//    cout << str << endl;
//    return 0;
//}


#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;

int main()
{
    int n = 0;
    cin >> n;
    vector<int> arr;
    arr.resize(n * 3);
    for (int i = 0; i < n*3; ++i)
    {
        cin >> arr[i];
    }
    sort(arr.begin(), arr.end());
    long num = 0;
    for (int i = 0; i < n; ++i)
    {
        num += arr[arr.size() - 2 * (i + 1)];
    }
    cout << num;
    return 0;
}