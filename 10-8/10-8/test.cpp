#define _CRT_SECURE_NO_WARNINGS 1

#include <iostream>

using namespace std;

//// 类中既有成员变量，又有成员函数
//class A1 {
//public:
//    void f1() {}
//private:
//    int _a;
//};
//// 类中仅有成员函数
//class A2 {
//public:
//    void f2() {}
//};
//// 类中什么都没有---空类
//class A3
//{};
//int main()
//{
//    cout << sizeof(A1) << endl;
//    cout << sizeof(A2) << endl;
//    cout << sizeof(A3) << endl;
//
//    return 0;
//}


class Date
{
public:
    void Init(int year, int month, int day)
    {
        _year = year;
        _month = month;
        _day = day;
    }
    void Print()
    {
        cout << this << endl;
        cout << _year << "-" << _month << "-" << _day << endl;
    }
   // --------------------------------------------------------------------
        // Print(&d1)
        // Print(&d2)
        //this指针的定义和传递，都是编译器的工作，我们不能越庖代俎
        //但是我们可以在类里用this指针，如果你加了编译器就不加，你不加它会自动加上
        //cout << this->_year<< "-" << _month << "-"<< _day <<endl; 这样都是可以的
        // void Print(Date* this)
        //{
        //   cout << this->_year<< "-" << this->_month << "-"<< this->_day <<endl;
        //}
      //  --------------------------------------------------------------------
private:
    int _year;     // 年
    int _month;    // 月
    int _day;      // 日
};
int main()
{
    Date d1, d2;
    d1.Init(2022, 10, 8);
    d2.Init(2022, 10, 9);
    d1.Print();
    d2.Print();
    cout << &d1 << endl;
    cout << &d2 << endl;

    return 0;
}