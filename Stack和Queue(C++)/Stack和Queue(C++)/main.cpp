#define _CRT_SECURE_NO_WARNINGS 1



#include "Stack.h"
#include "queue.h"


void test1()
{
	//byte::stack<int,vector<int>> st;
	//byte::stack<int,list<int>> st;
	byte::stack<int> st;

	st.push(1);
	st.push(2);
	st.push(3);
	st.push(4);
	st.push(5);

	cout << st.size() << endl;

	while (!st.empty())
	{
		cout<< st.top() << " ";
		st.pop();
	}
	cout << endl;

}
void test2()
{
	byte::queue<int> q;
	q.push(1);
	q.push(2);
	q.push(3);
	q.push(4);
	q.push(5);
	cout << q.front() << " " << q.back() << endl;
	cout << q.size() << endl;
	
	while (!q.empty())
	{
		cout << q.front() << " ";
		q.pop();
	}
	cout << endl;

}


int main()
{
	//test1();
	test2();

	return 0;
}