#define _CRT_SECURE_NO_WARNINGS 1

#include "sort.h"


void Print(int* arr, int n)
{
	for (int i = 0; i < n; ++i)
	{
		printf("%d ", arr[i]);
	}
	printf("\n");

}
void Swap(int* a, int* b)
{
	int ret = *a;
	*a = *b;
	*b = ret;
}
//冒泡排序			左闭右开
void BubbleSort(int* arr, int n)
{
	int flag = 0;
	for (int i = 0; i < n-1; ++i)
	{
		for (int j = 0; j < n -1- i; ++j)
		{
			if (arr[j] > arr[j + 1])
			{
				int tmp = arr[j];
				arr[j] = arr[j + 1];
				arr[j + 1] = tmp;
				flag = 1;
			}
		}
		if (flag == 0)
			break;
	}
}
//插入排序
void InsertSort(int* arr, int n)
{
	for (int i = 0; i < n - 1; ++i)
	{
		int end = i;
		int tmp = arr[end + 1];
		while (end >= 0)
		{
			if (arr[end] > tmp)
			{
				arr[end + 1] = arr[end];
				--end;
			}
			else
			{
				break;
			}
		}
		arr[end+1] = tmp;
	}
}
//希尔排序
void ShellSort(int* arr, int n)
{
	int gap = n;
	while (gap > 1)
	{
		gap = gap / 3 + 1;
		for (int i = 0; i < n - gap; ++i)
		{
			int end = i;
			int tmp = arr[end + gap];
			while (end >= 0)
			{
				if (arr[end] > tmp)
				{
					arr[end + gap] = arr[end];
					end -= gap;
				}
				else
				{
					break;
				}
			}
			arr[end + gap] = tmp;
		}
	}
}
//直接选择排序
void SelectSort(int* arr, int n)
{
	int begin = 0, end = n-1;
	while (begin < end)
	{
		int mini = begin,maxi = end;
		for (int i = begin; i <= end; ++i)
		{
			if (arr[mini] > arr[i])
				mini = i;
			if (arr[maxi] < arr[i])
				maxi = i;
		}

		Swap(&arr[mini], &arr[begin]);
		//可能会出现最大的数在begin位置上，一交换最大数跑到mini位置了，如果不修正会出问题
		if (maxi == begin)
			maxi = mini;
		Swap(&arr[maxi], &arr[end]);

		++begin;
		--end;
	}
}
//堆排序
//向下调整		升序建大堆，降序建小堆
void AdJustDown(int* arr,int n,int parent)
{
	int child = parent * 2 + 1;

	while (child < n)
	{
		//左右孩子比大小
		if (child+1 < n && arr[child] < arr[child + 1])
			child += 1;
		if (arr[parent] < arr[child])
			Swap(&arr[parent], &arr[child]);
		else
			break;
		parent = child;
		child = parent * 2 + 1;
	}
}
void HeapSort(int* arr, int n)
{

	for (int i = (n-1-1)/2; i >=0 ;--i)
	{
		AdJustDown(arr, n, i);
	}

	for (int i = n-1; i >=0; --i)
	{
		Swap(&arr[0], &arr[i]);
		AdJustDown(arr, i, 0);
	}
}



////key值快排单趟				左闭右闭
//int partSort(int* arr, int left, int right)
//{
//	int keyi = left;
//	while (left < right)
//	{
//		//R找小
//		while (left < right && arr[keyi] <= arr[right])
//		{
//			--right;
//		}
//		//L找大
//		while (left < right && arr[keyi] >= arr[left])
//		{
//			++left;
//		}
//		if(left < right)
//			Swap(&arr[left], &arr[right]);
//	}
//	Swap(&arr[left], &arr[keyi]);
//	return left;
//}
////快排递归						//左闭右开
//void QuickSort(int* arr, int begin, int end)
//{
//	if (begin >= end)
//		return;
//
//	int keyi = partSort(arr, begin, end-1);
//
//	QuickSort(arr, begin, keyi);
//
//	QuickSort(arr,keyi + 1,end);
//}
// 


//三数取中
int GetMidIndex(int* arr, int left, int right)
{
	//int mid = (left + right) / 2;		//如果right和left都很大，相加会溢出
	int mid = left + (right - left) / 2;


	if (arr[left] < arr[mid])
	{
		if (arr[mid] < arr[right])		//left < mid < right
			return mid;
		else if (arr[left] < arr[right])//left < right < mid
			return right;
		else                            //right < left < mid
			return left;
	}
	else
	{
		if (arr[right] < arr[mid])		//right < mid < left
			return mid;
		else if (arr[right] < arr[left])//mid < right < left
			return right;
		else                            //mid < left < right
			return left;
	}
}
////key值快排单趟+三数取中				左闭右闭
//int partSort(int* arr, int left, int right)
//{
//	int mid = GetMidIndex(arr, left, right);
//	Swap(&arr[left], &arr[mid]);
//
//	int keyi = left;
//	while (left < right)
//	{
//		//R找小
//		while (left < right && arr[keyi] <= arr[right])
//		{
//			--right;
//		}
//		//L找大
//		while (left < right && arr[keyi] >= arr[left])
//		{
//			++left;
//		}
//		if (left < right)
//			Swap(&arr[left], &arr[right]);
//	}
//	Swap(&arr[left], &arr[keyi]);
//	return left;
//}
////快排递归						//左闭右开
//void QuickSort(int* arr, int begin, int end)
//{
//	if (begin >= end)
//		return;
//
//	int keyi = partSort(arr, begin, end - 1);
//
//	QuickSort(arr, begin, keyi);
//
//	QuickSort(arr, keyi + 1, end);
//}
////快排递归	+小区间优化					//左闭右开
//void QuickSort(int* arr, int begin, int end)
//{
//	if (begin >= end)
//		return;
//
//	if (end - begin <= 8)
//	{
//		InsertSort(arr + begin, end - begin);
//	}
//	else
//	{
//		int keyi = partSort(arr, begin, end - 1);
//
//		QuickSort(arr, begin, keyi);
//		QuickSort(arr, keyi + 1, end);
//	}
//}
//挖坑法单趟
int partSort2(int* arr, int left, int right)
{
	int mid = GetMidIndex(arr, left, right);
	Swap(&arr[left], &arr[mid]);

	int key = arr[left];
	int hole = left;
	while (left < right)
	{
		//R找小
		while (left < right && arr[right] >= key)
		{
			--right;
		}
		arr[hole] = arr[right];
		hole = right;
		//L找大
		while (left < right && arr[left] <= key)
		{
			++left;
		}
		arr[hole] = arr[left];
		hole = left;
	}
	arr[hole] = key;
	return hole;
}

//前后指针法单趟
int partSort3(int* arr, int left, int right)
{
	int mid = GetMidIndex(arr, left, right);
	Swap(&arr[left], &arr[mid]);

	int keyi = left;
	int prev = left;
	int cur = left + 1;
	while (cur <= right)
	{
		if (arr[cur] < arr[keyi] && ++prev != cur)
		{
			Swap(&arr[cur], &arr[prev]);
		}
		cur++;
	}
	Swap(&arr[keyi], &arr[prev]);
	return prev;
}

//快排递归	+小区间优化					//左闭右开
void QuickSort(int* arr, int begin, int end)
{
	if (begin >= end)
		return;

	if (end - begin <= 8)
	{
		InsertSort(arr + begin, end - begin);
	}
	else
	{
		int keyi = partSort3(arr, begin, end - 1);

		QuickSort(arr, begin, keyi);
		QuickSort(arr, keyi + 1, end);
	}
}


//#include <stack>
////快排非递归，借助stack实现，
//void QuickSortNonR(int* arr, int begin, int end)
//{
//	std::stack<int> st;
//	//先入数组区间
//	st.push(begin);
//	st.push(end-1);
//	//栈不为空就继续
//	while (!st.empty())
//	{	//因为是正着存的，所以要反着取
//		int right = st.top();
//		st.pop();
//
//		int left = st.top();
//		st.pop();
//		//if (left >= right)//如果区间只有一个数，或者区间不存在就跳过下面的代码
//		//	continue;
//		int keyi = partSort3(arr, left, right);	//单趟排序
//
//		//插入右区间	区间有两个或两个以上的数才需要插入进行排序
//		if (keyi + 1 < right)
//		{
//			st.push(keyi + 1);
//			st.push(right);
//		}
//		//插入左区间
//		if (left < keyi - 1)
//		{
//			st.push(left);
//			st.push(keyi - 1);
//		}
//	}
//}


//归并排序  -> 递归
void _MergeSort(int* a ,int begin,int end,int* tmp)
{
	if (begin >= end)
		return;
	int mid = (begin + end) / 2;

	_MergeSort(a, begin, mid, tmp);
	_MergeSort(a, mid+1, end, tmp);

	//归并
	int begin1 = begin, end1 = mid;
	int begin2 = mid + 1, end2 = end;
	int idx = begin;
	while (begin1 <= end1 && begin2 <= end2)
	{
		if (a[begin1] <= a[begin2])
			tmp[idx++] = a[begin1++];
		else
			tmp[idx++] = a[begin2++];
	}
	while (begin1 <= end1)
		tmp[idx++] = a[begin1++];
	while (begin2 <= end2)
		tmp[idx++] = a[begin2++];
	//归并哪部分区间，就将哪部分拷贝回原数组
	memcpy(a + begin, tmp + begin, (end - begin + 1) * sizeof(int));
}
void MergeSort(int* a, int n)
{
	int* tmp = (int*)malloc(sizeof(int) * n);
	if (tmp == NULL)
	{
		perror("malloc fail");
		return;
	}

	_MergeSort(a, 0, n - 1, tmp);
	free(tmp);
	tmp = NULL;
}

//归并排序 ->非递归
void MergeSortNonR(int* a, int n)
{
	int* tmp = (int*)malloc(sizeof(int) * n);
	if (tmp == NULL)
	{
		perror("malloc fail");
		return;
	}
	int gap = 1;
	while (gap < n)
	{		//gap个数据进行归并
		for (int i = 0; i < n; i += gap * 2)
		{
			int begin1 = i, end1 = i + gap - 1;
			int begin2 = i + gap, end2 = i + 2 * gap - 1;
			//调整
			if (end1 >= n)//第一组部分越界
				break;
			if (begin2 >= n)//第二组全部越界
				break;
			if (end2 >= n )//第二组部分越界
				end2 = n - 1;
			printf("[%d,%d][%d,%d] ", begin1, end1, begin2, end2);

			int idx = i;
			while (begin1 <= end1 && begin2 <= end2)
			{
				if (a[begin1] <= a[begin2])
					tmp[idx++] = a[begin1++];
				else
					tmp[idx++] = a[begin2++];
			}
			while (begin1 <= end1)
				tmp[idx++] = a[begin1++];
			while (begin2 <= end2)
				tmp[idx++] = a[begin2++];

			//归并哪部分区间，就将哪部分拷贝回原数组 
			//(不建议整个数组归并完了，统一拷贝回去，因为边界不好控制)
			memcpy(a+i, tmp+i, (end2-i+1) * sizeof(int));
		}
		printf("\n");
		gap *= 2;
	}
	free(tmp);
	tmp = NULL;
}
//计数排序
void CountSort(int* a, int n)
{
	//先找max，min,
	int max = a[0], min = a[0];
	for (int i = 1; i < n; ++i)
	{
		if (max < a[i])
			max = a[i];
		if (min > a[i])
			min = a[i];
	}
	//开辟空间
	int range = max - min + 1;
	int* tmp = (int*)malloc(sizeof(int) * range);
	if (tmp == NULL)
	{
		perror("malloc,fail");
		return;
	}
	memset(tmp, 0, sizeof(int) * range);
	//遍历a，tmp数组里就是每个值的个数
	for (int i = 0; i < n; ++i)
	{
		tmp[a[i] - min]++;
	}
	//把tmp数组中的内容，写会原数组，按出现次数
	int idx = 0;
	for (int i = 0; i < range; ++i)
	{
		while (tmp[i]--)
		{
			a[idx++] = i+min;
		}
	}

	free(tmp);
	tmp = NULL;
}