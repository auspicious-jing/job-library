#define _CRT_SECURE_NO_WARNINGS 1


#include "sort.h"


void testBubble()
{
	//int arr[] = { 34,56,25,65,86,99,72,66 };
	//int arr[] = { 54,38,96,23,15,72,60,45,83 };
	int arr[] = { 6,9,3,4,2,7,1,5,8,0 };

	int len = sizeof(arr) / sizeof(arr[0]);
	Print(arr, len);
	BubbleSort(arr, len);		//����ҿ�

	Print(arr, len);
}
void testInsert()
{
	//int arr[] = { 34,56,25,65,86,99,72,66 };
	//int arr[] = { 54,38,96,23,15,72,60,45,83 };
	int arr[] = {6,9,3,4,2,7,1,5,8,0 };

	int len = sizeof(arr) / sizeof(arr[0]);
	Print(arr, len);
	InsertSort(arr, len);
	Print(arr, len);

}
void testShell()
{
	//int arr[] = { 34,56,25,65,86,99,72,66 };
	//int arr[] = { 54,38,96,23,15,72,60,45,83 };
	//int arr[] = { 9,8,7,6,5,4,3,2,1,0 };
	int arr[] = { 6,9,3,4,2,7,1,5,8,0 };
	 

	int len = sizeof(arr) / sizeof(arr[0]);
	Print(arr, len);
	ShellSort(arr, len);
	Print(arr, len);
}
void testQuick()
{
	//int arr[] = { 34,56,25,65,86,99,72,66 };
	//int arr[] = { 54,38,96,23,15,72,60,45,83 };
	int arr[] = { 9,8,7,6,5,4,3,2,1,0 };
	//int arr[] = { 6,9,3,4,2,7,1,5,8,0 };
	//int arr[] = { 6,6,6,6,6,6,6,6,6 };


	int len = sizeof(arr) / sizeof(arr[0]);
	Print(arr, len);
	QuickSort(arr, 0, len);		//����ҿ�
	Print(arr, len);
}
//void testQuickNonR()
//{
//	//int arr[] = { 34,56,25,65,86,99,72,66 };
//	//int arr[] = { 54,38,96,23,15,72,60,45,83 };
//	//int arr[] = { 9,8,7,6,5,4,3,2,1,0 };
//	int arr[] = { 6,9,3,4,2,7,1,5,8,0 };
//	//int arr[] = { 6,6,6,6,6,6,6,6,6 };
//
//
//	int len = sizeof(arr) / sizeof(arr[0]);
//	Print(arr, len);
//	QuickSortNonR(arr, 0, len);		//����ҿ�
//	Print(arr, len);
//}
void testSelect()
{
	//int arr[] = { 34,56,25,65,86,99,72,66 };
	//int arr[] = { 54,38,96,23,15,72,60,45,83 };
	//int arr[] = { 9,8,7,6,5,4,3,2,1,0 };
	int arr[] = { 6,9,3,4,2,7,1,5,8,0 };
	//int arr[] = { 6,6,6,6,6,6,6,6,6 };

	int len = sizeof(arr) / sizeof(arr[0]);
	Print(arr, len);
	SelectSort(arr,len);		//����ҿ�
	Print(arr, len);
}
void testMerge()
{
	//int arr[] = { 34,56,25,65,86,99,72,66 };
	//int arr[] = { 54,38,96,23,15,72,60,45,83 };
	//int arr[] = { 9,8,7,6,5,4,3,2,1,0 };
	int arr[] = { 6,9,3,4,2,7,1,5,8,0 };
	//int arr[] = { 6,6,6,6,6,6,6,6,6 };

	int len = sizeof(arr) / sizeof(arr[0]);
	Print(arr, len);
	MergeSort(arr, len);		//����ҿ�
	Print(arr, len);
}
void testMergeNonR()
{
	int arr[] = { 34,56,25,65,86,99,72 };
	//int arr[] = { 54,38,96,23,15,72,60,45,83 };
	//int arr[] = { 9,8,7,6,5,4,3,2,1,0 };
	//int arr[] = { 6,9,3,4,2,7,1,5,8,0 };
	//int arr[] = { 6,6,6,6,6,6,6,6,6 };

	int len = sizeof(arr) / sizeof(arr[0]);
	Print(arr, len);
	MergeSortNonR(arr, len);		//����ҿ�
	Print(arr, len);
}
int main()
{
	//testBubble();
	//testInsert();
	//testShell();
	//testQuick();
	//testQuickNonR();
	//testSelect();
	//testMerge();
	testMergeNonR();
	return 0;
}