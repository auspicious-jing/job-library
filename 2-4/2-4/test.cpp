#define _CRT_SECURE_NO_WARNINGS 1




#include<iostream>
using namespace std;


//二叉搜索树的递归插入
//bool _InsertR(Node*& root, const K& key)
//{
//	if (root == nullptr)
//	{
//		root = new node(key);
//		return true;
//	}
//	if (root->_key > key)
//		return _InsertR(root->_right, key);
//	else if (root->_key < key)
//		return _InsertR(root->_left, key);
//	else
//		return false;
//
//}

//
////抽象类-> 不能实例化出对象
//class Car
//{
//public:
//	//纯虚函数
//	virtual void Drive() = 0;
//};
//class Benz :public Car
//{
//public:
//	virtual void Drive()
//	{
//		cout << "Benz-舒适" << endl;
//	}
//};
//int main()
//{
//	Car* pBenz = new Benz;
//	pBenz->Drive();
//	
//	return 0;
//}



class Base
{
public:
	virtual void Func1(){cout << "Base::Func1()" << endl;}
	virtual void Func2(){cout << "Base::Func2()" << endl;}
	void Func3(){cout << "Base::Func3()" << endl;}
private:
	int _b = 1;
	char _ch;
};

class Derive : public Base
{
public:
	virtual void Func1(){cout << "Derive::Func1()" << endl;}

	void Func3(){cout << "Derive::Func3()" << endl;}
private:
	int _d = 2;
};
int main()
{
	Base b;
	Derive d;

	// 普通调用 -- 编译时/静态 绑定
	//谁的指针就去调用谁的函数
	Base* ptr = &b;
	ptr->Func3();
	ptr = &d;
	ptr->Func3();

	// 多态调用 -- 运行时/动态 绑定
	//指向谁就去调用谁的函数
	Base* ptr1 = &b;
	ptr1->Func1();
	ptr1 = &d;
	ptr1->Func1();

	return 0;
}