﻿#define _CRT_SECURE_NO_WARNINGS 1

#include <iostream>
using namespace std;

//int main(int argc, char* argv[])
//{
//
//	string a = "hello world";
//
//	string b = a;
//
//	if (a.c_str() == b.c_str())
//
//	{
//
//		cout << "true" << endl;
//
//	}
//
//	else cout << "false" << endl;
//
//	string c = b;
//
//	c = "";
//
//	if (a.c_str() == b.c_str())
//
//	{
//
//		cout << "true" << endl;
//
//	}
//
//	else cout << "false" << endl;
//
//	a = "";
//
//	if (a.c_str() == b.c_str())
//
//	{
//
//		cout << "true" << endl;
//
//	}
//
//	else cout << "false" << endl;
//
//	string strText = "How are you?";
//	string strSeparator = " ";
//	string strResult;
//	int size_pos = 0;
//	int size_prev_pos = 0;
//	//在strText里以空格分割的单词，依次赋值给strResult，并输出
//	//在strText里找strSeparator的位置找到返回下标（从0开始），没找到返回-1；
//	while ((size_pos = strText.find_first_of(strSeparator, size_pos)) != string::npos)
//	{
//		//把strText里size_prev_pos到size_pos - size_prev_pos的位置赋值给strResult；
//		strResult = strText.substr(size_prev_pos, size_pos - size_prev_pos);
//		cout << strResult << " ";
//		//让size_prev_pos和size_pos到空格后的位置
//		size_prev_pos = ++size_pos;
//	}
//	//因为最后一个单词后面没有空格，分割不了了，所以再把后面的单词续上
//	if (size_prev_pos != strText.size())
//
//	{
//		strResult = strText.substr(size_prev_pos, size_pos - size_prev_pos);
//		cout << strResult << " ";
//	}
//	cout << endl;
//return 0;
////
//}

//}
//int main()
//{
//	string str("hello bit.");
//	str.reserve(111);
//	str.resize(5);
//	str.reserve(50);//reserve改变不了capacity
//	cout << str.size() << ":" << str.capacity() << endl;
//	return 0;
//}



//﻿ int main(int argc, char* argv[])
//{
//	string strText = "How are you?";
//	string strSeparator = " ";
//	string strResult;
//	int size_pos = 0;
//	int size_prev_pos = 0;
//	while ((size_pos = strText.find_first_of(strSeparator, size_pos)) != string::npos)
//	{
//		strResult = strText.substr(size_prev_pos, size_pos - size_prev_pos);
//		cout << strResult << " ";
//		size_prev_pos = ++size_pos;
//	}
//	if (size_prev_pos != strText.size())
//
//	{
//		strResult = strText.substr(size_prev_pos, size_pos - size_prev_pos);
//		cout << strResult << " ";
//	}
//	cout << endl;
//	return 0;
//}


//把字符串转换成整数
//class Solution {
//public:
//    int StrToInt(string str) {
//        int len = str.size();
//        if (len == 0)//如果没有字符返回0
//            return 0;
//        int flag = 1;
//        long long num = 0;
//        for (int i = 0; i < len; ++i)
//        {
//            if (str[i] == ' ' || str[i] == '+')//如果第一个字符是空格或者+号就跳过
//                continue;
//            else if (str[i] == '-')//如果第一个是-号就把flag赋值为-1
//            {
//                flag = -1;
//                continue;
//            }
//            if (str[i] <= '9' && str[i] >= '0')//如果是数字字符，就减去字符0并加上num乘10
//            {
//                num = num * 10 + (str[i] - '0');
//                if (num >= INT_MAX && flag == 1)//如果num超过最大的正整数，且flag是1，就返回最大的正整数
//                    return INT_MAX;
//                else if (num > INT_MAX && flag == -1)//如果num比最小的负数还小，且flag是-1，就返回最小的负整数
//                    return INT_MIN;
//            }
//            else//不是数字字符就返回0
//            {
//                return 0;
//            }
//
//        }
//        return num * flag;
//    }
//};

//
class Solution {
public:
    string multiply(string num1, string num2) {
        if (num1 == "0" || num2 == "0") {//两个中有任意一个是0就返回0
            return "0";
        }
        int m = num1.size(), n = num2.size();
        auto ansArr = vector<int>(m + n);
        for (int i = m - 1; i >= 0; i--) {
            int x = num1.at(i) - '0';
            for (int j = n - 1; j >= 0; j--) {
                int y = num2.at(j) - '0';
                ansArr[i + j + 1] += x * y;
            }
        }
        for (int i = m + n - 1; i > 0; i--) {
            ansArr[i - 1] += ansArr[i] / 10;
            ansArr[i] %= 10;
        }
        int index = ansArr[0] == 0 ? 1 : 0;
        string ans;
        while (index < m + n) {
            ans.push_back(ansArr[index]);
            index++;
        }
        for (auto& c : ans) {
            c += '0';
        }
        return ans;
    }
};
//



//class Solution {
//public:
//    string multiply(string num1, string num2) {
//        if ((num1 == "0") || (num2 == "0"))
//            return "0";
//
//        int m = num1.size(), n = num2.size();
//        string product(m + n, 0);
//
//        int i = 0;
//        for (i = m - 1; i >= 0; --i)
//        {
//            int x = num1[i] - '0';
//            for (int j = n - 1; j >= 0; --j)
//            {
//                int y = num2[j] - '0';
//                product[i + j + 1] += x * y;
//            }
//        }
//        for (i = m + n - 1; i > 0; --i)
//        {
//            product[i - 1] += product[i] / 10;
//            product[i] %= 10;
//
//        }
//
//        int t = product[0] == 0 ? 1 : 0;
//
//        string tmp;
//        while (t < m + n)
//        {
//            tmp.push_back(product[t]);
//            ++t;
//        }
//        for (auto& e : tmp)
//        {
//            e += '0';
//        }
//        return tmp;
//
//
//    }
//};