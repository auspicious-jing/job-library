#define _CRT_SECURE_NO_WARNINGS 1

#include <iostream>
#include <cstdio>

using namespace std;




//void Test()
//{
//	int i = 1;
//	// 隐式类型转换
//	double d = i;
//	printf("%d, %.2f\n", i, d);
//
//	int* p = &i;
//	// 显示的强制类型转换
//	int address = (int)p;
//	printf("%x, %d\n", p, address);
//}
//void Test()
//{
//	//int i = 1;
//	////static_cast适用于相似类型的转换
//	//double d = static_cast<double>(i);
//	//printf("%d, %.2f\n", i, d);
//
//	//int* p = &i;
//	////reinterpret_cast 用于类型不相关之间的转换
//	////int address = static_cast<int>(p);//无效转换
//	//int address = reinterpret_cast<int>(p);
//	//printf("%x, %d\n", p, address);
//
//	//const_cast  去除const属性
//	volatile const int a = 2;
//	int* p = const_cast<int*>(&a);
//	*p = 3;
//	cout << a << endl;
//
//}

class A
{
public:
	virtual void f() {}

	int _a = 0;
};
class B : public A
{
public:
	int _b = 0;
};
//void func(A* pa)
//{
//	//直接转换是不安全的
//	B* pb = (B*)pa;
//	cout << pb << endl;
//
//	pb->_a++;
//	pb->_b++;
//	cout << pb->_a << endl;
//	cout << pb->_b << endl;
//}

void func(A* pa)
{
	//dynamic_cast会先检查是否能转换成功，能成功则转换，不能则返回0
	B* pb = dynamic_cast<B*>(pa);
	cout << pb << endl;
	if (pb)
	{
		pb->_a++;
		pb->_b++;
		cout << pb->_a << endl;
		cout << pb->_b << endl;
	}

}
int main()
{
	A aa;
	B bb;
	func(&aa);
	func(&bb);

	return 0;
}
