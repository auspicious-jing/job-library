#define _CRT_SECURE_NO_WARNINGS 1


#include <stdio.h>
#include <assert.h>
//模拟实现库函数strcpy
//char* my_strcpy(char* dest,const char* str)
//{
//	assert(str != NULL);
//	assert(dest != NULL);
//
//	char* set = dest;
//
//	while (*set++ = *str++);
//	return (dest);
//	
//}
//
//int main()
//{
//	char arr1[10] = "abcdef";
//	char arr2[10] = "xxxxxxxx";
//	printf("拷贝前：\narr1:%s\narr2:%s\n", arr1, arr2);
//	/*my_strcpy(arr2,arr1);
//	printf("%s", arr2);*/
//	printf("拷贝后：\narr1:%s\narr2:%s",arr1, my_strcpy(arr2, arr1));
//
//	return 0;
//}


//调整数组使奇数全部都位于偶数前面。
void adjust_array(int arr[],int sz)
{
	int left = 0;
	int right = sz - 1;
	int tmp = 0;
	while (left < right)
	{
		//找偶数
		while ((left < right) && (arr[left] % 2 == 1))
		{
			left++;
		}
		//找奇数
		while ((left < right) && (arr[right] % 2 == 0))
		{
			right--;
		}
		if (left < right)
		{
			tmp = arr[left];
			arr[left] = arr[right];
			arr[right] = tmp;
		}
	}
}
int main()
{
	int arr[] = { 1,2,3,4,5,6,7,8,9,10 };
	int sz = sizeof(arr) / sizeof(arr[0]);

	adjust_array(arr,sz);

	int i = 0;

	for (i = 0; i < 10; i++)
	{
		printf("%d ", arr[i]);
	}
	return 0;
}