#define _CRT_SECURE_NO_WARNINGS 1


#include "stack.h"


//初始化栈
void StackInit(Stack* ps)
{
	assert(ps);
	ps->_a = NULL;
	ps->_top = ps->_capacity = 0;

}
//入栈
void StackPush(Stack* ps, STDataType data)
{
	assert(ps);
	//扩容
	if (ps->_top == ps->_capacity)
	{
		int NewCapacity = ps->_capacity == 0 ? 4 : ps->_capacity * 2;
		ps->_a = (STDataType*)realloc(ps->_a, NewCapacity * sizeof(STDataType));
		if (ps->_a == NULL)
		{
			perror("realloc");
			exit(-1);
		}
		
		ps->_capacity = NewCapacity;
	}
	ps->_a[ps->_top] = data;
	ps->_top++;
}
//出栈
void StackPop(Stack* ps)
{
	assert(ps);
	assert(!StackEmpty(ps));
	ps->_top--;
}
//获取栈顶元素
STDataType StackTop(Stack* ps)
{
	assert(ps);
	assert(!StackEmpty(ps));
	return ps->_a[ps->_top - 1];
}
//获取栈中有效元素个数
int SatckSize(Stack* ps)
{
	assert(ps);
	return ps->_top;
}
//判断栈内是否为空
bool StackEmpty(Stack* ps)
{
	assert(ps);
	return ps->_top == 0;
}
//销毁
void StackDestroy(Stack* ps)
{
	assert(ps);
	free(ps->_a);
	ps->_a = NULL;
	ps->_top = ps->_capacity = 0;
}
