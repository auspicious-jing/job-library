#define _CRT_SECURE_NO_WARNINGS 1


#include <stdio.h>
//int main()
//{
//    int i = 1;
//    int ret = (++i) + (++i) + (++i);//
//    printf("ret = %d\n", ret);
//    return 0;
//}

//int i;
//int main()
//{
//    i--;
//    if (i > sizeof(i))
//    {
//        printf(">\n");
//    }
//    else
//    {
//        printf("<\n");
//    }
//    return 0;
//}
//
//void X_Pattern(int n)
//{
//    char ch[20][20] = { 0 };
//    int i = 0;
//    int j = 0;
//    //全部初始化为空格
//    for (i = 0; i < n; i++)
//    {
//        for (j = 0; j < n; j++)
//        {
//            ch[i][j] = ' ';
//        }
//    }
//    //x，y为坐标，按图案方式将‘*’赋值进去，把x，y看成横纵坐标，正好对角线赋值
//    int x = 0;
//    int y = n - 1;
//    for (i = 0; i < n; i++)
//    {
//        ch[x][x] = '*';
//        ch[x][y] = '*';
//        x++;
//        y--;
//    }
//    //打印
//    for (i = 0; i < n; i++)
//    {
//        for (j = 0; j < n; j++)
//        {
//            printf("%c", ch[i][j]);
//        }
//        printf("\n");
//    }
//}
//
//
//int main()
//{
//    int n = 0;
//    //牛客网它不止输入一个数，所以要加个循环
//    while (scanf("%d", &n) == 1)
//    {
//        X_Pattern(n);
//    }
//
//
//    return 0;
//}
//
//int main()
//{
//    int year = 0;
//    int mon = 0;
//    int day[12] = { 31,28,31,30,31,30,31,31,30,31,30,31 };
//    while (scanf("%d %d", &year, &mon) != EOF)
//    {
//        if (mon == 4 || mon == 6 || mon == 9 || mon == 11)
//            printf("%d\n", day[mon]);
//        else if (mon == 2)
//        {
//            if (year % 4 == 0 && year % 100 != 0 || year % 400 == 0)
//                printf("%d\n", day[mon] + 1);
//            printf("%d\n", day[mon]);
//        }
//        else
//            printf("%d\n", day[mon]);
//    }
//
//return 0;
//}


//int main()
//{
//  /*  int arr[] = { 1,2,3,4,5 };
//    short* p = (short*)arr;
//    int i = 0;
//    for (i = 0; i < 4; i++)
//    {
//        *(p + i) = 0;
//    }
//
//    for (i = 0; i < 5; i++)
//    {
//        printf("%d ", arr[i]);
//    }
//    printf("%d\n", sizeof(short));*/
//
//    unsigned long pulArray[] = { 6,7,8,9,10 };
//    unsigned long* pulPtr;
//    pulPtr = pulArray;
//    *(pulPtr + 3) += 3;
//    printf("%d, %d\n", *pulPtr, *(pulPtr + 3));
//    return 0;
//}
//


//int main()
//{
//	int a = 0x11223344;
//    char *pc = (char*)&a;
//    *pc = 0;
//    printf("%x\n", a);
//    return 0;
//}



//写一个函数打印arr数组的内容，不使用数组下标，使用指针。
//arr是一个整形一维数组

//
//void print(int* p, int sz)
//{
//	int i = 0;
//	for (i = 0; i < sz; i++)
//	{
//		printf("%d ", *(p + i));
//	}
//}
//int main()
//{
//	int arr[] = { 0,1,2,3,4,5,6,7,8,9 };
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	print(arr, sz);
//
//	return 0;
//}

#include <string.h>
int main()
{
    char len[10000] = { 0 };
    scanf("%s", len[10000]);
    int sz = strlen(len);
    char* str = len;
    while (*str != '\0')
    {
        str++;
    }

    int i = 0;
    for (i = 0; i < sz; i++)
    {
        printf("%c", *str);
        str--;
    }

    return 0;
}