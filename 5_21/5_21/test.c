#define _CRT_SECURE_NO_WARNINGS 1

#include <stdio.h>

#include <stdio.h>

//int main()
//{
//    int n = 0;
//    int m = 0;
//    scanf("%d %d", &n, &m);
//
//    int arr[1000] = { 0 };
//
//    int i = 0;
//    for (i = 0; i < n; i++)
//    {
//        scanf("%d", &arr[i]);
//    }
//    for (i = n; i < m + n; i++)
//    {
//        scanf("%d", &arr[i]);
//    }
//
//    int j = 0;
//
//    for (i = 0; i < m + n - 1; i++)
//    {
//        for (j = 0; j < m + n; j++)
//        {
//            if (arr[i] > arr[j])
//            {
//                int tmp = arr[i];
//                arr[i] = arr[j];
//                arr[j] = tmp;
//            }
//        }
//    }
//
//    for (i = 0; i < n + m; i++)
//    {
//        printf("%d ", arr[i]);
//    }
//    return 0;
//}

int main()
{
    int n = 0;
    scanf("%d", &n);

    int arr[100] = { 0 };

    int count = 1;
    int t = n;
    while (t / 10)
    {
        count++;
        t /= 10;
    }
    int i = 0;
    t = n;
    for (i = count - 1; i >= 0; i--)
    {
        arr[i] = t % 10;
        t /= 10;
    }
    for (i = 0; i < count; i++)
    {
        if (arr[i] % 2 == 0)
            arr[i] = 0;
        else
            arr[i] = 1;
    }
    for (i = 0; i < count; i++)
    {
        printf("%d", arr[i]);
    }

    return 0;
}