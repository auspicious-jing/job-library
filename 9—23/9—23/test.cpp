#define _CRT_SECURE_NO_WARNINGS 1

#include <stdio.h>















// int main()
//{
//    int a = 10;
//    char& ra = a;//<====定义引用类型
//    printf("%p\n", &a);
//    printf("%p\n", &ra);
//}
//
// int main()
//{
//	int a = 10;
//	int& ra = a;
//	int& rra = a;
//	printf("%d \n", rra);
//
//	int b = 20;
//	&rra = b;
//	//rra = b;//这个就变成了赋值
//	printf("%d \n", rra);
//}


//int main()
//{
//	int a = 10;
//	int& ra = a;
//	int& rra = a;//可以有多个引用
//
//	printf("%p %p %p\n", &a, &ra, &rra);
//}

#include <iostream>
using namespace std;

//int& Add(int a, int b)
//{
//    int c = a + b;
//    return c;
//}
//int main()
//{
//    int& ret = Add(1, 2);
//    Add(3, 4);
//    cout << "Add(1, 2) is :" << ret << endl;
//    return 0;
//}

//int& Count()
//{
//	int n = 0;
//	cout << &n << endl;
//
//	n++;
//
//	return n;
//}
//
//void Func()
//{
//	int x = 100;
//	cout << &x << endl;
//}
//
//int main()
//{
//	int& ret = Count();
//
//	cout << ret << endl;
//	cout << ret << endl;
//
//	Func();
//	cout << ret << endl;
//
//	cout << &ret << endl;
//
//	return 0;
//}


//int& Count()
//{
//	static int n = 0;
//	cout << &n << endl;
//
//	n++;
//
//	return n;
//}
//
//int main()
//{
//	int& ret = Count();
//
//	cout << ret << endl;
//	ret = 10;
//	cout << ret << endl;
//
//
//	return 0;
//}




#include <time.h>
struct A { int a[10000]; };
A a;
// 值返回
A TestFunc1() { return a; }
// 引用返回
A& TestFunc2() { return a; }
void TestReturnByRefOrValue()
{
	// 以值作为函数的返回值类型
	size_t begin1 = clock();
	for (size_t i = 0; i < 100000; ++i)
		TestFunc1();
	size_t end1 = clock();
	// 以引用作为函数的返回值类型
	size_t begin2 = clock();
	for (size_t i = 0; i < 100000; ++i)
		TestFunc2();
	size_t end2 = clock();
	// 计算两个函数运算完成之后的时间
	cout << "TestFunc1 time:" << end1 - begin1 << endl;
	cout << "TestFunc2 time:" << end2 - begin2 << endl;
}


int main()
{
	TestReturnByRefOrValue();
	return 0;
}