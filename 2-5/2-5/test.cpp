#define _CRT_SECURE_NO_WARNINGS 1


#include <iostream>
using namespace std;


//class Base {
//public:
//	virtual void func1() { cout << "Base::func1" << endl; }
//	virtual void func2() { cout << "Base::func2" << endl; }
//private:
//	int a;
//};
//class Derive :public Base {
//public:
//	virtual void func1() { cout << "Derive::func1" << endl; }
//	virtual void func3() { cout << "Derive::func3" << endl; }
//	void func4() { cout << "Derive::func4" << endl; }
//private:
//	int b;
//}; 
// typedef void(*VFptr)();
//void PrintVFptr(VFptr ptr[])
//{
//	for (int i = 0; ptr[i] != nullptr; ++i)
//	{
//		printf("[%d] 虚函数：%p ->", i, ptr[i]);
//		ptr[i]();
//	}
//	cout << endl;
//}
//int main()
//{
//	Base b;
//	PrintVFptr((VFptr*)(*(int*)&b));
//	//PrintVFptr((VFptr*)(*(void**)&b));//同时适应32位和64位机器
//
//	cout << endl;
//	Derive d;
//	PrintVFptr((VFptr*)(*(int*)&d));
//
//	return 0;
//}
//class Base1 {
//public:
//	virtual void func1() { cout << "Base1::func1" << endl; }
//	virtual void func2() { cout << "Base1::func2" << endl; }
//private:
//	int b1;
//};
//class Base2 {
//public:
//	virtual void func1() { cout << "Base2::func1" << endl; }
//	virtual void func2() { cout << "Base2::func2" << endl; }
//private:
//	int b2;
//};
//class Derive : public Base1, public Base2 {
//public:
//	virtual void func1() { cout << "Derive::func1" << endl; }
//	virtual void func3() { cout << "Derive::func3" << endl; }
//private:
//	int d1;
//};
//typedef void(*VFptr)();
//void PrintVFptr(VFptr ptr[ ])
//{
//	for (int i = 0; ptr[i] != nullptr; ++i)
//	{
//		printf("[%d] 虚函数：%p ->", i, ptr[i]);
//		ptr[i]();
//	}
//	cout << endl;
//}
//int main()
//{
//	Base1 b1;
//	PrintVFptr((VFptr*)(*(void**)&b1));
//	Base2 b2;
//	PrintVFptr((VFptr*)(*(void**)&b2));
//
//	Derive d;
//	PrintVFptr((VFptr*)(*(int*)&d));
//	//跳过Base1，指向第二个虚表
//	//PrintVFptr((VFptr*)(*(int*)((char*)&d+sizeof(Base1))));
//	//切片，直接指向第二个虚表
//	Base2* de = &d;
//	PrintVFptr((VFptr*)(*(int*)de));
//	return 0;
//}

//class A 
//{ public: void test(float a) { cout << a; } }; 
//class B :public A
//{ public: void test(int b) { cout << b; } }; 
//void main() 
//{ 
//	A* a = new A;
//	B* b = new B; 
//	a = b; 
//	a->test(1.1); 
//} 
//class A
//{
//public:
//    A() :m_iVal(0) { test(); }
//    virtual void func() { std::cout << m_iVal << ' '; }
//    void test() { func(); }
//public:
//    int m_iVal;
//};
//class B : public A
//{
//public:
//    B() { test(); }
//    virtual void func()
//    {
//        ++m_iVal;
//        std::cout << m_iVal << ' ';
//    }
//};
//int main(int argc, char* argv[])
//{
//    A* p = new B;
//    p->test();
//    return 0;
//
//}


class A

{

public:

    virtual void f()

    {

        cout << "A::f()" << endl;

    }

};



class B : public A

{

private:

    virtual void f()

    {

        cout << "B::f()" << endl;

    }

};


void main()
{
    A* pa = (A*)new B;

    pa->f();
}
