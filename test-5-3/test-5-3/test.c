#define _CRT_SECURE_NO_WARNINGS 1

#include "game.h"
//菜单函数
void menu()
{
	printf("********************************\n");
	printf("********* 1. 开始游戏 **********\n");
	printf("********* 0. 退出游戏 **********\n");
	printf("********************************\n");
}
//游戏内容的函数
void game()
{
	char ret = 0;
	char board[ROW][COL] = { 0 };
	//初始化棋盘
	ChessBoard(board,ROW,COL);
	//打印棋盘
	DisplayBoard(board, ROW, COL);

	while (1)
	{
		//玩家下棋
		PlayerBoard(board, ROW, COL);
		//判断输赢
		ret = Winning(board, ROW, COL);
		if (ret != 'C')
		{
			break;
		}
		DisplayBoard(board, ROW, COL);

		//电脑下棋
		ComputerBoard(board, ROW, COL);
		//判断输赢
		ret = Winning(board, ROW, COL);
		if (ret != 'C')
		{
			break;
		}
		DisplayBoard(board, ROW, COL);
	}
	if (ret == '*')
	{
		printf("玩家赢\n");
		DisplayBoard(board, ROW, COL);
	}
	else if (ret == '#')
	{
		printf("电脑赢\n");
		DisplayBoard(board, ROW, COL);

	}
	else
	{
		printf("平局\n");
		DisplayBoard(board, ROW, COL);
	}
}
//主函数
int main()
{
	srand((unsigned int)time(NULL));//生成随机数的起点

	int input = 0;
	do
	{
		menu();//打印菜单
		printf("请选择:> ");
		scanf("%d", &input);
		switch (input)
		{
		case 1:
			game();
			break;
		case 2:
			break;
		default :
			printf("选择错误，请重新选择！\n");
			break;
		}
	} while (input);
	return 0;
}