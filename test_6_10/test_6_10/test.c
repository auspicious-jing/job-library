#define _CRT_SECURE_NO_WARNINGS 1

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

//实现一个函数，可以左旋字符串中的k个字符
int  left_char(char* s1, char* s2)
{
	int sz = strlen(s1);
	char sh[10] = { 0 };
	int k = 1;
	int i = 0;
	int j = 0;
	while(k <= sz)
	{
		int n = strcmp(s1, s2);
		if (n == 0)
			return 1;

		for (i = k, j = 0; (i < sz, j < sz - k); i++, j++)
		{
			sh[j] = *(s1 + i);
		}
		for (i = 0, j = sz - k; (i < k, j < sz); i++, j++)
		{
			sh[j] = *(s1 + i);
		}
		for (i = 0, j = 0; (i < sz, j < sz); i++, j++)
		{
			*(s1 + i) = sh[j];
		}
		
		k++;
	}
	return 0;
}

int main()
{
	/*int k = 0;
	scanf("%d", &k);*/

	char s1[10] = "ABCDEF";//CDEAB//CBADE  CDABE
	char s2[10] = "EFABCD";

	int k = left_char(s1,s2);
	if (k == 1)
	{
		printf("相同\n");
	}
	else
		printf("不相同\n");

	printf("%s\n", s1);
	printf("%s\n", s2);


	return 0;
}