#define _CRT_SECURE_NO_WARNINGS 1

#include <iostream>


//int main()
//{
//    const int a = 10;
//    //int& ra = a;   // 该语句编译时会出错，a为常量
//    const int& ra = a;
//     //int& b = 10; // 该语句编译时会出错，b为常量
//    //const int& b = 10;
//    double d = 12.34;
//    //int& rd = d; // 该语句编译时会出错，类型不同
//    const int& rd = d;
//}

////指针和引用赋值，权限可以缩小，但是不能放大
//int main()
//{
//    int a = 0;
//    //权限平移
//    int& ra = a;
//
//    const int b = 10;
//    //我引用你，把权限放大了，是不行的
//    //int& rb = b;//会报错
//
//    b = a;
//    //我引用你，把权限缩小了，可行
//    const int& rra = a;
//
//        //我的权限缩小不代表你的权限也缩小了
//        //rra++;//会报错
//        a++;//可以用
//
//    return 0;
//}



int main()
{
	int a = 10;
	int& ra = a;
	ra = 20;
	int* pa = &a;
	*pa = 20;
	return 0;
}
