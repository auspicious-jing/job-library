#pragma once


#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <Windows.h>

//定义一个9X9的棋盘
#define ROW 9 
#define COL 9

#define ROWS ROW+2
#define COLS COL+2

#define EASY_COUNT 10  //布置的雷的数量

//初始化棋盘
void Initboard(char board[ROWS][COLS], int rows, int cols,char set);

//打印棋盘
void DIsPlay_board(char board[ROWS][COLS], int row, int col);

//存放雷
void Getmine(char board[ROWS][COLS], int row, int col);

//排查雷
void Getshow(char mine[ROWS][COLS],char show[ROWS][COLS], int row, int col);
