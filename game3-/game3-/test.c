#define _CRT_SECURE_NO_WARNINGS 1

#include "game.h"

//菜单
void menu()
{
	printf("****************************\n");
	printf("******** 1 . play   ********\n");
	printf("******** 0 . exit   ********\n");
	printf("****************************\n");

}

//游戏内容
void game()
{
	char mine[ROWS][COLS] = { 0 };//存放雷的信息
	char show[ROWS][COLS] = { 0 };//存放排查雷的信息

	//初始化棋盘
	Initboard(mine,ROWS,COLS,'0');
	Initboard(show, ROWS, COLS,'*');

	//存放雷
	Getmine(mine, ROW, COL);

	//打印棋盘
	//DIsPlay_board(mine, ROW, COL);
	DIsPlay_board(show, ROW, COL);

	//排查雷
	Getshow(mine, show, ROW, COL);
}



//主函数
int main()
{
	int input = 0;
	srand((unsigned int)time(NULL));
	do
	{
		menu();
		printf("请选择:>");
		scanf("%d", &input);
		system("cls");

		switch (input)
		{
		case 1:
			game();
			break;
		case 0:
			printf("游戏已退出！\n");
			break;
		default :
			printf("选择错误，请重新选择！\n");
			break;
		}
	} while (input);
	return 0;
}