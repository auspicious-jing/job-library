#pragma once

// 给不同容器的正向迭代器，适配出对应的这个容器需要的反向迭代器
template<class iterator,class Ref,class Ptr>
class ReveserIterator
{
	typedef ReveserIterator <iterator,Ref,Ptr> Self;
public:
	ReveserIterator(iterator it)
		:_it(it)
	{}
	Ref operator*()
	{
		iterator tmp = _it;
			return *(--tmp);
	}
	Ptr operator->()
	{
		return &(operator*());
	}
	Self& operator++()
	{
		--_it;
		return *this;
	}
	Self& operator--()
	{
		++_it;
		return *this;
	}
	bool operator!=(const Self& s)const
	{
		return _it != s._it;
	}
	bool operator==(const Self& s)const
	{
		return _it == s._it;
	}
private:
	iterator _it;
};