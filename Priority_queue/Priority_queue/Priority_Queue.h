#pragma once



#include <iostream>
using namespace std;
#include <vector>
#include <functional>

namespace byte
{
	// 仿函数/函数对象  //可以用库里面的
	/*template<class T>
	class less
	{
	public:
		bool operator()(const T& x, const  T& y)const
		{
			return x < y;
		}
	};
	template<class T>
	class greater
	{
	public:
		bool operator()(const T& x, const T& y)const
		{
			return x > y;
		}
	};*/
	template<class T, class Container = vector<int>,class Compare = less<T>>
	class priority_queue
	{
	public:
		//如果没有迭代器构造可以不写，编译器会默认生成，有迭代器构造就得写一个放这，让编译器走初始化列表
		priority_queue()
		{}
		//用迭代器构造建堆
		template<class InputIterator>
		priority_queue(InputIterator first, InputIterator last)
			:c(first,last)
		{
			for (size_t i = (c.size() - 1 - 1) / 2; i >= 0; --i)
			{
				AdJustDown(i);
			}
		}
		//向上调整
		void AdJustUp(size_t child)
		{
			
			size_t parent = (child - 1) / 2;
			while (child > 0)
			{
				//if (c[parent] < c[child])
				if (com(c[parent] , c[child]))
				{
					swap(c[parent], c[child]);
					child = parent;
					parent = (child - 1) / 2;
				}
				else
					break;
			}
		}
		void push(const T& x)
		{
			c.push_back(x);
			AdJustUp(c.size() - 1);
		}
		//向下调整
		void AdJustDown(size_t parent)
		{
			
			size_t child = parent * 2 + 1;
			while (child < c.size())
			{
				//if ( child+1 < c.size() && c[child] > c[child + 1])
				if (child + 1 < c.size() && com(c[child] , c[child + 1]))
					child++;
				//if (c[parent] < c[child])
				if (com(c[parent] , c[child]))
				{
					swap(c[parent], c[child]);
					parent = child;
					child = parent * 2 + 1;
				}
				else
					break;
			}
		}
		void pop()
		{
			swap(c[0], c[c.size() - 1]);
			c.pop_back();
			AdJustDown(0);
		}
		const T& top()const
		{
			return c[0];
		}
		size_t size()const
		{
			return c.size();
		}
		bool empty()const
		{
			return c.empty();
		}
	private:
		Compare com;
		Container c;
	};
};