#pragma once
#include <iostream>
#include <assert.h>
#include <vector>
#include <string.h>
using namespace std;


#include "iterator.h"
namespace bit
{
	template<class T>
	class vector
	{
	public:
		// Vector的迭代器是一个原生指针
		typedef T* iterator;
		typedef const T* const_iterator;
		typedef ReveserIterator<iterator, T&, T*> reveser_iterator;
		//迭代器
		iterator begin()
		{
			return _start;
		}
		iterator end()
		{
			return _finish;
		}
		const_iterator begin()const
		{
			return _start;
		}
		const_iterator end()const
		{
			return _finish;
		}
		reveser_iterator rbegin()
		{
			return end();
		}
		reveser_iterator rend()
		{
			return begin();
		}
		//构造函数
		vector()
			:_start(nullptr)
			, _finish(nullptr)
			, _endOfStorage(nullptr)
		{}
		//v(10,1)
		vector(int n, const T& value = T())
			:_start(nullptr)
			, _finish(nullptr)
			, _endOfStorage(nullptr)
		{
			reserve(n);
			for (int i = 0;i < n;++i)
			{
				push_back(value);
			}
		}
		vector(size_t n, const T& value = T())
			:_start(nullptr)
			, _finish(nullptr)
			, _endOfStorage(nullptr)
		{
			reserve(n);
			for (size_t i = 0; i < n; ++i)
			{
				push_back(value);
			}
		}
		//模板
		template<class InputIterator>
		vector(InputIterator first, InputIterator last)
			:_start(nullptr)
			, _finish(nullptr)
			, _endOfStorage(nullptr)
		{
			while (first != last)
			{
				push_back(*first);
				++first;
			}
		}
		//v(v1) 拷贝构造
		vector(const vector<T>& v)
			:_start(nullptr)
			, _finish(nullptr)
			, _endOfStorage(nullptr)
		{
			vector<T> tmp(v.begin(),v.end());
			swap(tmp);
			
		}
		//赋值重载
		vector<T>& operator= (vector<T> v)
		{
			swap(v);
			return *this;
		}
		//析构函数
		~vector()
		{
			delete[] _start;
			_start = _finish = _endOfStorage = nullptr;
		}


		//容量
		size_t size()const
		{
			return _finish - _start;
		}
		size_t capacity()const
		{
			return _endOfStorage - _start;
		}
		bool empty()const
		{
			return _finish == _start;
		}
		void reserve(size_t n)
		{
			if (n > capacity())
			{
				size_t oldsize = size();
				iterator tmp = new T[n];

				if (_start)
				{
					//memcpy(tmp, _start, sizeof(T) * size());这种写法会发生浅拷贝问题
					for (size_t i = 0; i < oldsize; ++i)
					{
						tmp[i] = _start[i];
					}
				}

				delete[] _start;
				_start = tmp;
				_finish = _start + oldsize;
				_endOfStorage = _start + n;
			}
		}
		void resize(size_t n, const T& value = T())
		{
			if (n > capacity())
			{
				reserve(n);
			}
			if (n > size())
			{
				while (_finish < _start + n)
				{
					*_finish = value;
					++_finish;
				}
			}
			else
			{
				_finish = _start + n;
			}
		}

		//access 访问
		T& operator[](size_t pos)
		{
			assert(pos < size());
			return _start[pos];
		}
		const T& operator[](size_t pos)const
		{
			assert(pos < size());
			return _start[pos];
		}
		//增删查改
		//尾插
		void push_back(const T& x)
		{
			//判断是否扩容
			if (_finish == _endOfStorage)
			{
				size_t newcapacity = capacity() == 0 ? 4 : capacity() * 2;
				reserve(newcapacity);
			}
			*_finish = x;
			++_finish;
		}
		//尾删
		void pop_back()
		{
			assert(!empty());
			--_finish;
		}
		//交换函数
		void swap(vector<T>& v)
		{
			std::swap(_start, v._start);
			std::swap(_finish, v._finish);
			std::swap(_endOfStorage, v._endOfStorage);
		}
		//pos位置插入数据
		iterator insert(iterator pos, const T& x)
		{
			assert(pos >= _start && pos < _finish);
			if (_finish == _endOfStorage)
			{
				size_t len = pos - _start;
				size_t newcapacity = capacity() == 0 ? 4 : capacity() * 2;
				reserve(newcapacity);
				// 扩容会导致pos迭代器失效，需要更新处理一下
				pos = _start + len;
			}
			//挪动数据
			iterator end = _finish - 1;
			while (pos <= end)
			{
				*(end + 1) = *end;
				--end;
			}
			*pos = x;
			++_finish;
			return pos;
		}
		//删除pos位置数据
		iterator erase(iterator pos)
		{
			assert(pos >= _start);
			assert( pos < _finish);
			//挪动数据
			iterator begin = pos + 1;
			while (begin < _finish)
			{
				*(begin - 1) = *begin;
				++begin;
			}
			--_finish;
			return pos;
		}

	private:
		iterator _start;//指向数据块的开始
		iterator _finish;//指向有效数据的尾
		iterator _endOfStorage;//指向存储容量的尾

	};
	void test1()
	{
		vector<int> v;
		v.reserve(10);
		v.push_back(1);//插入数据
		v.push_back(2);
		v.push_back(3);
		v.push_back(4);
		//迭代器
		vector<int>::iterator it = v.begin();
		while (it != v.end())
		{
			cout << *it << " ";
			++it;
		}
		cout << endl;
		//下标访问
		for (size_t i = 0; i < v.size(); ++i)
		{
			cout << v[i] << " ";
		}
		cout << endl;
		//删除数据
		v.pop_back();
		v.pop_back();
		//自动迭代器
		for (auto e : v)
		{
			cout << e << " ";
		}
		cout << endl;
	}
	/*void test2()
	{
		vector<int> v;
		v.resize(10);
		for (auto e : v)
		{
			cout << e << " ";
		}
		cout << endl;
		
		v.resize(5);
		for (auto e : v)
		{
			cout << e << " ";
		}
		cout << endl;
		v.resize(8, 1);
		for (auto e : v)
		{
			cout << e << " ";
		}
		cout << endl;
	}*/
	/*void test3()
	{
		vector<int> v;
		
		v.push_back(1);
		v.push_back(2);
		v.push_back(3);
		v.push_back(4);
		vector<int>::iterator pos = find(v.begin(), v.end(), 3);
		pos = v.insert(pos, 30);
		(*pos)++;
		for (auto e : v)
		{
			cout << e << " ";
		}
		cout << endl;

	}*/
	void test4()
	{
		vector<int> v;
		
		v.push_back(1);
		v.push_back(2);
		v.push_back(3);
		v.push_back(4);
		v.push_back(4);
		vector<int>::iterator pos = find(v.begin(), v.end(), 4);
		pos = v.erase(pos);
		(*pos)++;
		for (auto e : v)
		{
			cout << e << " ";
		}
		cout << endl;

	}
	void test5()
	{
		// 要求删除所有偶数
		vector<int> v;
		v.push_back(1);
		v.push_back(2);
		v.push_back(2);
		v.push_back(3);
		v.push_back(4);
		v.push_back(5);

		vector<int>::iterator it = v.begin();
		while (it != v.end())
		{
			if (*it % 2 == 0)
				it = v.erase(it);
			else
				++it;
		}
		for (auto e : v)
		{
			cout << e << " ";
		}
		cout << endl;
	}
	void test6()
	{
		vector<int> v;

		v.push_back(1);
		v.push_back(2);
		v.push_back(3);
		v.push_back(4);
		for (auto e : v)
		{
			cout << e << " ";
		}
		cout << endl;
		//迭代器
		vector<int> v1(v.begin(), v.end());
		for (auto e : v1)
		{
			cout << e << " ";
		}
		cout << endl;
		//拷贝构造
		vector<int> v2(v1);
		for (auto e : v2)
		{
			cout << e << " ";
		}
		cout << endl;
		//赋值重载
		vector<int> v3;
		v3 = v1;
		for (auto e : v2)
		{
			cout << e << " ";
		}
		cout << endl;
	}
	void test7()
	{
		vector<int> v;
		v.reserve(10);
		v.push_back(1);//插入数据
		v.push_back(2);
		v.push_back(3);
		v.push_back(4);
		//迭代器
		vector<int>::iterator it = v.begin();
		while (it != v.end())
		{
			cout << *it << " ";
			++it;
		}
		cout << endl;
		vector<int>::reveser_iterator rit = v.rbegin();
		while (rit != v.rend())
		{
			cout << *rit << " ";
			++rit;
		}
		cout << endl;
	}
}
