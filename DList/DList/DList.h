#pragma once

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

typedef int LTDateType;

typedef struct ListNode
{
	LTDateType Date;
	struct ListNode* prve;
	struct ListNode* next;

	
}DL;
//创建一个新节点
DL* BuyDListNode(LTDateType x);
// 创建返回链表的头结点.
DL* DListCreate();
// 双向链表销毁
void DListDestory(DL* pHead);
// 双向链表打印
void DListPrint(DL* pHead);
// 双向链表尾插
void DListPushBack(DL* pHead, LTDateType x);
// 双向链表尾删
void DListPopBack(DL* pHead);
// 双向链表头插
void DListPushFront(DL* pHead, LTDateType x);
// 双向链表头删
void DListPopFront(DL* pHead);
// 双向链表查找
DL* DListFind(DL* pHead, LTDateType x);
// 双向链表在pos的前面进行插入
void DListInsert(DL* pos, LTDateType x);
// 双向链表删除pos位置的节点
void DListErase(DL* pos);