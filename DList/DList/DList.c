#define _CRT_SECURE_NO_WARNINGS 1

#include "DList.h"

//创建一个新节点
DL* BuyDListNode(LTDateType x)
{
	DL* newnode = (DL*)malloc(sizeof(DL));
	if (newnode == NULL)
	{
		printf("malloc fail");
		exit(-1);
	}
	newnode->Date = x;
	newnode->next = NULL;
	newnode->prve = NULL;
	return newnode;
}
// 创建返回链表的头结点.
DL* DListCreate()
{
	DL* Guard = BuyDListNode(0);
	
	Guard->prve = Guard;
	Guard->next = Guard;

	return Guard;
}
// 双向链表销毁
void DListDestory(DL* pHead)
{
	assert(pHead);
	DL* cur = pHead->next;
	
	while (cur != pHead)
	{
		DL* next = cur->next;
		free(cur);
		cur = next;
	}
	free(pHead);
	pHead = NULL;
	printf("销毁完成\n");
}

// 双向链表打印
void DListPrint(DL* pHead)
{
	assert(pHead);

	DL* cur = pHead->next;
	printf("phead<==>");

	while (cur != pHead)
	{
		printf("%d<==>", cur->Date);
		cur = cur->next;
	}
	printf("\n");

}
// 双向链表尾插
void DListPushBack(DL* pHead, LTDateType x)
{
	assert(pHead);

	/*DL* tail = pHead->prve;
	DL* NewNode = BuyDListNode(x);
	tail->next = NewNode;
	NewNode->prve = tail;
	NewNode->next = pHead;
	pHead->prve = NewNode;*/
	DListInsert(pHead, x);

}
// 双向链表尾删
void DListPopBack(DL* pHead)
{
	assert(pHead);

	/*DL* cur = pHead->prve;
	DL* prve = cur->prve;
	prve->next = pHead;
	pHead->prve = prve;
	free(cur);
	cur = NULL;*/
	DListErase(pHead->prve);

}
// 双向链表头插
void DListPushFront(DL* pHead, LTDateType x)
{
	assert(pHead);

	/*DL* cur = pHead->next;
	DL* NewNode = BuyDListNode(x);
	NewNode->next = cur;
	cur->prve = NewNode;
	pHead->next = NewNode;
	NewNode->prve = pHead;*/
	DListInsert(pHead->next, x);
}
// 双向链表头删
void DListPopFront(DL* pHead)
{
	assert(pHead);
	/*DL* prve = pHead->next;
	DL* cur = prve->next;
	pHead->next = cur;
	cur->prve = pHead;
	free(prve);
	prve = NULL;*/
	DListErase(pHead->next);

}
// 双向链表查找
DL* DListFind(DL* pHead, LTDateType x)
{
	assert(pHead);
	DL* cur = pHead->next;
	while (cur != pHead)
	{
		if (x == cur->Date)
		{
			return cur;
		}
		cur = cur->next;
	}
	return NULL;
}
// 双向链表在pos的前面进行插入
void DListInsert(DL* pos, LTDateType x)
{
	assert(pos);
	DL* NewNode = BuyDListNode(x);
	DL* prve = pos->prve;

	NewNode->next = pos;
	pos->prve = NewNode;
	prve->next = NewNode;
	NewNode->prve = prve;

}
// 双向链表删除pos位置的节点
void DListErase(DL* pos)
{
	assert(pos);

	DL* cur = pos->next;
	DL* prve = pos->prve;

	prve->next = cur;
	cur->prve = prve;
	free(pos);
	pos = NULL;
}