#define _CRT_SECURE_NO_WARNINGS 1

#include "DList.h"
void test1()
{
	DL* Dlist = DListCreate();
	DListPrint(Dlist);

	DListPushBack(Dlist, 1);
	DListPushBack(Dlist, 2);
	DListPushBack(Dlist, 3);
	DListPushBack(Dlist, 4);
	DListPrint(Dlist);

	DListPushFront(Dlist, 10);
	DListPushFront(Dlist, 20);
	DListPushFront(Dlist, 30);
	DListPushFront(Dlist, 40);
	DListPrint(Dlist);

	DListPopBack(Dlist);
	DListPopBack(Dlist);
	DListPopBack(Dlist);
	DListPopBack(Dlist);
	DListPrint(Dlist);

	DListPopFront(Dlist);
	DListPopFront(Dlist);
	DListPopFront(Dlist);
	DListPopFront(Dlist);
	DListPrint(Dlist);
	DListDestory(Dlist);
	Dlist = NULL;
}
void test2()
{
	DL* Dlist = DListCreate();
	DListPrint(Dlist);

	DListPushBack(Dlist, 1);
	DListPushBack(Dlist, 2);
	DListPushBack(Dlist, 3);
	DListPushBack(Dlist, 4);
	DListPrint(Dlist);

	DListPushFront(Dlist, 10);
	DListPushFront(Dlist, 20);
	DListPushFront(Dlist, 30);
	DListPushFront(Dlist, 40);
	DListPrint(Dlist);

	DL* pos = DListFind(Dlist,20);
	DListInsert(pos, 25);
	DListPrint(Dlist);

	pos = DListFind(Dlist, 20);
	DListErase(pos);
	DListPrint(Dlist);

	DListDestory(Dlist);
	Dlist = NULL;

}
int main()
{
	test1();
	


	return 0;
}