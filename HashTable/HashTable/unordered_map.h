#pragma once

#include "Hashbucket(��ϣͰ).h"


namespace byte
{
	template<class K,class V ,class Hash = HashFunc<K>>
	class unordered_map
	{
		struct mapKeyOfT
		{
			const K& operator()(const pair<K,V>& kv)
			{
				return kv.first;
			}
		};
	public:
		typedef HashBucket<K, pair<const K,V>, Hash, mapKeyOfT> HaSh;
		typedef typename HashBucket<K, pair<const K, V>, Hash, mapKeyOfT>::iterator iterator;
		typedef typename HashBucket<K, pair<const K, V>, Hash, mapKeyOfT>::const_iterator const_iterator;

		iterator begin()
		{
			return _ht.begin();
		}
		iterator end()
		{
			return _ht.end();
		}
		const_iterator begin()const
		{
			return _ht.begin();
		}
		const_iterator end()const
		{
			return _ht.end();
		}
		pair<iterator, bool> Insert(const pair<K,V>& kv)
		{
			return _ht.Insert(kv);
		}
		iterator Erase(iterator pos)
		{
			return _ht.Erase(pos->first);
		}
		V& operator[](const K& key)
		{
			pair<iterator, bool> ret = _ht.Insert(make_pair(key,V()));
			return ret.first->second;
		}
		const V& operator[](const K& key)const
		{
			pair<iterator, bool> ret = _ht.Insert(make_pair(key, V()));
			return ret.first->second;
		}
		iterator Find(const K& key)
		{
			return _ht.Find(key);
		}
	private:
		HaSh _ht;
	};
	void test_unordered_map()
	{
		string arr[] = { "ƻ��", "����", "�㽶", "��ݮ", "ƻ��", "����", "ƻ��", "ƻ��", "����", "ƻ��", "�㽶", "ƻ��", "�㽶" };

		unordered_map<string, int> countMap;
		for (auto& e : arr)
		{
			countMap[e]++;
		}

		for (const auto& kv : countMap)
		{
			cout << kv.first << ":" << kv.second << endl;
		}
		cout << endl;

		unordered_map<string, int>::iterator it = countMap.Find("����");
		cout << (*it).first << endl<< endl;
		countMap.Erase(it);
		for (const auto& kv : countMap)
		{
			cout << kv.first << ":" << kv.second << endl;
		}
	}
}
