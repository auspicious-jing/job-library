#pragma once



template<class K>
struct HashFunc
{
	size_t operator()(const K& key)
	{
		return (size_t)key;
	}
};
template<>
struct HashFunc<string>
{
	size_t operator()(const string& key)
	{
		size_t num = 0;
		for (auto& ch: key)
		{
			num *= 131;
			num += ch;
		}
		return num;
	}
};
template<class T>
struct HashBucketNode
{
	HashBucketNode(const T& data)
		:_data(data)
		,_next(nullptr)
	{}
	T _data;
	HashBucketNode<T>* _next;
};
//前置声明
template<class K, class T, class Hash, class KeyOfT>
class HashBucket;

template<class K, class T, class Ref, class Ptr, class Hash, class KeyOfT>
struct const_IteratorHash
{
	typedef HashBucketNode<T> Data;
	typedef const_IteratorHash<K, T, Ref, Ptr, Hash, KeyOfT> Self;
	//为了实现简单，在哈希桶的迭代器类中需要用到hashBucket本身
	typedef HashBucket<K, T, Hash, KeyOfT> HT;

	const_IteratorHash(const Data* node, const HT* ht)
		:_node(node)
		, _ht(ht)
	{}

	//因为迭代器里多了一个哈希表对象，如果是普通迭代器，begin和end的this指针就是普通的，可以迭代器构造，
	//但是const begin和end 的this指针就不能调用迭代器构造，所以要多增加一个const迭代器的类，普通迭代器和const迭代器实现分开
	const HT* _ht;
	const Data* _node;
};

template<class K, class T,class Ref,class Ptr, class Hash, class KeyOfT>
struct _IteratorHash
{
	typedef HashBucketNode<T> Data;
	typedef _IteratorHash<K, T,Ref,Ptr, Hash, KeyOfT> Self;
	//为了实现简单，在哈希桶的迭代器类中需要用到hashBucket本身
	typedef HashBucket<K, T, Hash, KeyOfT> HT;

	_IteratorHash(Data* node,HT* ht)
		:_node(node)
		,_ht(ht)
	{}
	Ref operator*()
	{
		return _node->_data;
	}
	Ptr operator->()
	{
		return &_node->_data;
	}
	Self& operator++()
	{	//如果桶内还有节点，那就取他的下一个节点
		if (_node->_next)
		{
			_node = _node->_next;
		}
		else
		{
			Hash hs;
			KeyOfT kot;
			size_t hashi = hs(kot(_node->_data)) % _ht->_tables.size();//找当前桶的映射位置
			while (++hashi < _ht->_tables.size())	//找到下一个不为空的桶
			{
				if (_ht->_tables[hashi])
				{
					_node = _ht->_tables[hashi];
					break;
				}

			}
			if (hashi == _ht->_tables.size())	//数组已经走完了，都遍历一遍了
				_node = nullptr;
		}
		return *this;
	}
	//Self operator++(int)
	//{
	//	Self ret = *this;
	//	if (_node->_next)
	//	{
	//		_node = _node->_next;
	//	}
	//	else
	//	{
	//		Hash hs;
	//		KeyOfT kot;
	//		size_t hashi = hs(kot(_node->_data)) % _ht->_tables.size();
	//		while (++hashi < _ht->_tables.size())
	//		{
	//			if (_ht->_tables[hashi])
	//			{
	//				_node = _ht->_tables[hashi];
	//				break;
	//			}

	//		}
	//		if (hashi == _ht->_tables.size())
	//			_node = nullptr;
	//	}
	//	return ret;
	//}

	bool operator!=(const Self& s)const
	{
		return _node != s._node;
	}

	bool operator==(const Self& s)const
	{
		return _node == s._node;
	}
	HT* _ht;
	Data* _node;
};

template<class K,class T,class Hash, class KeyOfT>
class HashBucket
{
	typedef HashBucketNode<T> Data;
	template<class K, class T,class Ref,class Ptr, class Hash, class KeyOfT>
	friend struct _IteratorHash;	//友元

public:
	typedef _IteratorHash<K, T,T&,T*, Hash, KeyOfT> iterator;
	typedef _IteratorHash<K, T, const T&, const T*, Hash, KeyOfT> const_iterator;

	iterator begin()
	{
		for (size_t i = 0; i < _tables.size(); ++i)
		{
			if (_tables[i])
				return iterator(_tables[i], this);
		}
		return iterator(nullptr, this);
	}
	iterator end()
	{
		return iterator(nullptr, this);
	}
	const_iterator begin()const
	{
		for (size_t i = 0; i < _tables.size(); ++i)
		{
			if (_tables[i])
				return const_iterator(_tables[i], this);
		}
		return const_iterator(nullptr, this);
	}
	const_iterator end()const
	{
		return const_iterator(nullptr, this);
	}

	HashBucket()
		:_n(0)
	{
		_tables.resize(10);
	}
	pair<iterator,bool> Insert(const T& data)
	{
		KeyOfT kot;
		Hash hs;
		 //查找表里有没有该值
		iterator node = Find(kot(data));
		if (node != end())
			return make_pair(node,false);
		//扩容
		if (_tables.size() == _n)
		{
			vector<Data*> newHT;
			newHT.resize(_tables.size() * 2);
			for (size_t i = 0; i < _tables.size(); ++i)
			{
				Data* cur = _tables[i];
				
				while (cur)
				{
					Data* next = cur->_next;
					size_t hashi = hs(kot(cur->_data)) % newHT.size();
					cur->_next = newHT[hashi];
					newHT[hashi] = cur;
					cur = next;
				}
				_tables[i] = nullptr;
			}
			_tables.swap(newHT);
		}
		//插入
		size_t hashi = hs(kot(data)) % _tables.size();
		Data* newNode = new Data(data);
		newNode->_next = _tables[hashi];
		_tables[hashi] = newNode;
		++_n;
		return make_pair(iterator(newNode, this), true);
	}
	//bool Erase(const K& key)
	//{
	//	size_t hashi = Hash()(key) % _tables.size();
	//	Data* cur = _tables[hashi];
	//	Data* prev = nullptr;
	//	while (cur)
	//	{
	//		if (cur->_data == key)
	//		{
	//			//删除
	//			if (cur == _tables[hashi])
	//			{
	//				_tables[hashi] = cur->_next;
	//			}
	//			else
	//			{
	//				prev->_next = cur->_next;
	//			}
	//			delete cur;
	//			return true;
	//		}
	//		else
	//		{
	//			prev = cur;
	//			cur = cur->_next;
	//		}
	//		
	//	}
	//	return false;
	//}	
	iterator Erase(const K& key)
	{
		size_t hashi = Hash()(key) % _tables.size();
		Data* cur = _tables[hashi];
		Data* prev = nullptr;
		while (cur)
		{
			if (KeyOfT()(cur->_data) == key)
			{
				//删除
				if (cur == _tables[hashi])
				{
					_tables[hashi] = cur->_next;
				}
				else
				{
					prev->_next = cur->_next;
				}
				delete cur;
				return iterator(prev,this);
			}
			else
			{
				prev = cur;
				cur = cur->_next;
			}
		}
		return iterator(nullptr,this);
	}
	iterator Find(const K& key)
	{
		size_t hashi = Hash()(key) % _tables.size();
		Data* cur = _tables[hashi];
		while (cur)
		{
			if (KeyOfT()(cur->_data) == key)
			{
				return iterator(cur, this);
			}
			cur = cur->_next;
		}
		return iterator(nullptr,this);
	}
	void Clear()
	{
		for (size_t i = 0; i < _tables.size(); ++i)
		{
			Data* cur = _tables[i];
			while (cur)
			{
				Data* next = cur->_next;
				delete cur;
				cur = next;
			}
			_tables[i] = nullptr;
		}
	}

private:
	vector<Data*> _tables;
	size_t _n;
};
//void test1()
//{
//	int a[] = { 3,38,5,56,7,27,1,17,38,19,15 };
//
//	HashBucket<int, int,HashFunc<int>> ht;
//
//	for (auto e : a)
//	{
//		ht.Insert(e);
//	}
//	ht.Insert(13);
//	
//	cout << ht.Erase(27) << endl;
//	cout << ht.Erase(7) << endl;
//	cout << ht.Erase(7) << endl;
//
//
//}