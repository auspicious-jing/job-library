#pragma once

//namespace byte
//{
//	enum State// 三种状态
//	{
//		EMPTY,//空
//		EXIST,//存在
//		DELETE,//删除
//	};
//	template<class K>
//	struct HashFunc
//	{	//将key转化为整形
//		size_t operator()(const K& key)
//		{
//			return (size_t)key;
//		}
//	};
//	template<>//模板特化
//	struct HashFunc<string>
//	{	//将字符串转化为整形
//		size_t operator()(const string& key)
//		{
//			size_t num = 0;
//			for (auto& ch : key)
//			{
//				num *= 131;//减少冲突概率
//				num += ch;
//			}
//			return num;
//		}
//	};
//	template<class K, class V>
//	struct HashData
//	{
//		pair<K, V> _data;
//		State state = EMPTY;
//	};
//
//	template<class K, class V, class Hash = HashFunc<K>>
//	class HashTable
//	{
//		typedef HashData<K, V> Data;
//	public:
//		HashTable()
//			:_n(0)
//		{	//提前开好空间，避免第一次插入的地方计算负载因子时报错
//			_tables.resize(10);
//		}
//		bool insert(const pair<K, V>& data)
//		{	//如果key存在就不插入
//			if (Find(data.first))
//			{
//				return false;
//			}
//			//负载因子大于0.7就扩容，浮点数不好比较就直接乘10，按整形比
//			if (_n * 10 / _tables.size() > 7)
//			{
//				HashTable newHT;
//				newHT._tables.resize(_tables.size() * 2);
//				for (auto& e : _tables)//开一个二倍size的哈希表，把旧表数据插入新表，
//				{
//					if (e.state == EXIST)
//					{
//						newHT.insert(e._data);
//					}
//				}
//				_tables.swap(newHT._tables);
//			}
//			Hash hf;//仿函数
//			//求映射下标，如果是其他类型用仿函数转化为整形取模
//			size_t hashi = hf(data.first) % _tables.size();
//			while (_tables[hashi].state == EXIST)
//			{
//				++hashi;
//				hashi %= _tables.size();
//			}
//			_tables[hashi]._data = data;
//			_tables[hashi].state = EXIST;
//			++_n;
//			return true;
//		}
//		bool erase(const K& key)
//		{
//			Data* ret = Find(key);
//			if (ret)
//			{
//				ret->state = DELETE;
//				_n--;
//				return true;
//			}
//			else
//			{
//				return false;
//			}
//		}
//		Data* Find(const K& key)
//		{
//			Hash hf;
//			size_t hashi = hf(key) % _tables.size();//求下标
//			size_t start = hashi;
//			while (_tables[hashi].state != EMPTY)
//			{	//下标位置的数据状态要为存在才可以被找到
//				if (_tables[hashi].state == EXIST && _tables[hashi]._data.first == key)
//				{
//					return &_tables[hashi];
//				}
//				++hashi;
//				hashi %= _tables.size();
//				if(start == hashi)	//极端场景下，表里的状态没有空，全是存在和删除，如果不写这个可能导致死循环
//					break;
//			}
//			return nullptr;
//		}
//
//	private:
//		vector<Data> _tables;
//		int _n;
//	};
//	void test1()
//	{
//		int a[] = { 3,38,5,56,7,27,1,17 };
//
//		HashTable<int, int> ht;
//
//		for (auto e : a)
//		{
//			ht.insert(make_pair(e, e));
//		}
//
//		cout << ht.Find(27) << endl;
//		cout << ht.Find(1) << endl;
//		ht.erase(7);
//		cout << ht.Find(7) << endl;
//		cout << ht.Find(27) << endl;
//	}
//	void test2()
//	{
//		string arr[] = { "苹果", "西瓜", "香蕉", "草莓", "苹果", "西瓜", "苹果", "苹果", "西瓜", "苹果", "香蕉", "苹果", "香蕉" };
//
//		HashTable<string, int> countHT;
//		for (auto& e : arr)
//		{
//			HashData<string, int>* ret = countHT.Find(e);
//			if (ret)
//			{
//				ret->_data.second++;
//			}
//			else
//			{
//				countHT.insert(make_pair(e, 1));
//			}
//		}
//
//		//HashFunc<string> hf;
//		//cout << hf("abc") << endl;
//		//cout << hf("bac") << endl;
//		//cout << hf("cba") << endl;
//		//cout << hf("aad") << endl;
//	}
//}
//namespace Close_Hash
//{
//	enum State// 三种状态
//	{
//		EMPTY,//空
//		EXIST,//存在
//		DELETE,//删除
//	};
//	template<class K>
//	struct HashFunc
//	{
//		size_t operator()(const K& key)
//		{
//			return (size_t)key;
//		}
//	};
//	template<>
//	struct HashFunc<string>
//	{
//		size_t operator()(const string& key)
//		{
//			size_t num = 0;
//			for (auto& ch : key)
//			{
//				num *= 131;
//				num += ch;
//			}
//
//			return num;
//		}
//	};
//	template<class K, class V>
//	struct HashData
//	{
//		pair<K, V> _data;
//		State state = EMPTY;
//	};
//
//	template<class K, class V, class Hash = HashFunc<K>>
//	class HashTable
//	{
//		typedef HashData<K, V> Data;
//	public:
//		HashTable()
//			:_n(0)
//		{
//			_tables.resize(10);
//		}
//		bool insert(const pair<K, V>& data)
//		{
//			if (Find(data.first) < _tables.size())
//			{
//				return false;
//			}
//			//负载因子大于0.7就扩容
//			if (_n * 10 / _tables.size() > 7)
//			{
//				HashTable newHT;
//				newHT._tables.resize(_tables.size() * 2);
//				for (auto& e : _tables)
//				{
//					if (e.state == EXIST)
//					{
//						newHT.insert(e._data);
//					}
//				}
//				_tables.swap(newHT._tables);
//			}
//			Hash hf;
//
//			size_t hashi = hf(data.first) % _tables.size();
//			while (_tables[hashi].state == EXIST)
//			{
//				++hashi;
//				hashi %= _tables.size();
//			}
//			_tables[hashi]._data = data;
//			_tables[hashi].state = EXIST;
//			++_n;
//			return true;
//
//		}
//		bool erase(const K& key)
//		{
//			size_t hashi = Find(key);
//			if (hashi < _tables.size())
//			{
//				_tables[hashi].state = DELETE;
//				_n--;
//				return true;
//			}
//			else
//			{
//				return false;
//			}
//		}
//		size_t Find(const K& key)
//		{
//			Hash hf;
//			size_t hashi = hf(key) % _tables.size();
//
//			while (_tables[hashi].state != EMPTY)
//			{
//				if (_tables[hashi].state == EXIST && _tables[hashi]._data.first == key)
//				{
//					return hashi;
//				}
//				++hashi;
//				hashi %= _tables.size();
//			}
//
//			return -1;
//		}
//
//	private:
//		vector<Data> _tables;
//		int _n;
//	};
//
//	void test1()
//	{
//		int a[] = { 3,38,5,56,7,27,1,17 };
//
//		HashTable<int, int> ht;
//
//		for (auto e : a)
//		{
//			ht.insert(make_pair(e, e));
//		}
//
//		cout << ht.Find(27) << endl;
//		cout << ht.Find(1) << endl;
//		ht.erase(7);
//		cout << ht.Find(7) << endl;
//		cout << ht.Find(27) << endl;
//
//	}
//}