#pragma once

#include "Hashbucket(��ϣͰ).h"

namespace byte
{
	template<class K,class Hash = HashFunc<K>>
	class unordered_set
	{
		struct setKeyOfT
		{
			const K& operator()(const K& key)
			{
				return key;
			}
		};
	public:
		typedef HashBucket<K, K, Hash,setKeyOfT> HaSh;
		typedef typename HashBucket<K, K, Hash, setKeyOfT>::iterator iterator;
		typedef typename HashBucket<K, K, Hash, setKeyOfT>::const_iterator const_iterator;

		iterator begin()
		{
			return _ht.begin();
		}
		iterator end()
		{
			return _ht.end();
		}
		const_iterator begin()const
		{
			return _ht.begin();
		}
		const_iterator end()const
		{
			return _ht.end();
		}
		pair<iterator, bool> insert(const K& key)
		{
			return _ht.Insert(key);
		}
		iterator Erase(iterator pos)
		{
			return _ht.Erase(pos->first);
		}
		iterator Find(const K& key)
		{
			return _ht.Find(key);
		}
	private:
		HaSh _ht;
	};

	//void func(const unordered_set<int>& us)
	//{
	//	unordered_set<int>::const_iterator it = us.begin();
	//	while (it != us.end())
	//	{
	//		cout << (*it) << " ";
	//		++it;
	//	}
	//	cout << endl;
	//}
	void testset()
	{
		unordered_set<int> us;
		us.insert(1);
		us.insert(8);
		us.insert(5);
		us.insert(4);
		us.insert(7);
		us.insert(18);
		us.insert(15);


		unordered_set<int>::iterator it = us.begin();
		while (it != us.end())
		{
			cout << (*it) << " ";
			++it;
		}
		cout << endl;
		for (auto& e : us)
		{
			cout << e << " ";
		}
		cout << endl;

		//func(us);
	}
}
