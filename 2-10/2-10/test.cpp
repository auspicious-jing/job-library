#define _CRT_SECURE_NO_WARNINGS 1

#include <iostream>

using namespace std;

#include <set>
#include <vector>
#include <map>
#include <string>



//void TestSet()
//{
//	// 用数组array中的元素构造set
//	int array[] = { 1, 3, 5, 7, 9, 2, 4, 6, 8, 0, 
//		1, 3, 5, 7, 9, 2, 4,6, 8, 0 };
//	set<int> s;
//
//	for (auto e : array)
//	{
//		s.insert(e);
//	}
//	// 正向打印set中的元素，从打印结果中可以看出：set可去重
//	set<int>::iterator it = s.begin();
//	while (it != s.end())
//	{
//		cout << *it << " ";
//		++it;
//	}
//	cout << endl;
//	// 使用迭代器逆向打印set中的元素
//	for (auto it = s.rbegin(); it != s.rend(); ++it)
//		cout << *it << " ";
//	cout << endl;
//
//	// set中值为3的元素出现了几次
//	cout << s.count(3) << endl;
//}
void TestMap()
{
	 //统计水果出现的次数
	string arr[] = { "苹果", "西瓜", "香蕉", "草莓", "苹果", "西瓜", 
		"苹果", "苹果", "西瓜", "苹果", "香蕉", "苹果", "香蕉" };
	map<string, int> countmap;
	
	//for(auto& e: arr)
	//{
	//	auto it = countmap.find(e);
	//	if (it == countmap.end())
	//	{
	//		countmap.insert(make_pair(e, 1));
	//	}
	//	else
	//	{
	//		it->second++;
	//	}
	//}
	//上面的和下面这个代码结果是一样的
	for (auto& e : arr)
	{
		countmap[e]++;
	}
	for (auto& KV : countmap)
	{
		cout << KV.first << ":" << KV.second << endl;
	}

}
void TestMap1()
{
	multimap<string, string> dict;
	dict.insert(make_pair("left", "左边"));
	dict.insert(make_pair("left", "剩余"));
	dict.insert(make_pair("string", "字符串"));
	dict.insert(make_pair("left", "左侧"));

	for (const auto& kv : dict)
	{
		cout << kv.first << ":" << kv.second << endl;
	}

}

void TestSet1()
{
	int array[] = { 2, 1, 3, 9, 6, 0, 5, 8, 4, 7, 2, 4, 1, 9 };

	// 注意：multiset在底层实际存储的是<int, int>的键值对
	multiset<int> s(array, array + sizeof(array) / sizeof(array[0]));
	for (auto& e : s)
		cout << e << " ";
	cout << endl;
	
}
int main()
{
	//TestSet1();
	TestMap1();
	return 0;
}