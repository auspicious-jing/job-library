#define _CRT_SECURE_NO_WARNINGS 1

#include <iostream>
using namespace std;

//class Date
//{
//public:
//    // 1.无参构造函数
//    Date()
//    {
//        _year = 1;
//        _month = 1;
//        _day = 1;
//    }
//    // 2.带参构造函数
//    // 还可以用缺省参数Date(int year=1, int month=1, int day=1)
//    //用全缺省参数时注意就不要创建无参的构造函数了
//    Date(int year, int month, int day)
//    {
//        _year = year;
//        _month = month;
//        _day = day;
//    }
//private:
//    int _year;
//    int _month;
//    int _day;
//};
//void TestDate()
//{
//    Date d1; // 调用无参构造函数
//    Date d2(2015, 1, 1); // 调用带参的构造函数
//
//    //Date d3();
//    // 注意：如果通过无参构造函数创建对象时，对象后面不用跟括号，否则就成了函数声明
//    // 以下代码的函数：声明了d3函数，该函数无参，返回一个日期类型的对象
//    // warning C4930: “Date d3(void)”: 未调用原型函数(是否是有意用变量定义的?)
//}
//int main()
//{
//    TestDate();
//    return 0;
//}





class Time
{
public:
    Time()
    {
        _hour = 1;
        _minute = 1;
        _second = 1;
}
Time(const Time& t)
{
    _hour = t._hour;
    _minute = t._minute;
    _second = t._second;
    cout << "Time::Time(const Time&)" << endl;
}
private:
    int _hour;
    int _minute;
    int _second;
};
class Date
{
private:
    // 基本类型(内置类型)
    int _year = 1970;
    int _month = 1;
    int _day = 1;
    // 自定义类型
    Time _t;
};
int main()
{
    Date d1;

    // 用已经存在的d1拷贝构造d2，此处会调用Date类的拷贝构造函数
    // 但Date类并没有显式定义拷贝构造函数，则编译器会给Date类生成一个默认的拷贝构
    //造函数
        Date d2(d1);

    return 0;
}