#define _CRT_SECURE_NO_WARNINGS 1

#include <stdio.h>
#include <stdlib.h>
#include <string.h>


//struct Stu
//{
//	char name[20];
//	int age;
//};
//void my_swap(char* e1, char* e2, int width)
//{
//	char tmp = 0;
//	int i = 0;
//	for (i = 0; i < width; i++)
//	{
//		tmp = *e1;
//		*e1 = *e2;
//		*e2 = tmp;
//		e1++;
//		e2++;
//	}
//}
//void my_qst(void* base,int sz,int width,int (*cmp)(const void* e1,const void* e2) )
//{
//	int i = 0;
//	int j = 0;
//	int flag = 1;
//	for (i = 0; i < sz - 1; i++)
//	{
//
//		for (j = 0; j < sz - 1 - i; j++)
//		{
//			if (cmp((char*)base+j*width,(char*)base+(j+1)*width)>0)
//			{
//				my_swap((char*)base + j * width, (char*)base + (j + 1) * width,width);
//				flag = 0;
//			}
//		}
//		if (flag == 1)
//		{
//			break;
//		}
//	}
//}
//int my_cmp_int(const void* e1, const void* e2)
//{
//	return *(int*)e1 - *(int*)e2;
//}
//void test1()
//{
//	int arr[] = { 9,8,7,6,5,4,3,2,1,0 };
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	//qsort(arr, sz, sizeof(arr[0]), my_cmp_int);
//	my_qst(arr, sz, sizeof(arr[0]), my_cmp_int);
//
//	int i = 0;
//	for (i = 0; i < sz; i++)
//	{
//		printf("%d ", arr[i]);
//	}
//}
//int my_compare_name(const void* e1, const void* e2)
//{
//	return strcmp(((struct Stu*)e1)->name,((struct Stu*)e2)->name);
//}
//int my_compare_age(const void* e1, const void* e2)
//{
//	return ((struct Stu*)e1)->age - ((struct Stu*)e2)->age;
//}
//void test2()
//{
//	struct Stu s[] = { {"zhangsan",18},{"lisi",30},{"wangwu",20} };
//	int sz = sizeof(s) / sizeof(s[0]);
//	//qsort(s, sz, sizeof(s[0]), my_compare_name);
//	//qsort(s, sz, sizeof(s[0]), my_compare_age);
//	//my_qst(s, sz, sizeof(s[0]), my_compare_age);
//	my_qst(s, sz, sizeof(s[0]), my_compare_name);
//
//
//
//	int i = 0;
//	for (i = 0; i < sz; i++)
//	{
//		printf("%s %d\n", (s[i]).name, (s[i]).age);
//	}
//}
//int my_cmp_f(const void* e1, const void* e2)
//{
//	if ((*(float*)e1 - *(float*)e2) > 0)
//		return 1;
//	else if ((*(float*)e1 - *(float*)e2) == 0)
//		return 0;
//	else
//		return -1;
//}
//void test3()
//{
//	float f[] = { 9.12f,8.78f,7.65f,6.98f,4.34f,5.12f,3.23f,2.45f,1.10f,0.23f };
//	int sz = sizeof(f) / sizeof(f[0]);
//	//qsort(f, sz, sizeof(f[0]), my_cmp_f);
//	my_qst(f, sz, sizeof(f[0]), my_cmp_f);
//
//	int i = 0;
//	for (i = 0; i < sz; i++)
//	{
//		printf("%.2f ", f[i]);
//	}
//}
//
//
//
//int main()
//{
//	//test1();
//	//test2();
//	test3();
//
//	return 0;
//}


#include <stdio.h>
#include <string.h>

int char_compare(char* s1, char* s2)
{
	char ch[10] = { 0 };
	strcpy(ch, s1);
	int sz = strlen(s1) - 1;
	int i = 0;
	int j = 0;
	char tmp = 0;
	int k = 1;

	//左转一个字符比较一下，相同返回0；直到把字符转完，不返回0就认定s2不是由s1左转来的
	for (i = 0; i < sz; i++)
	{
		tmp = ch[0];
		for (j = 0; j < sz; j++)
		{
			ch[j] = ch[j + 1];
		}
		ch[j] = tmp;
		k = strcmp(ch, s2);
		if (k == 0)
		{
			return 1;
		}
	}
	return 0;
}


int main()
{
	char s1[] = "ABCDEFG";
	char s2[] = "DEFABCG";
	int ret = 1;//如果给两个一样的字符串，那么不进入比较函数，直接打印

	//比较s1，s2如果相同返回0；不进入char_compare函数
	if (strcmp(s1, s2))
	{
		ret = char_compare(s1, s2);
	}

	if (ret == 1)
	{
		printf("是\n");
	}
	else
	{
		printf("不是\n");
	}
	return 0;
}