#pragma once

#include <time.h>

enum Color
{
	RED,
	BLACK
};


template<class T>
struct RBTreeNode
{
	RBTreeNode(const T& Data)
		: _Data(Data)
		,_left(nullptr)
		,_right(nullptr)
		,_parent(nullptr)
		,_col(RED)			//默认是红色
	{}

	T _Data;
	RBTreeNode<T>* _left;
	RBTreeNode<T>* _right;
	RBTreeNode<T>* _parent;
	Color _col;					//节点的颜色

};
template<class T,class Ref,class Ptr>
struct __RBtree_Iterator
{
	typedef RBTreeNode<T> Node;
	typedef __RBtree_Iterator<T,Ref,Ptr> self;
	typedef __RBtree_Iterator<T, T&, T*> iterator;

	Node* _node;

	__RBtree_Iterator( Node* node)
		:_node(node)
	{}
	//参数不是self而是迭代器
	__RBtree_Iterator(const iterator& s)
		:_node(s._node)
	{}

	Ref operator*()const
	{
		return _node->_Data;
	}
	Ptr operator->()const
	{
		return &_node->_Data;
	}

	self& operator++()
	{
		//右不为空，就找右边的最左节点
		if (_node->_right)
		{
			Node* min = _node->_right;
			while (min->_left)
			{
				min = min->_left;
			}
			_node = min;
		}
		else//右为空，要向上找孩子是父亲的左的那个父亲
		{
			Node* cur = _node;
			Node* parent = cur->_parent;
			while (parent && parent->_right == cur)
			{
				cur = parent;
				parent = cur->_parent;
			}
			_node = parent;

		}

		return *this;
	}
	self& operator--()
	{
		if (_node->_left)
		{
			Node* mix = _node->_left;
			while (mix->_right)
			{
				mix = mix->_right;
			}
			_node = mix;
		}
		else
		{
			Node* cur = _node;
			Node* parent = cur->_parent;
			while (parent && parent->_left == cur)
			{
				cur = parent;
				parent = cur->_parent;
			}
			_node = parent;
		}
		return *this;
	}
	
	bool operator!=(const self& node) const
	{
		return _node != node._node;
	}
	bool operator==(const self& node) const
	{
		return _node == node._node;
	}
};


template<class K,class T,class KeyOfT>
class RBTree
{
public:
	typedef RBTreeNode<T> Node;
	typedef __RBtree_Iterator<T,T&,T*> iterator;
	typedef __RBtree_Iterator<T, const T&,const T*> const_iterator;

public:
	iterator begin()
	{
		Node* left = _root;
		while (left->_left)
		{
			left = left->_left;
		}

		return iterator(left);
	}
	const_iterator begin()const
	{
		Node* left = _root;
		while (left->_left)
		{
			left = left->_left;
		}

		return const_iterator(left);
	}

	iterator end()
	{
		return iterator(nullptr);
	}
	const_iterator end()const
	{
		return const_iterator(nullptr);
	}



	 pair<iterator,bool> Insert(const T& Data)
	{
		if (_root == nullptr)
		{
			_root = new Node(Data);
			_root->_col = BLACK;
			return make_pair(iterator(_root),true);//通过root节点构造一个迭代器
		}
		KeyOfT kot;
		Node* cur = _root;
		Node* parent = nullptr;
		while (cur)
		{
			if (kot(cur->_Data) > kot(Data))
			{
				parent = cur;
				cur = cur->_left;
			}
			else if (kot(cur->_Data) < kot(Data))
			{
				parent = cur;
				cur = cur->_right;
			}
			else
			{
				return make_pair(iterator(cur), false);
			}
		}
		
		cur = new Node(Data);
		Node* newnode = cur;//保存新插入的节点，后面要用这个节点构造迭代器，cur会随着循环丢失位置
		cur->_col = RED;
		if (kot(parent->_Data) > kot(Data))
		{
			parent->_left = cur;
			cur->_parent = parent;
		}
		else
		{
			parent->_right = cur;
			cur->_parent = parent;

		}
		//如果parent的颜色是红色那么就继续调整，是黑色就结束
		while ( parent && parent->_col == RED)
		{
			Node* grandParent = parent->_parent;

			if (parent == grandParent->_left)
			{
				Node* uncle = grandParent->_right;
				//情况一
				if (uncle && uncle->_col == RED)
				{
					parent->_col = uncle->_col = BLACK;
					grandParent->_col = RED;

					cur = grandParent;
					parent = cur->_parent;
				}
				else
				{
					//情况二
					if (parent->_left == cur)
					{
						RotateR(grandParent);
						grandParent->_col = RED;
						parent->_col = BLACK;
					}
					else //情况三
					{	
						RotateL(parent);
						RotateR(grandParent);
						grandParent->_col = RED;
						cur->_col = BLACK;
					}
					break;
				}
			}
			else
			{
				Node* uncle = grandParent->_left;
				if (uncle && uncle->_col == RED)
				{
					parent->_col = uncle->_col = BLACK;
					grandParent->_col = RED;

					cur = grandParent;
					parent = cur->_parent;
				}
				else
				{
					if (parent->_right == cur)
					{
						RotateL(grandParent);
						grandParent->_col = RED;
						parent->_col = BLACK;
					}
					else
					{
						RotateR(parent);
						RotateL(grandParent);
						grandParent->_col = RED;
						cur->_col = BLACK;
					}
					break;
				}
			}
		}
		_root->_col = BLACK;
		return make_pair(iterator(newnode), true);
	}

	//向左单旋
	void RotateL(Node* parent)
	{
		Node* subR = parent->_right;
		Node* subRl = subR->_left;

		parent->_right = subRl;
		//subRl不为空就让父指针指向parent节点
		if (subRl)
			subRl->_parent = parent;

		Node* pNode = parent->_parent;
		subR->_left = parent;
		parent->_parent = subR;

		if (pNode == nullptr)
		{
			_root = subR;
			_root->_parent = nullptr;
		}
		else
		{
			if (pNode->_left == parent)
			{
				pNode->_left = subR;
			}
			else
			{
				pNode->_right = subR;
			}
			subR->_parent = pNode;

		}

	}
	//向右单旋
	void RotateR(Node* parent)
	{
		Node* subL = parent->_left;
		Node* subLr = subL->_right;

		parent->_left = subLr;
		if (subLr)
			subLr->_parent = parent;

		Node* pNode = parent->_parent;
		subL->_right = parent;
		parent->_parent = subL;

		if (pNode == nullptr)
		{
			_root = subL;
			_root->_parent = nullptr;
		}
		else
		{
			if (pNode->_left == parent)
			{
				pNode->_left = subL;
			}
			else
			{
				pNode->_right = subL;
			}
			subL->_parent = pNode;

		}
	
	}
	//中序遍历
	void InOrder()
	{
		_InOrder(_root);
		cout << endl;
	}
	void _InOrder(Node* root)
	{
		if (root == nullptr)
			return;
		_InOrder(root->_left);
		cout << root->_Data.first << ":" << root->_Data.second << ":" << root->_col <<endl;
		_InOrder(root->_right);
	}
	bool IsBalance()
	{
		//空树也是红黑树
		if (_root == nullptr)
			return true;
		//根节点的颜色必须为黑色
		if (_root->_col != BLACK)
			return false;
		//拿最左路径的黑色节点，作为每条路径黑色节点的参考值
		int ref = 0;//黑色节点参考值
		Node* cur = _root;
		while (cur)
		{
			if(cur->_col == BLACK)
				ref++;

			cur = cur->_left;
		}
		return _IsBalance(_root,0,ref);
	}
	//用blacknum 记录每条路径的黑色节点的个数
	bool _IsBalance(Node* root,int blacknum,const int& ref)
	{
		if (root == nullptr)
		{
			//每条路径上的黑色节点相同
			if (blacknum != ref)
			{
				cout << "异常：当前路径的黑色节点与参考值不同" << endl;
				return false;
			}

			return true;
		}
		
		//不能有连续的红色节点
		if (root->_col == RED &&  root->_parent->_col == RED)
		{
			cout << root->_Data.first<<"异常：出现了连续的红色节点" << endl;
			return false;
		}

		//统计黑色节点的个数
		if (root->_col == BLACK)
			++blacknum;

		return _IsBalance(root->_left, blacknum, ref)
			&& _IsBalance(root->_right, blacknum, ref);
	}
private:
	Node* _root = nullptr;
};
//
//void test1()
//{
//	int arr[] = { 16, 3, 7, 11, 9, 26, 18, 14, 15 };
//	//int arr[] = { 4, 2, 6, 1, 3, 5, 15, 7, 16, 14 };
//
//	
//
//	RBTree<int, int> rbt;
//
//	for (auto& e : arr)
//	{
//		rbt.Insert(make_pair(e, e));
//	}
//
//	rbt.InOrder();
//
//	cout << rbt.IsBalance() << endl;
//}
//void test2()
//{
//	
//	#define N 10000
//	srand(time(NULL));
//
//	RBTree<int, int> rbt;
//
//	for (size_t i = 0;i<N;++i)
//	{
//		size_t e = rand();
//		rbt.Insert(make_pair(e, e));
//	}
//
//	rbt.InOrder();
//
//	cout << rbt.IsBalance() << endl;
//}
//void test3()
//{
//	RBTree<int, int> rbt;
//
//	rbt.Insert(make_pair(1, 1));
//
//	cout << rbt.IsBalance() << endl;
//}












