#pragma once

#include "RBTree.h"


namespace byte
{
	template<class K>
	class set
	{
		struct SetKeyOfT
		{
			//提取key值
			const K& operator()(const K& key)
			{
				return key;
			}
		};
	public:
		typedef typename RBTree<K, K, SetKeyOfT>::const_iterator iterator;
		typedef typename RBTree<K, K, SetKeyOfT>::const_iterator const_iterator;

		iterator begin()const
		{
			return _t.begin();
		}
		iterator end()const
		{
			return _t.end();
		}

		pair<iterator, bool> insert(const K& key)
		{
			return _t.Insert(key);//会报错
			/*pair<typename RBTree<K, K, SetKeyOfT>::iterator, bool> ret = _t.Insert(key);

			return pair<iterator, bool>(ret.first, ret.second);*/
		}
		

	private:
		RBTree<K, K, SetKeyOfT> _t;
	};

	void testset()
	{
		int arr[] = { 16, 3, 7, 11, 9, 26, 18, 14, 15 };

		set<int> t;

		for (auto& e : arr)
		{
			t.insert(e);
		}

		set<int>::iterator it = t.begin();
		while (it != t.end())
		{
			cout << (*it) << " ";
			++it;
		}
		cout << endl;

		for (auto& e : t)
		{
			cout << e << " ";
		}
		cout << endl;

	}
}
