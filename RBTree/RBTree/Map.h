#pragma once

#include "RBTree.h"



namespace byte
{
	template<class K,class V>
	class map
	{
		struct MapKeyOfT
		{
			//��ȡkeyֵ
			const K& operator()(const pair<const K, V>& kv)
			{
				return kv.first;
			}
		};
	public:
		typedef typename RBTree<K, pair<const K, V>, MapKeyOfT>::iterator iterator;
		typedef typename RBTree<K, pair<const K, V>, MapKeyOfT>::const_iterator const_iterator;


		iterator begin()
		{
			return _t.begin();
		}
		iterator end()
		{
			return _t.end();
		}
		const_iterator begin()const
		{
			return _t.begin();
		}
		const_iterator end()const
		{
			return _t.end();
		}
		pair<iterator, bool> insert(const pair<const K, V>& kv)
		{
			return _t.Insert(kv);
		}
		V& operator[](const K& key)
		{
			 pair<iterator, bool> ret = insert(make_pair(key, V()));
			 return ret.first->second;
		}
		
	private:
		RBTree<K, pair<const K,V>, MapKeyOfT> _t;
	};
	void testmap()
	{
		int arr[] = { 16, 3, 7, 11, 9, 26, 18, 14, 15 };

		map<int,int> t;

		for (auto& e : arr)
		{
			t.insert(make_pair(e,e));
		}

		map<int,int>::iterator it = t.begin();
		while (it != t.end())
		{
			
			cout << (*it).first <<":" << (*it).second << " ";
			++it;
		}
		cout << endl;

		string arr1[] = { "ƻ��", "����", "�㽶", "��ݮ", "ƻ��", "����",
	"ƻ��", "ƻ��", "����", "ƻ��", "�㽶", "ƻ��", "�㽶" };
		map<string, int> countmap;
		for (auto& e : arr1)
		{
			countmap[e]++;
		}
		for (auto& e : countmap)
		{
			cout << e.first << ":" << e.second << endl;
		}

	}
	
}