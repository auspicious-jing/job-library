#define _CRT_SECURE_NO_WARNINGS 1


#include <stdio.h>
//获取天数
int GetMonthDay(int y, int m)
{
    int MonthDay[13] = { 0,31,28,31,30,31,30,31,31,30,31,30,31 };

    if (m == 2 && ((y % 4 == 0 && y % 100 != 0) || (y % 400 == 0)))
        return 29;
    else
        return MonthDay[m];
}

int main() {
    int a, b, c;
    while (scanf("%d %d %d", &a, &b, &c) != EOF) { // 注意 while 处理多个 case
        // 64 位输出请用 printf("%lld")

        int num = 0;
        while (--b >= 1)
        {
            num += GetMonthDay(a, b);

        }
        num += c;
        printf("%d\n", num);
    }
    return 0;
}