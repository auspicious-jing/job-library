#define _CRT_SECURE_NO_WARNINGS 1

#include <iostream>

using namespace std;
//class HeapOnly
//{
//public:
//	static HeapOnly* createOBJ()
//	{
//		return new HeapOnly;
//	}
//private:
//	HeapOnly()
//	{}
//	HeapOnly(const HeapOnly&) = delete;
//	
//};


//int main()
//{
//	HeapOnly hp1;
//	HeapOnly hp2 = new HeapOnly;
//	static HeapOnly hp3;
//
//
//	HeapOnly* hp4 = HeapOnly::createOBJ();//只能通过实现的接口去创建对象
//
//	HeapOnly hp5(*hp4);
//
//
//	return 0;
//}

//class HeapOnly
//{
//public:
//	HeapOnly()
//	{}
//	void destory()
//	{
//		this->~HeapOnly();
//	}
//private:
//	~HeapOnly()
//	{}
//	HeapOnly(const HeapOnly&) = delete;
//
//};
//int main()
//{
//	HeapOnly hp1;
//	HeapOnly hp2 = new HeapOnly;
//	static HeapOnly hp3;
//
//
//	HeapOnly* hp4 = new HeapOnly;
//
//	hp4->destory();
//	
//
//
//	return 0;
//}

////设计一个只能在栈上创建的类
//class StackOnly
//{
//public:
//	static StackOnly createOBJ()
//	{
//		return StackOnly();
//	}
//	/*void* operator new(size_t size) = delete;
//	void operator delete(void* p) = delete;*/   //即使这样禁掉了也还是可以创建静态对象
//private:
//	StackOnly()
//	{}
//
//	//StackOnly(const StackOnly&) = delete;
//};
//int main()
//{
//	StackOnly st1 = StackOnly::createOBJ();
//	static StackOnly st2 = StackOnly::createOBJ();
//
//	static StackOnly st3;
//	StackOnly* st4 = new StackOnly;
//
//
//
//	return 0;
//}

//设计一个只能在栈上创建的类
//class StackOnly
//{
//public:
//	/*static StackOnly createOBJ()
//	{
//		return StackOnly();
//	}*/
//	static StackOnly CreateOBJ()
//	{
//		return StackOnly();
//	}
//	void Print()
//	{
//		cout << "StackOnly::Print" << endl;
//	}
//	/*void* operator new(size_t size) = delete;
//	void operator delete(void* p) = delete;*/   //即使这样禁掉了也还是可以创建静态对象
//private:
//	StackOnly()
//	{}
//
//	StackOnly(const StackOnly& st) = delete;
//};
//int main()
//{
//	/*StackOnly st1 = StackOnly::CreateOBJ();
//	static StackOnly st2 = StackOnly::CreateOBJ();*/
//
//	/*static StackOnly st3;
//	StackOnly* st4 = new StackOnly;*/
//
//	StackOnly::CreateOBJ().Print();
//
//	return 0;
//}


// 请设计一个类，只能在栈上创建对象
class StackOnly
{
public:
	static StackOnly CreateObj()
	{
		return StackOnly();
	}

	void Print() const
	{
		cout << "StackOnly::Print()" << endl;
	}

private:
	StackOnly()
	{}
	StackOnly(const StackOnly&) = delete;
};
int main()
{
	//StackOnly so1 = StackOnly::CreateObj();
	//static StackOnly so4 = StackOnly::CreateObj();

	//static StackOnly so2;
	//StackOnly* pso3 = new StackOnly;

	StackOnly::CreateObj().Print();

	const StackOnly& so4 = StackOnly::CreateObj();
	so4.Print();

	return 0;
}

#include <map>
#include <string>


//饿汉模式
//class Singleton
//{
//public:
//	static Singleton& create()
//	{
//		return _sins;
//	}
//	//插入
//	void Insert(string s, int money)
//	{
//		//_info.insert(make_pair(s, money));
//		_info[s] = money;
//	}
//
//	void Print()
//	{
//		for (const auto& kv : _info)
//		{
//			cout << kv.first << ":" << kv.second << endl;
//		}
//		cout << endl;
//	}
//private:
//	Singleton()
//	{}
//	Singleton(const Singleton&) = delete;
//	Singleton& operator=(const Singleton&) = delete;
//private:
//	map<string, int> _info;
//	static Singleton _sins;
//};
//Singleton Singleton::_sins;//初始化
//
//int main()
//{
//	Singleton::create().Insert("张三", 10000);
//	const Singleton& slt = Singleton::create();
//
//	slt.create().Insert("李四", 12000);
//	slt.create().Insert("王五", 14000);
//	slt.create().Insert("赵六", 9000);
//
//	slt.create().Print();
//
//	Singleton::create().Insert("张三", 13000);//加薪
//	slt.create().Print();
//
//	/*Singleton copy1 = Singleton::create();
//	Singleton copy2(slt);*/
//
//	return 0;
//}
#include <mutex>


//template<class lock>
//class lockGuard //智能指针->自动加锁，解锁	RAII方式
//{
//public:
//	lockGuard(lock& lk)
//		:_lk(lk)
//	{
//		_lk.lock();
//	}
//	~lockGuard()
//	{
//		_lk.unlock();
//	}
//private:
//	lock& _lk;
//};
////懒汉模式
//class Singleton
//{
//public:
//	//static Singleton& create()
//	//{
//	//	//第一次获取单列对象的时候创建对象
//	//	//双检查加锁
//	//	if (_psins == nullptr)//对象new出来以后，避免每次都加锁检查，提高性能
//	//	{
//	//		_mtx.lock();
//	//		if (_psins == nullptr)//保证线程安全且只new一次
//	//		{
//	//			_psins = new Singleton;		//如果new抛异常，就没有解锁
//	//		}
//	//		_mtx.unlock();
//	//	}
//	//	return *_psins;
//	//}
//	
//	static Singleton& create()
//	{
//		//第一次获取单列对象的时候创建对象
//		//双检查加锁
//		if (_psins == nullptr)//对象new出来以后，避免每次都加锁检查，提高性能
//		{
//			//lockGuard<mutex> mtx(_mtx);    //自己实现的
//			std::lock_guard<mutex> mtx(_mtx);//库里提供的
//
//			if (_psins == nullptr)//保证线程安全且只new一次
//			{
//				_psins = new Singleton;
//			}
//		}
//		return *_psins;
//	}
//	//插入
//	void Insert(string s, int money)
//	{
//		//_info.insert(make_pair(s, money));
//		_info[s] = money;
//	}
//
//	void Print()
//	{
//		for (const auto& kv : _info)
//		{
//			cout << kv.first << ":" << kv.second << endl;
//		}
//		cout << endl;
//	}
//	//一般单例对象不需要考虑释放
//	//单例对象不用时，必须手动处理，一些资源需要保存
//	static void DelInstance()
//	{
//		//保存数据到文件
//		//...
//		std::lock_guard<mutex> lock(_mtx);
//		if (_psins)
//		{
//			delete _psins;
//			_psins = nullptr;
//		}
//	}
//	//自动回收
//	class GC
//	{
//	public:
//		~GC()
//		{
//			if (_psins)//如果你没有显示的释放，我就帮你回收
//			{
//				cout << "~GC()" << endl;
//				DelInstance();	//内部类是外部类的友元，可以直接调用
//			}
//		}
//	};
//	
//private:
//	Singleton()
//	{}
//	Singleton(const Singleton&) = delete;
//	Singleton& operator=(const Singleton&) = delete;
//private:
//	map<string, int> _info;
//	static Singleton* _psins;
//	static mutex _mtx;
//	static GC _gc;
//};
//Singleton* Singleton::_psins = nullptr;//初始化
//mutex Singleton::_mtx;
//Singleton::GC Singleton::_gc;
//
//int main()
//{
//	Singleton::create().Insert("张三", 10000);
//	const Singleton& slt = Singleton::create();
//
//	slt.create().Insert("李四", 12000);
//	slt.create().Insert("王五", 14000);
//	slt.create().Insert("赵六", 9000);
//
//	slt.create().Print();
//
//	Singleton::create().Insert("张三", 13000);//加薪
//	slt.create().Print();
//
//	//Singleton::create().DelInstance();
//
//	return 0;
//}

////懒汉模式
//class Singleton
//{
//public:
//
//	static Singleton& create()
//	{
//		static Singleton sins;
//		return sins;
//	}
//	//插入
//	void Insert(string s, int money)
//	{
//		//_info.insert(make_pair(s, money));
//		_info[s] = money;
//	}
//
//	void Print()
//	{
//		for (const auto& kv : _info)
//		{
//			cout << kv.first << ":" << kv.second << endl;
//		}
//		cout << endl;
//	}
//private:
//	Singleton()
//	{
//		cout << "Singleton()" << endl;
//	}
//	Singleton(const Singleton&) = delete;
//	Singleton& operator=(const Singleton&) = delete;
//private:
//	map<string, int> _info;
//};
//int main()
//{
//	Singleton::create().Insert("张三", 10000);
//	const Singleton& slt = Singleton::create();
//
//	slt.create().Insert("李四", 12000);
//	slt.create().Insert("王五", 14000);
//	slt.create().Insert("赵六", 9000);
//
//	slt.create().Print();
//
//	Singleton::create().Insert("张三", 13000);//加薪
//	slt.create().Print();
//
//
//	return 0;
//}