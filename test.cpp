#define _CRT_SECURE_NO_WARNINGS 1

#include <iostream>
#include <vector>
#include <queue>

using namespace std;

#define K 3
#define RADIX 10

queue<int> qe[RADIX];

//基数排序
//获取数值中的第k位
int Getnum(int num,int k)
{
	int key = 0;
	while (k >= 0)
	{
		key = num % 10;
		num /= 10;
		k--;
	}
	return key;
}
void Distribute(vector<int>& arr,int left,int right,int k)//分发数据
{
	for (int i = left; i < right; ++i)
	{
		int idx = Getnum(arr[i], k);
		qe[idx].push(arr[i]);
	}

}

void Collect(vector<int>& arr)//回收数据
{
	int i = 0;
	for (int j = 0; j < RADIX; ++j)
	{
		while (!qe[j].empty())
		{
			arr[i++] = qe[j].front();
			qe[j].pop();
		}
	}
	
}
//基数排序
void RadixSort(vector<int>& arr, int left, int right)
{
	for (int i = 0; i < K; ++i)
	{
		//分发数据
		Distribute(arr,left,right,i);
		//回收数据
		Collect(arr);
	}
}

int main()
{
	vector<int> arr = {278,109,63,930,589,184,505,269,8,83};
	for (auto e : arr)
	{
		cout << e << " ";
	}
	cout << endl;
	RadixSort(arr,0, arr.size());
	for (auto e : arr)
	{
		cout << e << " ";
	}
	cout << endl;
	return 0;
}