#define _CRT_SECURE_NO_WARNINGS 1


#include <iostream>
#include <map>


using namespace std;

//class Date
//{
//public:
//	Date(int year, int month, int day)
//		:_year(year)
//		, _month(month)
//		, _day(day)
//	{
//		cout << "Date(int year, int month, int day)" << endl;
//	}
//private:
//	int _year;
//	int _month;
//	int _day;
//};
//struct Point
//{
//	int _x;
//	int _y;
//};
//
//int main()
//{
//	Point p = { 1,2 };
//	Point p1{ 2,2 };	//C++11,表示赋值符号可以被去掉
//
//
//	int arr[] = { 1,2,3,4 };
//	int arr1[]{ 2,3,4,5 };
//
//	int x = 0;
//	//不建议省掉赋值符号，容易产生歧义，能看懂就行
//	int x1 = { 1 };
//	int x2{ 2 };
//
//	//列表初始化也可以适用于new表达式中
//	int* ptr1 = new int[4];
//	int* ptr2 = new int[4]{ 1,2,3,4 };
//	Point* ptr3 = new Point[2]{ {1,2},{2,2} };//vs2013可能初识化不了该数据，存在bug，没有很好的兼容C++11
//
//	//Date d1(2012, 1, 1);
//	//// C++11支持的列表初始化，这里会调用构造函数初始化
//	//Date d2 = { 2013,1,1 };
//	//Date d3{ 2014,1,1 };
//
//
//	auto il = { 10, 20, 30 };
//	cout << typeid(il).name() << endl;
//
//	return 0;
//}

//int main()
//{
//    int i = 10;
//    auto p = &i;
//    auto pf = strcpy;
//
//    cout << typeid(p).name() << endl;
//    cout << typeid(pf).name() << endl;
//
//    map<string, string> dict = { {"sort", "排序"}, {"insert", "插入"} };
//    //map<string, string>::iterator it = dict.begin();
//
//    auto it = dict.begin();
//    
//
//    return 0;
//}
// 



//
//// decltype的一些使用使用场景
//template<class T1, class T2>
//void F(T1 t1, T2 t2)
//{
//	//不知道这两个参数的类型，就可以通过decltype去推导
//	decltype(t1 * t2) ret = t1 * t2;
//	cout << typeid(ret).name() << endl;
//}
//int main()
//{
//	const int x = 1;
//	double y = 2.2;
//
//	decltype(x * y) ret; // ret的类型是double
//	decltype(&x) p; // p的类型是int*
//	cout << typeid(ret).name() << endl;
//	cout << typeid(p).name() << endl;
//
//	F(1, 'a');
//	return 0;
//}


//int main()
//{
//    double x = 1.1, y = 2.2;
//
//    // 以下几个都是常见的右值
//    10;            //字面常量
//    x + y;         //表达式返回值
//    fmin(x, y);    //函数返回值
//
//    // 以下几个都是对右值的右值引用
//    int&& rr1 = 10;
//    double&& rr2 = x + y;
//    double&& rr3 = fmin(x, y);
//
//    // 这里编译会报错：error C2106: “=”: 左操作数必须为左值
//    10 = 1;
//    x + y = 1;
//    fmin(x, y) = 1;
//    return 0;
//}


//int main()
//{
//	double x = 1.1, y = 2.2;
//	int&& rr1 = 10;
//	const double&& rr2 = x + y;
//	rr1 = 20;
//	rr2 = 5.5; // 报错
//	return 0;
//}



//int main()
//{
//    // 右值引用只能右值，不能引用左值。
//    int&& r1 = 10;
//
//    // error C2440: “初始化”: 无法从“int”转换为“int &&”
//    // message : 无法将左值绑定到右值引用
//    int a = 10;
//    int&& r2 = a;
//
//    // 右值引用可以引用move以后的左值
//    int&& r3 = std::move(a);
//
//    return 0;
//}
#include <assert.h>

namespace bit
{
	class string
	{
	public:
		typedef char* iterator;
		iterator begin()
		{
			return _str;
		}
		iterator end()
		{
			return _str + _size;
		}
		string(const char* str = "")
			:_size(strlen(str))
			, _capacity(_size)
		{
			//cout << "string(char* str)" << endl;
			_str = new char[_capacity + 1];
			strcpy(_str, str);
		}
		// s1.swap(s2)
		void swap(string& s)
		{
			::swap(_str, s._str);
			::swap(_size, s._size);
			::swap(_capacity, s._capacity);
		}
		// 拷贝构造
		string(const string& s)
			:_str(nullptr)
		{
			cout << "string(const string& s) -- 拷贝构造-深拷贝" << endl;
			string tmp(s._str);
			swap(tmp);
		}
		// 赋值重载
		string& operator=(const string& s)
		{
			cout << "string& operator=(string s) -- 拷贝赋值-深拷贝" << endl;
			string tmp(s);
			swap(tmp);
			return *this;
		}
		// 移动构造
		string(string&& s)
			:_str(nullptr)
			, _size(0)
			, _capacity(0)
		{
			cout << "string(string&& s) -- 移动构造" << endl;
			swap(s);
		}
		// 移动赋值
		string& operator=(string&& s)
		{
			cout << "string& operator=(string&& s) -- 移动赋值" << endl;
			swap(s);
			return *this;
		}
		~string()
		{
			delete[] _str;
			_str = nullptr;
		}
		char& operator[](size_t pos)
		{
			assert(pos < _size);
			return _str[pos];
		}
		void reserve(size_t n)
		{
			if (n > _capacity)
			{
				char* tmp = new char[n + 1];
				strcpy(tmp, _str);
				delete[] _str;
				_str = tmp;
				_capacity = n;
			}
		}
		void push_back(char ch)
		{
			if (_size >= _capacity)
			{
				size_t newcapacity = _capacity == 0 ? 4 : _capacity * 2;
				reserve(newcapacity);
			}
			_str[_size] = ch;
			++_size;
			_str[_size] = '\0';
		}
		//string operator+=(char ch)
		string& operator+=(char ch)
		{
			push_back(ch);
			return *this;
		}
		const char* c_str() const
		{
			return _str;
		}
	private:
		char* _str;
		size_t _size;
		size_t _capacity; // 不包含最后做标识的\0
	};
	bit::string to_string(int value)
	{
		bool flag = true;
		if (value < 0)
		{
			flag = false;
			value = 0 - value;
		}
		bit::string str;
		while (value > 0)
		{
			int x = value % 10;
			value /= 10;
			str += ('0' + x);
		}
		if (flag == false)
		{
			str += '-';
		}
			std::reverse(str.begin(), str.end());
		return str;
	}

}
//int main()
//{
//	/*bit::string ret1 = bit::to_string(1234);
//
//	bit::string ret2;
//	ret2 = bit::to_string(-1233);*/
//
//
//
//	bit::string s1("hello");
//	bit::string s2(s1);
//	bit::string s3(move(s1));
//
//
//	return 0;
//}



//void Fun(int& x) { cout << "左值引用" << endl; }
//void Fun(const int& x) { cout << "const 左值引用" << endl; }
//void Fun(int&& x) { cout << "右值引用" << endl; }
//void Fun(const int&& x) { cout << "const 右值引用" << endl; }
//
//template<typename T>
//void PerfectForward(T&& t)
//{
//	//Fun(t);
//	//完美转发，保持原有属性
//	Fun(forward<T>(t));
//}
//int main()
//{
//	/*int x = 10;
//	Fun(x);
//	Fun(10);
//
//	PerfectForward(x);
//	PerfectForward(10);*/
//
//	PerfectForward(10); // 右值
//	int a = 1;
//	PerfectForward(a); // 左值
//	PerfectForward(std::move(a)); // 右值
//
//	const int b = 8;
//	PerfectForward(b); // const 左值
//	PerfectForward(std::move(b)); // const 右值
//	return 0;
//}
//

// 以下代码在vs2013中不能体现，在vs2019下才能演示体现上面的特性。
//class Person
//{
//public:
//	Person(const char* name = "", int age = 0)
//		:_name(name)
//		, _age(age)
//	{}
//	Person(const Person& p)
//		:_name(p._name)
//		,_age(p._age)
//	{}
//	//Person(Person&& p) = default;
//
//
//private:
//	Person(Person&& p);
//
//	bit::string _name;
//	int _age;
//};
//int main()
//{
//	Person s1;
//	Person s2 = s1;
//	Person s3 = std::move(s1);
//
//	/*Person s4;
//	s4 = std::move(s2);*/
//	return 0;
//}

#include <vector>
#include <algorithm>

struct Goods
{
	string _name; // 名字
	double _price; // 价格
	int _evaluate; // 评价
	Goods(const char* str, double price, int evaluate)
		:_name(str)
		, _price(price)
		, _evaluate(evaluate)
	{}
};
struct ComparePriceLess
{
	bool operator()(const Goods& gl, const Goods& gr)
	{
		return gl._price < gr._price;
	}
};
struct ComparePriceGreater
{
	bool operator()(const Goods& gl, const Goods& gr)
	{
		return gl._price > gr._price;
	}
};
//int main()
//{
//	vector<Goods> v = { { "苹果", 2.1, 5 }, { "香蕉", 3, 4 }, { "橙子", 2.2,
//	3 }, { "菠萝", 1.5, 4 } };
//	sort(v.begin(), v.end(), ComparePriceLess());//价格升序
//	for (auto& e : v){cout << e._price << "  ";}cout <<endl;
//
//	sort(v.begin(), v.end(), ComparePriceGreater());//价格降序
//	for (auto& e : v) { cout << e._price << "  "; }cout << endl;
//}
//int main()
//{
//	vector<Goods> v = { { "苹果", 2.1, 5 }, { "香蕉", 3, 4 }, { "橙子", 2.2,
//	3 }, { "菠萝", 1.5, 4 } };
//	sort(v.begin(), v.end(), [](const Goods& g1, const Goods& g2) {
//								return g1._price < g2._price; });//价格升序
//	for (auto& e : v){cout << e._price << "  ";}cout <<endl;
//
//	sort(v.begin(), v.end(), [](const Goods& g1, const Goods& g2) {
//								return g1._price > g2._price; });//价格降序
//	for (auto& e : v) { cout << e._price << "  "; }cout << endl;
//
//	sort(v.begin(), v.end(), [](const Goods& g1, const Goods& g2) {
//								return g1._evaluate < g2._evaluate; });//评价升序
//	for (auto& e : v) { cout << e._evaluate << "  "; }cout << endl;
//
//	sort(v.begin(), v.end(), [](const Goods& g1, const Goods& g2) {
//								return g1._evaluate > g2._evaluate; });//评价降序
//	for (auto& e : v) { cout << e._evaluate << "  "; }cout << endl;
//
//}


//int main()
//{
	////想要直接调用，可借助auto将其赋值给一个变量
	//auto compare = [](int x, int y) {return x > y; };

	//int a = 2,b = 1;
	//cout << compare(a, b) << endl;

	//int a = 10, b = 1;
	//auto compare1 = [a,b]() {return a > b; };//捕捉变量a,b
	//cout << compare1() << endl;

	//auto swapfunc = [a, b]() //传值捕捉不能修改值
	//				{
	//	int ret = a;
	//	a = b;
	//	b = ret;
	//				};
	//swapfunc();

	//auto swapfunc1 = [a, b]()mutable 
	//{
	//	int ret = a;
	//	a = b;
	//	b = ret;
	//};
	//swapfunc1();
	//cout << a <<" : " << b << endl;
	
	//auto swapfunc2 = [&a, &b]()
	//{
	//	int ret = a;
	//	a = b;
	//	b = ret;
	//};
	//swapfunc2();
	//cout << a << " : " << b << endl;

//	int a = 10, b = 1;
//	int x = 1, y = 2;
//	auto func1 = [=] { return a + b + x + y; }; //传值捕捉所有变量
//	cout << func1() << endl;
//	cout << endl;
//
//	auto func2 = [&] ()//传引用捕捉所有变量
//					{ 
//		b = a;
//		y = x;
//					};
//	func2();
//	cout << a << ":" << b << endl;
//	cout << x << ":" << y << endl;
//	cout << endl;
//
//
//	auto func3 = [x,&b] { b = x; };//混合捕捉
//	func3();
//	cout << b << endl;
//	cout << endl;
//
//
//	auto func4 = [=, &b] { b = a; };//混合捕捉
//	func4();
//	cout << b << endl;
//
//	return 0;
//}


//class Rate
//{
//public:
//	Rate(double rate) : _rate(rate)
//	{}
//	double operator()(double money, int year)
//	{
//		return money * _rate * year;
//	}
//private:
//	double _rate;
//};
//int main()
//{
//	// 函数对象
//	double rate = 0.49;
//	Rate r1(rate);
//	r1(10000, 2);
//
//	// lamber
//	auto r2 = [=](double monty, int year)->double {return monty * rate * year;};
//	r2(10000, 2);
//
//	return 0;
//}

////递归终止函数
//void ShowList()
//{
//	cout << endl;
//}
//
//template<class T,class ...Args>
////展开函数
//void ShowList(T val,Args ...args)
//{
//	cout << val << " ";
//	ShowList(args...);
//}

//template <class T>
//void PrintArg(T t)
//{
//	cout << t << " ";
//}
////展开函数
//template <class ...Args>
//void ShowList(Args... args)
//{
//	int arr[] = { (PrintArg(args), 0)... };
//	cout << endl;
//}

//template <class T>
//int PrintArg(T t)
//{
//	cout << t << " ";
//	return 0;
//}
////展开函数
//template <class ...Args>
//void ShowList(Args... args)
//{
//	int arr[] = { PrintArg(args)... };
//	cout << endl;
//}
//
//int main()
//{
//	ShowList(1);
//	ShowList(1,1.1);
//	ShowList(1, 1.1,string("xxxxx"));
//
//	return 0;
//}
#include <list>

//int main()
//{
//	//std::list< std::pair<int, char> > mylist;
//
//	/*mylist.emplace_back(10, 'a');
//	mylist.emplace_back(make_pair(30, 'c'));
//	mylist.push_back(make_pair(40, 'd'));
//	mylist.push_back({ 50, 'e' });
//
//	mylist.emplace_back();*/
//	/*for (auto e : mylist)
//		cout << e.first << ":" << e.second << endl;*/
//
//	std::list< std::pair<int, bit::string> > mylist;
//	mylist.emplace_back(10, "sort");
//	mylist.emplace_back(make_pair(20, "sort"));
//
//	cout << " ------------" << endl;
//
//	mylist.push_back(make_pair(30, "sort"));
//	mylist.push_back({ 40, "sort" });
//
//	
//	return 0;
//}


//template<class F, class T>
//T useF(F f, T x)
//{
//	static int count = 0;
//	cout << "count:" << ++count << endl;
//	cout << "count:" << &count << endl;
//	return f(x);
//}
//double f(double i)
//{
//	return i / 2;
//}
//struct Functor
//{
//	double operator()(double d)
//	{
//		return d / 3;
//	}
//};
//int main()
//{
//	// 函数名
//	cout << useF(f, 11.11) << endl;
//	// 函数对象
//	cout << useF(Functor(), 11.11) << endl;
//	// lamber表达式
//	cout << useF([](double d)->double { return d / 4; }, 11.11) << endl;
//
//	return 0;
//}


//#include <functional>
//int f(int a, int b)
//{
//	return a + b;
//}
//struct Functor
//{
//public:
//	int operator() (int a, int b)
//	{
//		return a + b;
//	}
//};
//class Plus
//{
//public:
//	static int plusi(int a, int b)
//	{
//		return a + b;
//	}
//	int plusd(int a, int b)
//	{
//		return a + b;
//	}
//};
//
//int main()
//{
//	//函数名
//	function<int(int, int)> f1 = f;
//	cout << f1(1, 2) << endl;
//
//	//function<int(int, int)> f2(Functor());//这里识别会有问题，会将Functor()识别成函数指针
//	//函数对象（仿函数）
//	function<int(int, int)> f2 = Functor();
//	cout << f2(2, 2) << endl;
//
//	//lambda表达式
//	function<int(int, int)> f3 = [](const int a, const int b) {return a + b; };
//	cout << f3(3, 2) << endl;
//	
//	//类的成员函数
//	function<int(int, int)> f4 = &Plus::plusi;//静态成员函数
//	cout << f4(4, 2) << endl;
//
//	function<int(Plus, int, int)> f5 = &Plus::plusd;//普通成员函数
//	cout << f5(Plus(),5, 2) << endl;
//
//	return 0;
//}


//#include <functional>
//int Plus(int a, int b)
//{
//	return a + b;
//}
//class Sub
//{
//public:
//	int sub(int a, int b)
//	{
//		return a - b;
//	}
//};
//int main()
//{
//	//表示绑定函数plus 参数分别由调用 func1 的第一，二个参数指定
//	function<int(int, int)> func1 = bind(Plus, placeholders::_1, placeholders::_2);
//	cout << func1(1, 2) << endl;
//
//	//表示绑定函数 plus 的第一，二为： 1， 2
//	auto func2 = std::bind(Plus, 1, 2);
//	cout << func2() << endl;
//
//	function<int(int, int)> func3 = bind(less<int>(), placeholders::_1, placeholders::_2);
//	cout << func3(1, 2) << endl;
//	//调整参数顺序
//	function<int(int, int)> func4 = bind(less<int>(), placeholders::_2, placeholders::_1);
//	cout << func4(1, 2) << endl;
//
//	function<int(Sub, int, int)> f5 = &Sub::sub;
//	cout << f5(Sub(),5, 2) << endl;
//// 
//	//绑定成员函数
//	function<int(int, int)> func5 = bind(&Sub::sub, Sub(), placeholders::_1, placeholders::_2);
//	cout << func5(2, 1) << endl;
//
//	return 0;
//}]


int main()
{

	short c = 65535; 
	short d = { 65535 };

	return 0;
}