//#define _CRT_SECURE_NO_WARNINGS 1
//
//
#include <iostream>
#include <string>

using namespace std;
//
////double Division(int a, int b)
////{
////	 //当b == 0时抛出异常
////	if (b == 0)
////	{
////		string s("Division by zero condition!");
////		throw s;//这里抛的不是s，而是s的拷贝
////	}
////	else
////		return ((double)a / (double)b);
////}
//double Division(int a, int b)
//{
//	// 当b == 0时抛出异常
//	if (b == 0)
//		throw "Division by zero condition!";
//	else
//		return ((double)a / (double)b);
//}
//
//void Func()
//{
//	try 
//	{
//		int len, time;
//		cin >> len >> time;
//		cout << Division(len, time) << endl;
//	}
//	catch (...)//这里我们不知道是什么异常，可以通过这样抛到main函数里去捕获
//	{
//		throw;
//	}
//		
//}
//int main()
//{
//	try {
//		Func();
//	}
//	catch (const string& err)
//	{
//		cout << err << endl;
//	}
//	catch (...) //可能会出现我们不知道类型的异常
//	{
//		cout << "未知异常" << endl;
//	}
//		return 0;
//}
#include <string.h>
//void Permutation(char* pStr, char* pBegin)
//{
//	if (*pBegin == '\0')
//		printf("%s\n", pStr);
//	else
//	{
//		for (char* pCh = pBegin; *pCh != '\0'; ++pCh)
//		{
//			char temp = *pCh;
//			*pCh = *pBegin;
//			*pBegin = temp;
//
//			Permutation(pStr, pBegin + 1);
//
//			temp = *pCh;
//			*pCh = *pBegin;
//			*pBegin = temp;
//		}
//	}
//}
//void Permutation(char* pStr)
//{
//	int sz = strlen(pStr);
//	if (pStr == nullptr)
//		return;
//	Permutation(pStr, pStr);
//	
//}
//
//int main()
//{
//	char p[100];
//	cin  >> p;
//
//	Permutation(p);
//
//	return 0;
//}


//#include <windows.h>
//
//
//// 服务器开发中通常使用的异常继承体系
//class Exception
//{
//public:
//	Exception(const string& errmsg, int id)
//		:_errmsg(errmsg)
//		, _id(id)
//	{}
//	virtual string what() const
//	{
//		return _errmsg;
//	}
//protected:
//	string _errmsg;//错误描述
//	int _id;		//错误id
//};
////数据库异常
//class SqlException : public Exception
//{
//public:
//	SqlException(const string& errmsg, int id, const string& sql)
//		:Exception(errmsg, id)
//		, _sql(sql)
//	{}
//	virtual string what() const
//	{
//		string str = "SqlException:";
//		str += _errmsg;
//		str += "->";
//		str += _sql;
//		return str;
//	}
//private:
//	const string _sql;
//};
////缓存
//class CacheException : public Exception
//{
//public:
//	CacheException(const string& errmsg, int id)
//		:Exception(errmsg, id)
//	{}
//	virtual string what() const
//	{
//		string str = "CacheException:";
//		str += _errmsg;
//		return str;
//	}
//};
////服务器
//class HttpServerException : public Exception
//{
//public:
//	HttpServerException(const string& errmsg, int id, const string& type)
//		:Exception(errmsg, id)
//		, _type(type)
//	{}
//	virtual string what() const
//	{
//		string str = "HttpServerException:";
//		str += _type;
//		str += ":";
//		str += _errmsg;
//		return str;
//	}
//private:
//	const string _type;
//};
//void SQLMgr()
//{
//	srand((unsigned int)time(0));
//	if (rand() % 7 == 0)
//	{
//		throw SqlException("权限不足", 100, "select * from name = '张三'");
//	}
//	throw "1";
//}
//void CacheMgr()
//{
//	srand((unsigned int)time(0));
//	if (rand() % 5 == 0)
//	{
//		throw CacheException("权限不足", 100);
//	}
//	else if (rand() % 6 == 0)
//	{
//		throw CacheException("数据不存在", 101);
//	}
//	SQLMgr();
//}
//void HttpServer()
//{
//	// ...
//	srand((unsigned int)time(0));
//	if (rand() % 3 == 0)
//	{
//		throw HttpServerException("请求资源不存在", 100, "get");
//	}
//	else if (rand() % 4 == 0)
//	{
//		throw HttpServerException("权限不足", 101, "post");
//	}
//	CacheMgr();
//}
//int main()
//{
//	while (1)
//	{
//		Sleep(1000);
//		try 
//		{
//			HttpServer();
//		}
//		catch (const Exception& e) // 这里捕获父类对象就可以
//		{
//			// 多态
//			cout << e.what() << endl;
//		}
//		catch (...)
//		{
//			cout << "未知异常" << endl;
//		}
//	}
//	return 0;
//}
#include <vector>

//int main()
//{
//	try {
//		vector<int> v(10, 5);
//		// 这里如果系统内存不够也会抛异常
//		v.reserve(1000000000);
//		// 这里越界会抛异常
//		v.at(10) = 100;
//	}
//	catch (const exception& e) // 这里捕获父类对象就可以
//	{
//		cout << e.what() << endl;
//	}
//	catch (...)
//	{
//		cout << "Unkown Exception" << endl;
//	}
//	return 0;
//}


class A

{

public:

    A() {}

};

void foo()

{
    throw new A;
}
int main()
{
    try {
        foo();
    }
    //catch (A x) //异常终止
    //catch (A& x)//异常终止
    //catch (A* x)

        {
        cout << "111" << endl;
    }

    return 0;
}