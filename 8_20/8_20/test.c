#define _CRT_SECURE_NO_WARNINGS 1

#include <stdio.h>
#include <string.h>
#include <assert.h>


void* Mymemmove(void* dest, const void* src, size_t num)
{
	assert(dest && src);
	void* ret = dest;
	//两种情况,两个指针相减，大于0就从前往后，小于0就从后往前，等于0的时候不动
	//1.从前往后
	if(((char*)src - (char*)dest) > 0)
	{
		while (num--)
		{
			*(char*)dest = *(char*)src;
			++(char*)dest;
			++(char*)src;

		}
	}
	//2.从后往前
	else if (((char*)src - (char*)dest) < 0)
	{
		/*dest = (char*)dest + (num - 1);
		src = (char*)src + (num - 1);*/
		/*while (num--)
		{
			*(char*)dest = *(char*)src;
			--(char*)dest;
			--(char*)src;
		}*/
		while (num--)
		{
			*((char*)dest + num) = *((char*)src + num);
		}
	}
	else
	{

	}
	return ret;
}

int main()
{
	int arr1[10] = { 1,2,3,4,5,6,7,8,9,10 };

	//memmove(arr1, arr1+3, 20);
	//memmove(arr1+3, arr1, 20);
	//Mymemmove(arr1,arr1+3,20);
	Mymemmove(arr1+3,arr1,20);

	for (int i = 0; i < 10; i++)
	{
		printf("%d ", arr1[i]);
	}
	return 0;
}