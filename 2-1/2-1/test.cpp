#define _CRT_SECURE_NO_WARNINGS 1

#include <iostream>

using namespace std;



//class Student;
//class Person
//{
//public:
//	friend void Display(const Person& p, const Student& s);
//protected:
//	string _name; // 姓名
//};
//class Student : public Person
//{
//protected:
//	int _stuNum; // 学号
//};
//void Display(const Person& p, const Student& s)
//{
//	cout << p._name << endl;
//	cout << s._stuNum << endl;
//}
//int main()
//{
//	Person p;
//	Student s;
//	Display(p, s);
//
//	return 0;
//}


//class Person
//{
//public:
//	Person() { ++_count; }
//protected:
//	string _name; // 姓名
//public:
//	static int _count; // 统计人的个数。
//};
//int Person::_count = 0;
//class Student : public Person
//{
//protected:
//	int _stuNum; // 学号
//};
//class Graduate : public Student
//{
//protected:
//	string _seminarCourse; // 研究科目
//};
//void TestPerson()
//{
//	Student s1;
//	Student s2;
//	Student s3;
//	Graduate s4;
//	cout << " 人数 :" << Person::_count << endl;
//	Student::_count++;
//	cout << " 人数 :" << Person::_count << endl;
//}
//int main()
//{
//	TestPerson();
//
//	return 0;
//}

class Base1
{ 
	public:
		int _b1; 
};
class Base2 { public: int _b2; };
class Derive : public Base1, public Base2
{
public: int _d;
};



int main() 
{

	Derive d;

	Base1* p1 = &d;

	Base2* p2 = &d;

	Derive* p3 = &d;

	return 0;

}