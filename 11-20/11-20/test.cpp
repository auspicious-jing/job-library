#define _CRT_SECURE_NO_WARNINGS 1

//找出数组中出现次数最多的数
//class Solution {
//public:
//    int MoreThanHalfNum_Solution(vector<int> numbers) {
//        //创建一个候选人candidate和票数votes,遍历数组，如果票数为0，那么当前i位置的值就当选候选人，如果有候选人了，就判断下一个数是否和候选人是一样的，一样票数+1，不一样票数-1，继续往后遍历，遍历完成时，数组中出现最多的数字一定当选候选人
//        int cdate = -1;
//        int votes = 0;
//        for (int i = 0; i < numbers.size(); ++i)
//        {
//            if (votes == 0)
//            {
//                cdate = numbers[i];
//                ++votes;
//            }
//            else
//            {
//                if (cdate == numbers[i])
//                    ++votes;
//                else
//                    --votes;
//
//            }
//        }
//
//        // //遍历一遍数组，看cdate是不是最多的,当前题目加不加这个判断都可以，因为它一定会有一个数字是出现最多次数的
//        // votes = 0;
//        // for(auto e:numbers)
//        // {
//        //     if(cdate == e)
//        //         ++votes;
//        // }
//        // if(votes >= numbers.size()/2)
//        //     return cdate;
//
//        // return 0;
//        return cdate;
//    }
//};
//
//数组中只有一个元素出现一次，其他元素都出现3次，找出那个出现一次的元素
//class Solution {
//public:
//    int singleNumber(vector<int>& nums) {
//        //只有一个数字出现一次，其他的都出现三次，那么把每个数的每个二进制位依次相加，第一个二进制位有j个数，那么相加%上3，剩下的就是那个单独元素的二进制位上的数字，通过每个二进制位最后组成那个单独的元素
//        int num = 0;
//        for (int i = 0; i < 32; ++i)
//        {
//            int ret = 0;
//            for (int j = 0; j < nums.size(); ++j)
//            {
//                if ((nums[j] >> i & 1) == 1)//如果j下标当前位置的值向右移动i位，如果是1，那么ret+1，如果nums数组里第j个位置的值的32进制位中第i个位置的数是1，就让ret+1，
//                {
//                    ret++;
//                }
//            }
//            if (ret % 3 != 0)//如果当前二进制位上的数字相加%3后是1的话，那么就让1到num里的第i个二进制位上去，循环结束那个元素就出来了
//            {
//                num = num | 1 << i;
//            }
//        }
//        return num;
//    }
//};