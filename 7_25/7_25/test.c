#define _CRT_SECURE_NO_WARNINGS 1


#include <stdio.h>


void reversr(int* fir, int* end)
{
    while (fir < end)
    {
        int ret = *fir;
        *fir = *end;
        *end = ret;
        fir++;
        end--;
    }
}
void revers(int* nums,int numsSize,int k)
{
    /*k %= sz;
    if (k == 0)
    {
        return;
    }

    while(k)
    {
        int ret = num[sz - 1];
        int i = sz - 1;
        for (i; i > 0; i--)
        {
            num[i] = num[i - 1];
        }
        num[i] = ret;
        k--;
    }*/
    /*int ret[10];

    int i = numsSize - k;
    int j = 0;
    for (i; i < numsSize; i++)
    {
        ret[j++] = nums[i];
    }

    for (i = 0; i < numsSize - k; i++)
    {
        ret[j++] = nums[i];
    }

    for (i = 0; i < numsSize; i++)
    {
        nums[i] = ret[i];
    }*/
    int mid = k;
    int end = numsSize - 1;

    reversr(nums, nums + end);
    reversr(nums, nums + (k - 1));
    reversr(nums + k, nums + (numsSize - 1));
}
int tsest2(int* nums, int sz)
{
    int sum = 0;
    int ret = 0;
    int i = 0;
    for (i = 0; i < sz - 1; i++)
    {
        sum += (i + 1);
        ret += *(nums + i);

    }
    return sum - ret;
}
int main()
{
    int arr[] = { 1,2,3,4,5,6,7 };
    int sz = sizeof(arr) / sizeof(arr[0]);
    int k = 0;
    scanf("%d", &k);
    revers(arr, sz, k);
    int i = 0;
    for (i = 0; i < sz; i++)
    {
        printf("%d ", arr[i]);

    }

    
	return 0;
}